import * as React from "react";
import { Layout } from "antd";
import { HeaderConnector } from "./modules/Header/HeaderConnector";
import { GlobalProgressBar } from "@shared/GlobalProgressBar/GlobalProgressBar";
import { AuthProvider, AuthContext } from "@auth/AuthProvider/AuthProvider";
import { Routes } from "./routes/Routes";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { AppLoader } from "@utils/Loaders/AppLoader/AppLoader";
import { StatusIndicator } from "@utils/StatusIndicator/StatusIndicator";
const { Content } = Layout;

class App extends React.Component {
  public render() {
    return (
      <>
        <StatusIndicator />
        <GlobalProgressBar />
        <Layout
          style={{
            display: "flex",
            flexDirection: "column",
            minHeight: "100vh"
          }}
        >
          <AuthProvider>
            <AuthContext.Consumer>
              {({ isFetchingOnInit }) => (
                <TransitionGroup>
                  <CSSTransition
                    classNames="RoutesFade"
                    timeout={200}
                    key={isFetchingOnInit ? 1 : 2}
                  >
                    {isFetchingOnInit ? (
                      <AppLoader key={1} />
                    ) : (
                      <React.Fragment>
                        <HeaderConnector />
                        <Content
                          style={{
                            flex: 1,
                            marginTop: 64,
                            position: "relative"
                          }}
                        >
                          <Routes />
                        </Content>
                      </React.Fragment>
                    )}
                  </CSSTransition>
                </TransitionGroup>
              )}
            </AuthContext.Consumer>
          </AuthProvider>
        </Layout>
      </>
    );
  }
}

export default App;
