import { combineReducers } from "redux";

import {
  AuthState,
  initialAuthState,
  authReducer
} from "@auth/store/auth.reducer";
import { RouterState, connectRouter } from "connected-react-router";

import { History } from "history";
import {
  BooksFilterState,
  initialBooksFilterState,
  BooksFilterReducer
} from "@books/BooksFilter/store/books.filter.reducer";
import {
  BooksListingState,
  initialBooksListingState,
  BooksListingReducer
} from "@books/BooksListing/store/books.listing.reducer";
import {
  BooksEntityState,
  initialBooksEntityState,
  BooksEntityReducer
} from "@book/store/books.entity.reducer";
import {
  ShoppingCartState,
  initialShoppingCartState,
  ShoppingCartReducer
} from "@shoppingCart/store/shopping.cart.reducer";
import {
  CheckoutState,
  initialCheckoutState,
  CheckoutReducer
} from "@checkout/store/checkout.reducer";
import {
  UserState,
  initialUserState,
  UserReducer
} from "@user/store/user.reducer";

export interface ApplicationState {
  auth: AuthState;
  router?: RouterState;
  booksFilter: BooksFilterState;
  booksListing: BooksListingState;
  booksEntities: BooksEntityState;
  shoppingCart: ShoppingCartState;
  checkout: CheckoutState;
  user: UserState;
}

export const initialApplicationState: ApplicationState = {
  auth: initialAuthState,
  booksFilter: initialBooksFilterState,
  booksListing: initialBooksListingState,
  booksEntities: initialBooksEntityState,
  shoppingCart: initialShoppingCartState,
  checkout: initialCheckoutState,
  user: initialUserState
};

export const rootReducer = (history: History) =>
  combineReducers({
    auth: authReducer,
    booksFilter: BooksFilterReducer,
    booksListing: BooksListingReducer,
    booksEntities: BooksEntityReducer,
    router: connectRouter(history),
    shoppingCart: ShoppingCartReducer,
    checkout: CheckoutReducer,
    user: UserReducer
  });
