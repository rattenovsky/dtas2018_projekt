import { applyMiddleware, createStore, Store } from "redux";
import { ApplicationState, rootReducer } from "./root.reducer";
import { composeWithDevTools } from "redux-devtools-extension";
import { routerMiddleware } from "connected-react-router";
import { createEpicMiddleware, combineEpics } from "redux-observable";
import { AuthEpics } from "@auth/store/auth.epics";
import { History } from "history";
import { BooksFilterEpics } from "@books/BooksFilter/store/books.filter.epics";
import { BooksListingEpics } from "@books/BooksListing/store/books.listing.epics";
import { ShoppingCartEpic } from "@shoppingCart/store/shopping.cart.epics";
import { UserEpics } from "@user/store/user.epics";
import { BooksEntityEpics } from "@book/store/books.entity.epics";

const rootEpic = combineEpics(
  ...AuthEpics,
  ...BooksFilterEpics,
  ...BooksListingEpics,
  ...ShoppingCartEpic,
  ...UserEpics,
  ...BooksEntityEpics
);

export function configureStore(
  history: History,
  initialApplicationState: ApplicationState
): Store<any> {
  const epicMiddleware = createEpicMiddleware();
  const middlewares: any = [routerMiddleware(history), epicMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);
  const enhancers: any = [middlewareEnhancer];
  const composedEnhancers = composeWithDevTools(...enhancers);

  const store = createStore(
    rootReducer(history),
    initialApplicationState,
    composedEnhancers
  );

  epicMiddleware.run(rootEpic);
  return store;
}
