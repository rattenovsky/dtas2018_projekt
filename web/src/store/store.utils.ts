export function toPayload(action: any) {
  return action.payload;
}

export function toData(response: any) {
  return response.data;
}
