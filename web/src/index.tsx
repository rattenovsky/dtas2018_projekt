import "intersection-observer";
import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";
import { configureStore } from "./store";
import createBrowserHistory from "history/createBrowserHistory";
import { initialApplicationState } from "./store/root.reducer";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import "./axiosConfig";
import "@book/store/books.entity.reducer";
export const routerHistory = createBrowserHistory();

export const store = configureStore(routerHistory, initialApplicationState);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={routerHistory}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
