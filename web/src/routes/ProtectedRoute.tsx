import * as React from "react";
import {
  RouteProps,
  Route,
  Redirect,
  RouteComponentProps,
  Switch
} from "react-router";

import { AuthContext } from "@auth/AuthProvider/AuthProvider";

type RenderFunc = (props: RouteComponentProps) => JSX.Element;

export class ProtectedRoute extends React.Component<RouteProps, {}> {
  public static contextType = AuthContext;

  public shouldComponentUpdate(nextProps: any) {
    if (this.props.location.pathname === nextProps.location.pathname) {
      return false;
    }
    return true;
  }

  public render() {
    const { component, ...routeProps } = this.props;
    return (
      <Switch>
        <Route {...routeProps} render={this.renderFunc} />
      </Switch>
    );
  }

  private renderFunc: RenderFunc = props => (
    <AuthContext.Consumer>
      {({ isAuthenticated, isFetchingOnInit }) => {
        if (!isFetchingOnInit && !isAuthenticated) {
          return <Redirect to={"/login"} />;
        } else if (isFetchingOnInit && !isAuthenticated) {
          return null;
        } else if (!isFetchingOnInit && isAuthenticated) {
          return <this.props.component {...props} />;
        } else {
          return null;
        }
      }}
    </AuthContext.Consumer>
  );
}
