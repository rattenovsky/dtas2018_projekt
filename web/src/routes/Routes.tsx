import * as React from "react";
import { Route, Switch, Redirect, Link } from "react-router-dom";

import { HomeView } from "../modules/Home/HomeView";

import { TransitionGroup, CSSTransition } from "react-transition-group";
import "./Routes.css";
import { Button } from "antd";

import { BookDetailConnector } from "@book/BookDetail/BookDetailConnector";

import { ResourceNotFound } from "@shared/ResourceNotFound/ResourceNotFound";

import LoginConnector from "@auth/Login/LoginConnector";
import RegisterConnector from "@auth/Register/RegisterConnector";
import { ProtectedRoute } from "./ProtectedRoute";
import { PosedAppLoader } from "@utils/Loaders/AppLoader/PosedAppLoader";

const CheckoutConnector = React.lazy(() =>
  import("src/modules/Checkout/CheckoutConnector")
);
const UserProfileConnector = React.lazy(() =>
  import("@user/UserProfile/UserProfileConnector")
);

const OrderDetailConnector = React.lazy(() =>
  import("@order/OrderDetail/OrderDetailConnector")
);
const AddBook = React.lazy(() => import("@book/AddBook/AddBook"));

export const pageNotFoundDefaultActions = (
  <Button type="primary">
    <Link to="/home">Go back!</Link>
  </Button>
);

export const Routes: React.SFC = () => (
  <Route
    render={(
      { location } // tslint:disable-next-line:jsx-no-lambda
    ) => (
      <TransitionGroup>
        <CSSTransition key={location.key} timeout={200} classNames="RoutesFade">
          <React.Suspense
            fallback={
              <PosedAppLoader isVisible={true} iconStyles={{ fontSize: 60 }} />
            }
          >
            <Switch location={location}>
              <Route
                path="/"
                exact={true}
                render={() => <Redirect to="/home" />}
              />
              <Route path="/home" component={HomeView} />
              <Route path="/register" component={RegisterConnector} />
              <Route path="/login" component={LoginConnector} />
              <Route path="/book/add" render={() => <AddBook />} />
              <Route path="/book/:id" component={BookDetailConnector} />
              <ProtectedRoute
                path="/user/checkout"
                component={CheckoutConnector}
              />
              <ProtectedRoute
                path="/user/profile"
                component={UserProfileConnector}
              />
              <ProtectedRoute
                path="/order/:id"
                component={OrderDetailConnector}
              />
              <Route
                path="**"
                render={() => (
                  <ResourceNotFound
                    title="Page Not Found!"
                    verticalCenter={{ withHeader: true }}
                    actions={pageNotFoundDefaultActions}
                  />
                )}
              />
            </Switch>
          </React.Suspense>
        </CSSTransition>
      </TransitionGroup>
    )}
  />
);
