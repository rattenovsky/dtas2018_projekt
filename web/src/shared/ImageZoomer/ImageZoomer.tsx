import * as React from "react";
import "./ImageZoomer.css";
import { Modal } from "antd";

interface Props {
  imageSrc: string;
  style?: React.CSSProperties;
}

interface State {
  isImageZoomed: boolean;
}

export class ImageZoomer extends React.PureComponent<Props, State> {
  public state = {
    isImageZoomed: false
  };

  public render() {
    return (
      <React.Fragment>
        <div
          style={{ ...this.props.style, cursor: "pointer" }}
          className="ImageZoomerWrapper"
          onClick={this.onChildrenClickedHandler}
        >
          {this.props.children}
        </div>
        <Modal
          visible={this.state.isImageZoomed}
          closable={false}
          footer={false}
          maskClosable={true}
          onOk={this.onModalCloseHandler}
          onCancel={this.onModalCloseHandler}
          destroyOnClose={true}
          centered={true}
        >
          <img src={this.props.imageSrc} className="ImageZoomerImage" />
        </Modal>
      </React.Fragment>
    );
  }

  private onChildrenClickedHandler = () => {
    this.setState({
      ...this.state,
      isImageZoomed: true
    });
  };

  private onModalCloseHandler = () => {
    this.setState({
      ...this.state,
      isImageZoomed: false
    });
  };
}
