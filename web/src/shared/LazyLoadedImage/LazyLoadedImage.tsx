import * as React from "react";
import "./LazyLoadedImage.css";
// import { idleQueue } from "@utils/globals";
import { BookCover } from "@book/store/books.entity.reducer";

interface State {
  ready: boolean;
}

export class LazyLoadedImage extends React.PureComponent<BookCover, State> {
  public state = {
    ready: false
  };

  private nodeRef: any;
  private hasMounted: boolean = false;

  private observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (entry.intersectionRatio > 0) {
        this.handleImageIntersection();
      }
    });
  });

  public componentDidMount() {
    this.hasMounted = true;
    this.nodeRef && this.observer.observe(this.nodeRef);
  }

  public componentWillUnmount() {
    this.hasMounted = false;
  }

  public render() {
    return (
      <div className="lazy-loader" ref={ref => (this.nodeRef = ref)}>
        {this.state.ready ? (
          <img className="img-normal" src={this.props.normal} />
        ) : null}
        <img
          src={this.props.lazy}
          className={"img-thumb " + (this.state.ready ? "img-hide" : "")}
        />
      </div>
    );
  }

  private handleImageIntersection = () => {
    const buffer = new Image();
    buffer.onload = () => {
      if (!this.hasMounted) {
        return;
      }
      this.setState({ ...this.state, ready: true });
      this.nodeRef && this.observer.unobserve(this.nodeRef);
    };
    buffer.src = this.props.normal;
  };
}
