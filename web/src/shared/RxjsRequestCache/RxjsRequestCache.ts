import { Subject, Observable, of } from "rxjs";
import { tap } from "rxjs/operators";

interface CacheContent {
  expiry: number;
  value: any;
}

export class RxjsRequestCache {
  private cache: Map<string, CacheContent> = new Map();
  private readonly MAX_CACHE_ITEM_AGE: number = 30000;

  public cacheRxjsRequest = (
    target: any,
    key: string,
    descriptor: PropertyDescriptor
  ) => {
    const originalMethod = descriptor.value;
    const classThis = this;
    descriptor.value = function facadeDescriptor(inputFieldValue: string) {
      return classThis.handleCaching(
        classThis,
        this as any,
        inputFieldValue,
        originalMethod
      );
    };
  };

  public cacheFunctionCall = (f: any) => (inputFieldValue: any) => {
    return this.handleCaching(this, this, inputFieldValue, f);
  };

  private handleCaching = (
    thisArg = this,
    originalThis = this,
    inputFieldValue: string,
    originalMethod: any
  ): Observable<any> | Subject<any> => {
    if (thisArg.hasValidCachedValue(inputFieldValue)) {
      return thisArg.getFromCache(inputFieldValue);
    }
    const originalObs = originalMethod.call(originalThis, inputFieldValue);
    return originalObs.pipe(
      tap(val => {
        thisArg.addToCache(inputFieldValue, val);
      })
    );
  };

  private getFromCache(key: string): Observable<any> | Subject<any> {
    return of(this.cache.get(key).value);
  }

  private addToCache(key: string, value: any): void {
    this.cache.set(key, {
      value,
      expiry: Date.now() + this.MAX_CACHE_ITEM_AGE
    });
  }

  private hasValidCachedValue(key: string): boolean {
    if (this.cache.has(key)) {
      if (this.cache.get(key).expiry < Date.now()) {
        return false;
      }
      return true;
    }
    return false;
  }
}
