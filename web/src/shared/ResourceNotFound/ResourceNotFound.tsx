import * as React from "react";
import "./ResourceNotFound.css";

interface Props {
  actions?: React.ReactNode | Element;
  title: string;
  verticalCenter?: { withHeader: boolean };
  style?: React.CSSProperties;
}

export const ResourceNotFound: React.SFC<Props> = props => {
  const verticalClasses = props.verticalCenter
    ? props.verticalCenter.withHeader
      ? "VerticalCenterWithHeader"
      : "VerticalCenterNoHeader"
    : "";
  return (
    <div
      className={"ResourceNotFoundWrapper " + verticalClasses}
      style={props.style}
    >
      <img src="https://gw.alipayobjects.com/zos/rmsportal/KpnpchXsobRgLElEozzI.svg" />
      <div className="ResourceNotFoundContent">
        <h1>{props.title}</h1>
        <div className="ResourceNotFoundActions">{props.actions}</div>
      </div>
    </div>
  );
};
