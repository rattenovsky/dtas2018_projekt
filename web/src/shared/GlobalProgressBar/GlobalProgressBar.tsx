import * as React from "react";
import "./GlobalProgressBar.css";
import { Subscription, BehaviorSubject, timer, of } from "rxjs";
import { switchMap, map, skip, delay, mergeAll } from "rxjs/operators";

const sourceInterval = timer(0, 500);
const requestCounterHandlerSubject = new BehaviorSubject(0);

const pauseableInterval = requestCounterHandlerSubject.pipe(
  map((numOfRequests: number) =>
    numOfRequests > 0 ? of(false) : of(true).pipe(delay(500))
  ),
  mergeAll(),
  switchMap(shouldFinish =>
    shouldFinish
      ? of(shouldFinish)
      : sourceInterval.pipe(map(() => shouldFinish))
  )
);

const rangesMap: { [key: number]: number[] } = {
  25: [0],
  10: [1, 2, 3, 4],
  5: [5, 6, 7, 8],
  [2.5]: [9, 10, 11, 12]
};

function tickFunction(tick: number) {
  const filtered = Object.keys(rangesMap).filter(
    key => rangesMap[key].indexOf(tick) !== -1
  );
  return filtered.length > 0 ? Number(filtered[0]) : 0;
}

interface State {
  transform: number;
  tick: number;
  isFinished: boolean;
}
export class GlobalProgressBar extends React.PureComponent<{}, State> {
  public state = {
    transform: -100,
    tick: 0,
    isFinished: false
  };

  private ticklingSubscription: Subscription;
  private progressBarRef = React.createRef<HTMLDivElement>();

  public componentDidMount() {
    this.ticklingSubscription = pauseableInterval
      .pipe(skip(1))
      .subscribe(shouldFinish =>
        shouldFinish
          ? this.setState({ isFinished: true, transform: -100, tick: 0 })
          : this.handleIncrementTick()
      );
  }

  public componentWillUnmount() {
    this.ticklingSubscription.unsubscribe();
  }

  public render() {
    return (
      <div
        ref={this.progressBarRef}
        onTransitionEnd={this.handleTransitionEnd}
        style={
          this.state.isFinished
            ? {
                transform: "translateX(0%)",
                opacity: 0
              }
            : {
                transform: `translateX(${this.state.transform}%)`
              }
        }
        className="GlobalProgressBar"
      />
    );
  }

  private handleIncrementTick = () => {
    this.setState(({ transform, tick }) => ({
      isFinished: false,
      transform: transform + Number(tickFunction(tick)),
      tick: tick + 1
    }));
  };

  private handleTransitionEnd = (event: React.TransitionEvent) => {
    if (this.state.isFinished && event.propertyName === "opacity") {
      const { current }: { current: HTMLDivElement } = this.progressBarRef;
      // Jake Archibald FTW!
      requestAnimationFrame(() => {
        current.style.transition = "none";
        current.style.transform = "translateX(-100%)";
        current.style.opacity = "1";
        requestAnimationFrame(() => {
          current.style.transition =
            "transform 200ms linear, opacity 1s linear";
        });
      });
    }
  };
}

export function notifyAboutRequest() {
  requestCounterHandlerSubject.next(
    requestCounterHandlerSubject.getValue() + 1
  );
}

export function removeRequest() {
  requestCounterHandlerSubject.next(
    requestCounterHandlerSubject.getValue() - 1
  );
}
