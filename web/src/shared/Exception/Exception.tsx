import * as React from "react";
import "./Exception.css";
import { Icon } from "antd";

interface Props {
  onRetryClick: () => void;
  style?: React.CSSProperties;
}

export const Exception: React.SFC<Props> = props => {
  return (
    <div className="ExceptionWrapper" style={props.style}>
      <div className="Exception">
        <h1>
          <Icon type="meh" style={{ marginRight: 10 }} />
          Oh no!
        </h1>
        <span>
          Something bad happened, you can
          <a onClick={props.onRetryClick}>
            {" "}
            retry whatever you were trying to do.
          </a>
        </span>
      </div>
    </div>
  );
};
