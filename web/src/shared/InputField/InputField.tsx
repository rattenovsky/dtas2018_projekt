import * as React from "react";
import { ReactElement, ReactNode } from "react";
import { Form, Input, Checkbox, DatePicker } from "antd";
import { FieldProps, FormikBag, getIn } from "formik";
import * as moment from "moment";
import TextArea from "antd/lib/input/TextArea";
const FormItem = Form.Item;

type InputFieldFormikTypes = FieldProps<FormikBag<any, any>>;

export interface AntDesignInputTypes {
  prefix: ReactNode;
  size: "default" | "small" | "large";
  type: string;
  label: string;
  style: { [key: string]: any };
}

interface Props {
  showSpinner: boolean;
  isDisabled: boolean;
  isInsideFieldArray?: boolean;
  onDateChangeHandlerFunc?: (val: any, dateString: string) => void;
}

export const InputField: React.SFC<
  InputFieldFormikTypes & Partial<AntDesignInputTypes & Props>
> = ({
  field,
  form: { touched, errors },
  showSpinner,
  isDisabled,
  isInsideFieldArray = false,
  onDateChangeHandlerFunc,
  prefix,
  ...props
}): ReactElement<InputFieldFormikTypes> => {
  const errorMsg = getIn(touched, field.name) && getIn(errors, field.name);
  let formatedDatePickerValue: moment.Moment;

  if (props.type === "datePicker") {
    formatedDatePickerValue = field.value
      ? moment(field.value, "YYYY-MM-DD")
      : undefined;
  }

  function renderInput() {
    switch (props.type) {
      case "checkbox":
        return (
          <Checkbox {...field as any} style={{ ...props.style }}>
            {props.label}
          </Checkbox>
        );
      case "datePicker":
        return (
          <DatePicker
            {...field}
            value={formatedDatePickerValue}
            {...props}
            onChange={onDateChangeHandlerFunc}
          />
        );
      case "textarea":
        return <TextArea {...props} {...field} disabled={isDisabled} />;
      default:
        return (
          <Input {...props} {...field} disabled={isDisabled} prefix={prefix} />
        );
    }
  }
  return (
    <FormItem
      style={{ ...props.style }}
      hasFeedback={true}
      help={errorMsg}
      validateStatus={
        errorMsg ? "error" : showSpinner ? "validating" : undefined
      }
    >
      {renderInput()}
    </FormItem>
  );
};
