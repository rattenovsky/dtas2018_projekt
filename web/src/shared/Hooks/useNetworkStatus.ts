import * as React from "react";

export function useNetworkStatus() {
  // re-render when navigator online status changes
  const [isOnline, setOnline] = React.useState(navigator.onLine);
  function createListener(status: string) {
    return () => {
      setOnline(status === "online");
    };
  }
  React.useEffect(() => {
    const offlineListener = createListener("offline");
    const onlineListener = createListener("online");
    window.addEventListener("online", onlineListener);
    window.addEventListener("offline", offlineListener);
    return () => {
      window.removeEventListener("online", onlineListener);
      window.removeEventListener("offline", offlineListener);
    };
  }, []);

  return isOnline;
}
