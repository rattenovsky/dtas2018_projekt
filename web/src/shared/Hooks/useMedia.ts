import * as React from "react";

export function useMedia(query: string) {
  const [matches, setMatches] = React.useState(
    window.matchMedia(query).matches
  );

  function effectFunction() {
    const media = window.matchMedia(query);
    if (media.matches !== matches) {
      setMatches(media.matches);
    }
    const listener = () => setMatches(media.matches);
    media.addListener(listener);
    return () => media.removeListener(listener);
  }

  React.useEffect(effectFunction, [query]);
  return matches;
}
