import axios from "axios";
import {
  notifyAboutRequest,
  removeRequest
} from "@shared/GlobalProgressBar/GlobalProgressBar";
const authHeaderName = "authorization";
const apiKeyHeaderName = "x-api-key";

// const typeHeaderName = "Content-Type";
axios.defaults.baseURL =
  process.env.NODE_ENV === "production"
    ? "https://emeison.herokuapp.com"
    : "http://localhost:1337/";
axios.defaults.headers.common[authHeaderName] =
  "Bearer " + localStorage.getItem("token");

// axios.defaults.headers[typeHeaderName] = "application/json";
axios.defaults.headers.common[apiKeyHeaderName] =
  "RCKPQPK-CVAMVTG-NR5BFDS-4SB710P";

axios.interceptors.request.use(
  config => {
    notifyAboutRequest();
    config.headers.authorization = "Bearer " + localStorage.getItem("token");
    return config;
  },
  error => {
    removeRequest();
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  response => {
    removeRequest();
    return response;
  },
  error => {
    removeRequest();
    return Promise.reject(error);
  }
);
