import * as React from "react";

export function normalizeString(str: string) {
  return str.toLowerCase().trim();
}

export function getResultMatchStrength(
  splitText: string[],
  stringToCompareTo: string
): number {
  if (normalizeString(splitText[0]) === normalizeString(stringToCompareTo)) {
    return stringToCompareTo.length;
  } else if (
    splitText[1] &&
    normalizeString(splitText[1]) === normalizeString(stringToCompareTo)
  ) {
    return splitText[1].length - splitText[0].length;
  } else {
    return 0;
  }
}

export function splitTextOnMatch(
  str: string,
  matchingString: string
): string[] {
  return str.split(new RegExp(`(${matchingString})`, "i"));
}

export function getWeakerMatchInfo<T>(
  arr: T[],
  matchStrength: number
): { hasWeakerMatch: boolean; weakerMatchIdx: number } {
  const weakerMatches = arr.filter((v: any) => {
    v.matchStrength < matchStrength;
  });
  return {
    hasWeakerMatch: weakerMatches.length > 0 ? true : false,
    weakerMatchIdx: weakerMatches[0]
      ? weakerMatches.indexOf(weakerMatches[0])
      : null
  };
}

export function sortArrayByMatchStrength<T extends { matchStrength: number }>(
  acc: T[]
) {
  return acc.sort((a, b) => {
    if (a.matchStrength > b.matchStrength) {
      return -1;
    } else if (a.matchStrength < b.matchStrength) {
      return 1;
    } else {
      return 0;
    }
  });
}

export function mapSplitTextToProperHTML(
  splitText: string[],
  matchingString: string
) {
  return splitText.map((fragment: string, i: number) =>
    normalizeString(fragment) === normalizeString(matchingString) ? (
      <span key={i} className="typeaheadHighlight">
        {fragment}
      </span>
    ) : (
      fragment
    )
  );
}

export function sanitizeString(str: string): string {
  return str.replace(/[^a-zA-Ząćęłńóśźż ]/g, "");
}
