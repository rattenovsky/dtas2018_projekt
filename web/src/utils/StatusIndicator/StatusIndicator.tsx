import * as React from "react";
import { useNetworkStatus } from "@shared/Hooks/useNetworkStatus";
import "./StatusIndicator.css";
import posed, { PoseGroup } from "react-pose";
import { Icon } from "antd";

const PosedStatusDiv = posed.div({
  enter: {
    opacity: 1
  },
  exit: {
    opacity: 0
  }
});

export const StatusIndicator: React.SFC = () => {
  const isOnline = useNetworkStatus();
  return (
    <PoseGroup>
      {!isOnline ? (
        <PosedStatusDiv key={1} className="StatusIndicatorWrapper">
          You appear to be offline <Icon type="api" />
        </PosedStatusDiv>
      ) : null}
    </PoseGroup>
  );
};
