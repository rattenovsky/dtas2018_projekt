import * as React from "react";
import { Skeleton } from "antd";
import "./SquareLoader.css";

interface Props {
  loaderProps?: { [key: string]: string | number | {} };
}

export const SquareLoader: React.SFC<Props> = ({ loaderProps }) => (
  <div className="SquareLoaderWrapper">
    <Skeleton
      {...loaderProps}
      active={true}
      avatar={{ shape: "square" }}
      title={{ width: 0 }}
      paragraph={{ rows: 0 }}
    />
  </div>
);
