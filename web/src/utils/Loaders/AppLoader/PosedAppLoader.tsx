import * as React from "react";

import "@shared/Css/Utils.css";
import posed, { PoseGroup } from "react-pose";
import { AppLoader } from "./AppLoader";

interface Props {
  innerDivStyle?: React.CSSProperties;
  posedDivStyle?: React.CSSProperties;
  iconStyles?: React.CSSProperties;
  isVisible: boolean;
}

const PosedOpacityDiv = posed.div({
  enter: {
    opacity: 1,
    transition: {
      duration: 500
    },
    position: "fixed"
  },
  exit: {
    opacity: 0,
    transition: {
      duration: 500
    },
    position: "fixed"
  }
});

export const PosedAppLoader: React.SFC<Props> = ({
  innerDivStyle,
  posedDivStyle,
  isVisible,
  iconStyles
}) => {
  return (
    <PoseGroup flipMove={false}>
      {isVisible ? (
        <PosedOpacityDiv
          flipMove={false}
          key={1}
          style={{
            width: "100%",
            height: " 100%",
            zIndex: 999,
            ...posedDivStyle
          }}
        >
          <AppLoader
            loaderWrapperStyles={{
              width: "100%",
              height: "100%",
              background: "rgba(255,255,255,0.7)",
              zIndex: 1,
              position: "absolute",
              ...innerDivStyle
            }}
            iconStyles={{
              fontSize: 40,
              ...iconStyles
            }}
          />
        </PosedOpacityDiv>
      ) : null}
    </PoseGroup>
  );
};
