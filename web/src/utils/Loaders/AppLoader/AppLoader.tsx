import * as React from "react";
import { Icon } from "antd";
import "./AppLoader.css";
import "@shared/Css/Utils.css";
interface Props {
  iconStyles?: React.CSSProperties;
  loaderWrapperStyles?: React.CSSProperties;
}

export const AppLoader: React.SFC<Props> = ({
  iconStyles,
  loaderWrapperStyles
}) => (
  <div
    className="AppLoaderWrapper FlexFullCentered"
    style={loaderWrapperStyles}
  >
    <Icon
      type="setting"
      theme="twoTone"
      spin={true}
      className="AppLoader"
      style={iconStyles}
    />
  </div>
);
