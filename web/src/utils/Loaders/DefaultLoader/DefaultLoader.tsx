import * as React from "react";
import { Skeleton } from "antd";

interface Props {
  loaderProps?: { [key: string]: string | number | {} };
}

export const DefaultLoader: React.SFC<Props> = ({ loaderProps }) => {
  return <Skeleton {...loaderProps} active={true} />;
};
