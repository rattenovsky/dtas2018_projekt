import * as yup from "yup";
import { emailSchema, passwordSchema } from "./form.inputs.schemas";

export const loginSchema = yup.object().shape({
  email: emailSchema,
  password: passwordSchema
});
