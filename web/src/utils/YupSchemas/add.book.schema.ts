import * as yup from "yup";
import {
  defaultTextInputSchema,
  defaultRequiredFeedback
} from "./form.inputs.schemas";
import { addAuthorSchema } from "./add.author.schema";

export const addBookSchema = yup.object().shape({
  title: defaultTextInputSchema(2, 40),
  isbn: yup.string().required(defaultRequiredFeedback),
  pageCount: yup
    .number()
    .positive()
    .integer()
    .min(1)
    .required(defaultRequiredFeedback),
  language: defaultTextInputSchema(2, 10),
  publisher: defaultTextInputSchema(2, 20),
  publishedDate: defaultTextInputSchema(3, 30),
  description: defaultTextInputSchema(10, 100000),
  price: yup
    .number()
    .integer()
    .positive()
    .min(1)
    .required(defaultRequiredFeedback),
  authors: yup.array().of(addAuthorSchema)
});
