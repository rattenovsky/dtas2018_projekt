import * as yup from "yup";
import { defaultTextInputSchema } from "./form.inputs.schemas";

export const addAuthorSchema = yup.object().shape({
  firstName: defaultTextInputSchema(3, 30),
  lastName: defaultTextInputSchema(3, 30)
});
