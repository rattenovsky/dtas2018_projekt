import * as yup from "yup";
import {
  defaultTextInputSchema,
  defaultRequiredFeedback
} from "./form.inputs.schemas";

export const checkoutShippingFormSchema = yup.object().shape({
  firstName: defaultTextInputSchema(3, 30),
  lastName: defaultTextInputSchema(3, 30),
  city: defaultTextInputSchema(2, 20),
  street: defaultTextInputSchema(2, 30),
  zipCode: yup
    .number()
    .required(defaultRequiredFeedback)
    .positive()
    .integer()
    .test(
      "have 5 digits",
      "Zipcode has to have 5 digits",
      (value: number) => String(value).length === 5
    )
});
