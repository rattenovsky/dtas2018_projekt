import * as yup from "yup";

const fieldNotLongEnough = (min: number) =>
  `This field must be at least ${min} characters`;
const invalidEmail = "Email must be a valid email";
export const defaultRequiredFeedback = "This field is required";

export const passwordSchema = yup
  .string()
  .min(3, fieldNotLongEnough(3))
  .max(255)
  .required("Password is required");

export const emailSchema = yup
  .string()
  .min(3, fieldNotLongEnough(3))
  .max(255)
  .email(invalidEmail)
  .required("Email is required");

export const defaultTextInputSchema = (min: number, max: number) => {
  return yup
    .string()
    .min(min, fieldNotLongEnough(min))
    .max(max)
    .required(defaultRequiredFeedback);
};

export const checkboxSchema = yup.bool().required();
