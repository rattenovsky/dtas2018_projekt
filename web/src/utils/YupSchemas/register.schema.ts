import * as yup from "yup";
import {
  emailSchema,
  passwordSchema,
  defaultTextInputSchema
} from "./form.inputs.schemas";

export const registerSchema = yup.object().shape({
  firstName: defaultTextInputSchema(3, 30),
  lastName: defaultTextInputSchema(3, 30),
  email: emailSchema,
  password: passwordSchema,
  repeatPassword: passwordSchema.oneOf(
    [yup.ref("password"), null],
    "Passwords don't match"
  )
});
