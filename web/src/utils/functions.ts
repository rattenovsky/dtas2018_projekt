export function composeRight(...fns: any[]) {
  return (
    fns
      // map index getting in the way!
      .filter(f => {
        return typeof f === "function";
      })
      .reduce((fn1: any, fn2: any) => (args: any[]) => fn2(fn1(args)))
  );
}

export function composeLeft(...fns: any[]) {
  return composeRight(...fns.reverse());
}

export const mapReducer = <K, T>(mapFunction: (element: K) => K | T) => (
  combiner: PushCombiner
) => (transformedList: K[], nextElement: K): K[] | T[] =>
  combiner(transformedList, mapFunction(nextElement));

export const filterReducer = <T>(
  predicateFunction: (element: T) => boolean
) => (combiner: PushCombiner) => (
  filteredElements: T[],
  nextElement: T
): T[] => {
  return predicateFunction(nextElement)
    ? combiner(filteredElements, nextElement)
    : filteredElements;
};

export const removeDuplicateElementsFromArray = <T>(array: T[]): T[] =>
  Array.from(new Set(array));

export const filterDataBasedOnArray = <T>(
  data: T[],
  rootArray: T[],
  getMissing: boolean = false
): T[] =>
  data.filter(arrElement =>
    getMissing
      ? !rootArray.includes(arrElement)
      : rootArray.includes(arrElement)
  );
export function pushCombiner<T>(list: T[], element: T) {
  list.push(element);
  return list;
}

export const transduce = (...fns: any[]) => (combiner: PushCombiner) =>
  composeLeft(...fns)(combiner);

type PushCombiner = <T>(list: T[], element: T) => any[];
