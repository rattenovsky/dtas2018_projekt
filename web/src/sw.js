workbox.skipWaiting();
workbox.clientsClaim();
workbox.setConfig({ debug: false });

workbox.precaching.precacheAndRoute(
  [...self.__precacheManifest, "/offline.html"] || []
);

workbox.routing.registerNavigationRoute("/index.html");

workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|svg)$/,
  workbox.strategies.cacheFirst({
    cacheName: "lazy-images",
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50
      })
    ]
  })
);

workbox.routing.registerRoute(
  new RegExp("https://emeison.herokuapp.com/.*"),
  workbox.strategies.networkFirst({
    cacheName: "api-cache"
  })
);

const customFallbackHandler = async args => {
  try {
    const response = await networkFirst({ cacheName: "app-cache" }).handle(
      args
    );
    return response || (await caches.match("/offline.html"));
  } catch (error) {
    return await caches.match("/offline.html");
  }
};
// emeison-web2.herokuapp.com
workbox.routing.registerRoute(
  /^.*emeison-web.herokuapp.com\/[^.]*$/,
  customFallbackHandler
);
