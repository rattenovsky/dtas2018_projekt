import * as React from "react";
import { RouteComponentProps } from "react-router";
import "@header/Header.css";
import { Menu, Icon } from "antd";
import { ShoppingCartHeaderConnector } from "@shoppingCart/ShoppingCartHeader/ShoppingCartHeaderConnector";
import { DrawerMenuView } from "./DrawerMenuView";
import { UserAvatarConnector } from "@user/UserAvatar/UserAvatarConnector";
import { HeaderAutoComplete } from "@header/HeaderAutoComplete/HeaderAutoComplete";

interface PassedProps {
  location: RouteComponentProps["location"];
  drawerStateChangedHandler: () => void;
  isDrawerOpened: boolean;
  isAuthenticated: boolean;
  onLogoutHandler: () => void;
  closeDrawerHandler: () => void;
}

export const MobileHeaderView: React.SFC<PassedProps> = props =>
  props.isAuthenticated ? (
    <AuthenticatedView {...props} />
  ) : (
    <NotAuthenticatedView {...props} />
  );

const AuthenticatedView: React.SFC<PassedProps> = props => (
  <React.Fragment>
    <Menu
      selectable={false}
      theme="light"
      mode="horizontal"
      className="MobileVisible HeaderMenu IsAuthenticated"
    >
      <Menu.Item
        className={"MobileDrawerBars"}
        onClick={props.drawerStateChangedHandler}
      >
        <Icon type="bars" style={{ marginRight: 0, fontSize: 20 }} />
      </Menu.Item>
      <Menu.Item>
        <UserAvatarConnector />
      </Menu.Item>
      <Menu.Item style={{ padding: 0 }}>
        <ShoppingCartHeaderConnector />
      </Menu.Item>
      <HeaderAutoComplete />
    </Menu>
    <DrawerMenuView {...props} />
  </React.Fragment>
);

const NotAuthenticatedView: React.SFC<PassedProps> = props => (
  <React.Fragment>
    <Menu
      selectable={false}
      theme="light"
      mode="horizontal"
      className="MobileVisible HeaderMenu"
    >
      <HeaderAutoComplete />
      <Menu.Item
        className={"MobileDrawerBars"}
        onClick={props.drawerStateChangedHandler}
      >
        <Icon type="bars" style={{ marginRight: 0, fontSize: 20 }} />
      </Menu.Item>
    </Menu>
    <DrawerMenuView {...props} />
  </React.Fragment>
);
