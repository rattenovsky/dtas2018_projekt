import * as React from "react";
import { RouteComponentProps } from "react-router";
import { Drawer, Menu } from "antd";
import { Link } from "react-router-dom";

interface DrawerMenuProps {
  location: RouteComponentProps["location"];
  isDrawerOpened: boolean;
  drawerStateChangedHandler: () => void;
  closeDrawerHandler: () => void;
  isAuthenticated: boolean;
  onLogoutHandler: () => void;
}

export const DrawerMenuView: React.SFC<DrawerMenuProps> = (
  props: DrawerMenuProps
) => (
  <Drawer
    className="MobileDrawer MobileVisible"
    title="Menu"
    placement="right"
    closable={true}
    onClose={props.drawerStateChangedHandler}
    visible={props.isDrawerOpened}
  >
    {props.isAuthenticated ? (
      <AuthenticatedView {...props} />
    ) : (
      <NotAuthenticatedView {...props} />
    )}
  </Drawer>
);

const NotAuthenticatedView: React.SFC<DrawerMenuProps> = ({
  location,
  closeDrawerHandler
}) => (
  <Menu
    mode="inline"
    style={{ height: "100%", borderRight: 0 }}
    selectedKeys={[location.pathname]}
  >
    <Menu.Item key="/home" onClick={closeDrawerHandler}>
      <Link to={"/home"} />
      Home
    </Menu.Item>
    <Menu.Item key="/register" onClick={closeDrawerHandler}>
      <Link to={"/register"} />
      Register
    </Menu.Item>
    <Menu.Item key="/login" onClick={closeDrawerHandler}>
      <Link to={"/login"} />
      Login
    </Menu.Item>
  </Menu>
);

const AuthenticatedView: React.SFC<DrawerMenuProps> = ({
  location,
  onLogoutHandler,
  closeDrawerHandler
}) => (
  <Menu
    mode="inline"
    style={{ height: "100%", borderRight: 0 }}
    selectedKeys={[location.pathname]}
  >
    <Menu.Item key="/home" onClick={closeDrawerHandler}>
      <Link to={"/home"} />
      Home
    </Menu.Item>
    <Menu.Item onClick={callAll(onLogoutHandler, closeDrawerHandler)}>
      Logout
    </Menu.Item>
  </Menu>
);

function callAll(...fns: Array<() => void>) {
  return () => {
    fns.forEach(fn => fn && fn());
  };
}
