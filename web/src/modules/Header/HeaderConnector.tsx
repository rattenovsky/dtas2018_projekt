import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { ApplicationState } from "@store/root/root.reducer";
import { Dispatch, AnyAction, compose } from "redux";
import { Logout } from "@auth/store/auth.actions";
import { connect } from "react-redux";
import { HeaderView } from "./HeaderView";

interface DispatchProps {
  onLogoutHandler: () => void;
}

interface StateProps {
  isAuthenticated: boolean;
}

interface State {
  isDrawerOpened: boolean;
}

export class C extends React.PureComponent<
  RouteComponentProps & StateProps & DispatchProps,
  State
> {
  public state = {
    isDrawerOpened: false
  };

  public render() {
    return (
      <HeaderView
        {...this.props}
        {...this.state}
        drawerStateChangedHandler={this.drawerStateChangedHandler}
        closeDrawerHandler={this.closeDrawerHandler}
      />
    );
  }

  private drawerStateChangedHandler = () => {
    this.setState(({ isDrawerOpened }) => ({
      isDrawerOpened: !isDrawerOpened
    }));
  };

  private closeDrawerHandler = () => {
    this.setState({
      ...this.state,
      isDrawerOpened: false
    });
  };
}

const mapStateToProps = ({ auth }: ApplicationState): StateProps => ({
  isAuthenticated: auth.isAuthenticated
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>): DispatchProps => ({
  onLogoutHandler: () => dispatch(Logout())
});

export const HeaderConnector = compose(
  withRouter,
  connect<StateProps, DispatchProps, {}>(
    mapStateToProps,
    mapDispatchToProps
  )
)(C);
