import * as React from "react";
import { RouteComponentProps } from "react-router";
import { Menu } from "antd";
import { Link } from "react-router-dom";
import "@header/Header.css";
import "@shared/Css/Utils.css";
import { ShoppingCartHeaderConnector } from "@shoppingCart/ShoppingCartHeader/ShoppingCartHeaderConnector";
import { UserAvatarConnector } from "@user/UserAvatar/UserAvatarConnector";
import { HeaderAutoComplete } from "@header/HeaderAutoComplete/HeaderAutoComplete";

interface PassedProps {
  location: RouteComponentProps["location"];
  isAuthenticated: boolean;
  onLogoutHandler: () => void;
}

export const DesktopHeaderView: React.SFC<PassedProps> = props =>
  props.isAuthenticated ? (
    <AuthenticatedView {...props} />
  ) : (
    <NotAuthenticatedView {...props} />
  );

const NotAuthenticatedView: React.SFC<PassedProps> = ({ location }) => {
  return (
    <Menu
      theme="light"
      mode="horizontal"
      selectedKeys={[location.pathname]}
      className="DesktopVisible HeaderMenu"
    >
      <Menu.Item key="/home">
        <Link to={"/home"} />
        Home
      </Menu.Item>
      <HeaderAutoComplete />
      <Menu.Item key="/register">
        <Link to={"/register"} />
        Register
      </Menu.Item>
      <Menu.Item key="/login">
        <Link to={"/login"} />
        Login
      </Menu.Item>
    </Menu>
  );
};

const AuthenticatedView: React.SFC<PassedProps> = ({
  location,
  onLogoutHandler
}) => (
  <Menu
    theme="light"
    mode="horizontal"
    selectedKeys={[location.pathname]}
    className="DesktopVisible HeaderMenu"
  >
    <Menu.Item key="/home">
      <Link to={"/home"} />
      Home
    </Menu.Item>
    <HeaderAutoComplete />
    <Menu.Item>
      <UserAvatarConnector />
    </Menu.Item>
    <Menu.Item style={{ padding: 0 }}>
      <ShoppingCartHeaderConnector />
    </Menu.Item>
    <Menu.Item onClick={onLogoutHandler}>Logout</Menu.Item>
  </Menu>
);
