import * as React from "react";
import { TypeaheadResult } from "./HeaderAutoComplete";

interface Props {
  typeaheadResult: TypeaheadResult;
  onTypeaheadResultItemClick: (result: { id: string; type: string }) => void;
  shouldHighlightText: boolean;
}

export const HeaderAutoCompleteResult: React.SFC<Props> = React.memo(
  ({ typeaheadResult, onTypeaheadResultItemClick, shouldHighlightText }) => {
    return (
      <React.Fragment>
        <div className="TypeaheadResultCategory TypeaheadResultItem">
          {typeaheadResult.displayName}
        </div>
        {typeaheadResult.results.map(result => (
          <li
            key={result.id}
            className={
              "TypeaheadResultItem " +
              (!shouldHighlightText ? "noTypeaheadTextHighlight" : "")
            }
            // tslint:disable-next-line:jsx-no-lambda
            onMouseDown={e => {
              e.stopPropagation();
              onTypeaheadResultItemClick({
                id: result.id,
                type: typeaheadResult.type
              });
            }}
          >
            {result.modTitle}
          </li>
        ))}
      </React.Fragment>
    );
  }
);
