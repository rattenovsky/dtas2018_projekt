import * as React from "react";
import { Subscription, from, of, Subject } from "rxjs";
import axios, { CancelTokenSource } from "axios";
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  map,
  tap,
  catchError,
  filter
} from "rxjs/operators";
import { toData } from "@store/root/store.utils";
import { RxjsRequestCache } from "@shared/RxjsRequestCache/RxjsRequestCache";
import { HeaderAutoCompleteView } from "./HeaderAutoCompleteView";
import { message } from "antd";

import {
  splitTextOnMatch,
  getResultMatchStrength,
  getWeakerMatchInfo,
  sortArrayByMatchStrength,
  normalizeString,
  mapSplitTextToProperHTML,
  sanitizeString
} from "@utils/typeahead";
import { withRouter, RouteComponentProps } from "react-router";
import {
  FilterPayload,
  BooksFilterActions,
  FilterSelected,
  ConstructFilterUrl,
  ResetFilters
} from "@books/BooksFilter/store/books.filter.actions";
import { Dispatch, compose } from "redux";
import { connect } from "react-redux";
import { ApplicationState } from "@store/root/root.reducer";

const cache = new RxjsRequestCache();

interface DispatchProps {
  filterSelected: (payload: FilterPayload) => void;
  constructFilterUrl: () => void;
  resetFilters: () => void;
}

interface StateProps {
  publisherAuthorSelectedFilters: string[];
}

export interface TypeaheadResultItem {
  modTitle: Array<string | JSX.Element>;
  matchStrength: number;
  id: string;
  typeaheadField: string;
}

export interface TypeaheadResult {
  type: string;
  displayName: string;
  results: TypeaheadResultItem[];
}

export type TypeaheadResults = TypeaheadResult[];

interface State {
  typeaheadResults: TypeaheadResults;
  isLoadingResults: boolean;
  typeaheadInputValue: string;
}

export class C extends React.PureComponent<
  RouteComponentProps & DispatchProps & StateProps,
  State
> {
  public state = {
    typeaheadResults: [] as TypeaheadResults,
    isLoadingResults: false,
    typeaheadInputValue: ""
  };

  private inputValueSubscription: Subscription;
  private inputValueStream: Subject<string> = new Subject();
  private cancelablePromiseSource: CancelTokenSource;

  public render() {
    return (
      <HeaderAutoCompleteView
        onTypeaheadResultItemClick={this.onTypeaheadResultItemClick}
        onTypeaheadInputReset={this.handleTypeaheadInputReset}
        typeaheadInputValue={this.state.typeaheadInputValue}
        isLoadingResults={this.state.isLoadingResults}
        typeaheadResults={this.state.typeaheadResults}
        onTypeaheadChangeHandler={this.handleTypeaheadInputChange}
      />
    );
  }

  public componentWillUnmount() {
    this.inputValueSubscription.unsubscribe();
  }

  public componentDidMount() {
    this.inputValueSubscription = this.inputValueStream
      .pipe(
        map(sanitizeString),
        filter((str: string) => str.length > 0),
        tap((typeaheadInputValue: string) => {
          if (this.state.typeaheadResults.length > 0) {
            this.updateResultsParsing(
              this.state.typeaheadResults,
              typeaheadInputValue
            );
          }
        }),
        debounceTime(500),
        distinctUntilChanged(this.inputComparerFunc),
        tap(() => this.setState({ isLoadingResults: true })),
        switchMap(inputValue =>
          this.fetchData(inputValue).pipe(
            map(fetchResults => {
              const dataFromFetchResults = toData(fetchResults);
              return this.parseResults(dataFromFetchResults, inputValue);
            }),
            catchError(e => {
              message.error("An error occurred");
              this.setState({
                isLoadingResults: false,
                typeaheadResults: []
              });
              return of();
            })
          )
        )
      )
      .subscribe((results: TypeaheadResults) => {
        this.setState({
          typeaheadResults: results,
          isLoadingResults: false
        });
      });
  }

  private modifyTypeaheadResultItem = (
    typeaheadResultItem: TypeaheadResultItem,
    typeaheadInputValue: string
  ): TypeaheadResultItem => {
    const splitTitle = splitTextOnMatch(
      typeaheadResultItem.typeaheadField,
      typeaheadInputValue
    );

    const matchStrength = getResultMatchStrength(
      splitTitle,
      typeaheadInputValue
    );

    const modTitle = mapSplitTextToProperHTML(splitTitle, typeaheadInputValue);

    return {
      ...typeaheadResultItem,
      matchStrength,
      modTitle
    };
  };

  private updateResultsParsing = (
    results: TypeaheadResults = this.state.typeaheadResults,
    typeaheadInputValue: string
  ) => {
    const parsedResults = this.parseResults(results, typeaheadInputValue);
    this.setState({ typeaheadResults: parsedResults });
  };

  private parseResults = (
    typeaheadResults: TypeaheadResults,
    typeaheadInputValue: string
  ): TypeaheadResults => {
    return typeaheadResults
      .map(typeaheadResult => ({
        ...typeaheadResult,
        results: this.reduceTypeaheadResultItems(
          typeaheadResult.results,
          typeaheadInputValue
        )
      }))
      .sort((a, b) => (a.results.length > b.results.length ? -1 : 1));
  };

  private reduceTypeaheadResultItems(
    typeaheadResultItems: TypeaheadResultItem[],
    typeaheadInputValue: string
  ) {
    const reducedResults = typeaheadResultItems.reduce(
      (acc: TypeaheadResultItem[], result) => {
        const modifiedResult = this.modifyTypeaheadResultItem(
          result,
          typeaheadInputValue
        );

        if (acc.length < 20) {
          acc.push(modifiedResult);
          return acc;
        }
        const { hasWeakerMatch, weakerMatchIdx } = getWeakerMatchInfo<
          TypeaheadResultItem
        >(acc, modifiedResult.matchStrength);

        if (!hasWeakerMatch) {
          return acc;
        }
        return acc.splice(weakerMatchIdx, 0, modifiedResult);
      },
      []
    );
    return sortArrayByMatchStrength(reducedResults);
  }

  private inputComparerFunc = (inputVal1: string, inputVal2: string) => {
    return normalizeString(inputVal1) === normalizeString(inputVal2);
  };

  @cache.cacheRxjsRequest
  private fetchData(value: string) {
    this.cancelPreviousRequest();
    return from(
      axios.get(`/book/search?q=${value}`, {
        cancelToken: this.cancelablePromiseSource.token
      })
    );
  }

  private cancelPreviousRequest() {
    if (this.cancelablePromiseSource) {
      this.cancelablePromiseSource.cancel();
    }
    this.cancelablePromiseSource = axios.CancelToken.source();
  }

  private handleTypeaheadInputReset = () => {
    this.setState({ typeaheadInputValue: "" });
  };

  private handleTypeaheadInputChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    this.setState({ typeaheadInputValue: event.currentTarget.value });
    this.inputValueStream.next(event.currentTarget.value);
  };

  private onTypeaheadResultItemClick = (result: {
    type: string;
    id: string;
  }) => {
    switch (result.type) {
      case "book":
        if (this.props.location.pathname !== `/book/${result.id}`) {
          this.props.history.push(`/book/${result.id}`);
        }
        break;
      case "publisher":
      case "author":
        if (!this.props.location.pathname.includes("home")) {
          this.props.history.push("/home");
        }
        this.props.resetFilters();
        this.props.filterSelected({
          filterType: result.type,
          filterValue: [result.id]
        });
        this.props.constructFilterUrl();
        break;
    }
  };
}

const mapDispatchToProps = (
  dispatch: Dispatch<BooksFilterActions>
): DispatchProps => ({
  filterSelected: filterPayload => dispatch(FilterSelected(filterPayload)),
  constructFilterUrl: () => dispatch(ConstructFilterUrl()),
  resetFilters: () => dispatch(ResetFilters())
});

const mapStateToProps = ({ booksFilter }: ApplicationState): StateProps => ({
  publisherAuthorSelectedFilters: booksFilter.selectedAuthors.concat(
    booksFilter.selectedPublishers
  )
});

export const HeaderAutoComplete = compose(
  connect<StateProps, DispatchProps>(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter
)(C);
