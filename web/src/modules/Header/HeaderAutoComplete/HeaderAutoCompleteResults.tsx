import * as React from "react";
import "./HeaderAutoComplete.css";
import posed from "react-pose";
import { Icon } from "antd";
import "@shared/Css/Utils.css";
import { TypeaheadResults, TypeaheadResult } from "./HeaderAutoComplete";
import { HeaderAutoCompleteResult } from "./HeaderAutoCompleteResult";

interface Props {
  typeaheadResults: TypeaheadResults;
  isLoading: boolean;
  inputClicked: boolean;
  inputDirty: boolean;
  formVisible: boolean;
  onTypeaheadResultItemClick: (result: { id: string; type: string }) => void;
  doesInputHaveText: boolean;
}

const PosedAutoCompleteResultsWrapper = posed.div({
  vs: {
    height: "auto",
    transition: {
      ease: "backInOut"
    }
  },
  hd: {
    height: 0,
    transition: {
      duration: 0
    }
  }
});
export const HeaderAutoCompleteResults: React.SFC<Props> = React.memo(
  ({
    inputClicked,
    inputDirty,
    typeaheadResults,
    isLoading,
    onTypeaheadResultItemClick,
    formVisible,
    doesInputHaveText
  }) => {
    return (
      <PosedAutoCompleteResultsWrapper
        pose={
          formVisible &&
          (shouldDisplayLoading() ||
            shouldDisplayNoResults() ||
            shouldDisplayResults())
            ? "vs"
            : "hd"
        }
        className="HeaderAutoCompleteResultsWrapper"
      >
        <ul
          className="HeaderAutoCompleteResultsWrapperInner"
          style={isLoading ? { opacity: 0.5 } : { opacity: 1 }}
        >
          {shouldDisplayLoading() ? renderLoading() : null}
          {shouldDisplayResults() ? typeaheadResults.map(renderResult) : null}
          {shouldDisplayNoResults() ? renderNoResults() : null}
        </ul>
      </PosedAutoCompleteResultsWrapper>
    );

    function renderResult(result: TypeaheadResult) {
      return (
        <HeaderAutoCompleteResult
          key={result.type}
          typeaheadResult={result}
          onTypeaheadResultItemClick={onTypeaheadResultItemClick}
          shouldHighlightText={doesInputHaveText}
        />
      );
    }

    function renderNoResults() {
      return (
        <li
          className="FlexFullCentered TypeaheadFeedbackWrapper"
          style={{
            overflow: "hidden"
          }}
        >
          No results ...
        </li>
      );
    }

    function renderLoading() {
      return (
        <li className="FlexFullCentered TypeaheadFeedbackWrapper">
          <Icon type="loading" style={{ fontSize: 20 }} />
        </li>
      );
    }

    function shouldDisplayNoResults() {
      return inputDirty && typeaheadResults.length === 0 && !isLoading;
    }

    function shouldDisplayLoading() {
      return isLoading && inputDirty && typeaheadResults.length === 0;
    }

    function shouldDisplayResults() {
      return (
        (typeaheadResults.length > 0 && inputDirty) ||
        (inputClicked && typeaheadResults.length > 0 && !isLoading)
      );
    }
  }
);
