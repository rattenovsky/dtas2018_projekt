import * as React from "react";
import { Icon } from "antd";
import posed from "react-pose";
import "./HeaderAutoComplete.css";
import { useMedia } from "@shared/Hooks/useMedia";
import { HeaderAutoCompleteResults } from "./HeaderAutoCompleteResults";
import "@shared/Css/Utils.css";

// ten div odpowiada za animacje wysuwania i chowania sie searchbara
const PosedAutoCompleteWrapper = posed.div({
  visible: {
    width: ({ isOnMobile }: { isOnMobile: boolean }) =>
      isOnMobile ? "calc(100vw - 96px)" : 400
  },
  hidden: {
    width: 0,
    afterChildren: true
  }
});

interface Props {
  typeaheadResults: any[];
  isLoadingResults: boolean;
  onTypeaheadChangeHandler: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void;
  typeaheadInputValue: string;
  onTypeaheadInputReset: () => void;
  onTypeaheadResultItemClick: (result: { id: string; type: string }) => void;
}

export const HeaderAutoCompleteView: React.SFC<Props> = React.memo(
  ({
    typeaheadResults,
    onTypeaheadChangeHandler,
    isLoadingResults,
    typeaheadInputValue,
    onTypeaheadInputReset,
    onTypeaheadResultItemClick
  }) => {
    const [formVisible, setFormVisible] = React.useState(false);
    const [inputClicked, setInputClicked] = React.useState(false);
    const [inputDirty, setInputDirty] = React.useState(false);
    const inputRef = React.createRef<HTMLInputElement>();
    const isBlurring = React.useRef(false);
    const isOnMobile = useMedia("(max-width: 660px)");

    return (
      <div className="HeaderAutoCompleteWrapper" onBlur={toggleFormVisible}>
        <Icon
          type="search"
          onClick={toggleFormVisible}
          className="HeaderAutoCompleteSearchIcon FlexCenteredVertically"
        />
        <PosedAutoCompleteWrapper
          pose={formVisible ? "visible" : "hidden"}
          onPoseComplete={handlePoseComplete}
          style={{ position: "relative", height: "100%" }}
          isOnMobile={isOnMobile}
        >
          <div
            className="HeaderAutoCompleteForm"
            style={{ overflow: "hidden" }}
          >
            <input
              placeholder="Search for something ..."
              type="text"
              ref={inputRef}
              onClick={handleInputClicked}
              className={
                "HeaderAutoCompleteInput " +
                (typeaheadInputValue !== "" ? "HasText" : "")
              }
              onChange={onInputChangeLocalHandler}
              value={typeaheadInputValue}
            />
            <div
              className="AutoCompleteEraseTextIconWrapper"
              style={
                !formVisible && typeaheadInputValue !== "" ? { opacity: 0 } : {}
              }
              onMouseDown={onTypeaheadInputResetLocalHandler}
            >
              <Icon
                type="close-circle"
                theme="filled"
                className="AutoCompleteEraseTextIcon"
              />
            </div>
          </div>
          <HeaderAutoCompleteResults
            doesInputHaveText={typeaheadInputValue.trim().length > 0}
            formVisible={formVisible}
            onTypeaheadResultItemClick={onTypeaheadResultItemClick}
            inputClicked={inputClicked}
            inputDirty={inputDirty}
            isLoading={isLoadingResults}
            typeaheadResults={typeaheadResults}
          />
        </PosedAutoCompleteWrapper>
      </div>
    );

    function toggleFormVisible() {
      if (isBlurring.current) {
        return;
      }
      if (!isBlurring.current && formVisible) {
        isBlurring.current = true;
      }
      setFormVisible(!formVisible);
      setInputClicked(false);
      setInputDirty(false);
    }

    function handleInputClicked() {
      inputClicked === false ? setInputClicked(true) : null;
    }

    function onInputChangeLocalHandler(
      event: React.ChangeEvent<HTMLInputElement>
    ) {
      if (!inputDirty) {
        setInputDirty(true);
      }
      onTypeaheadChangeHandler(event);
    }

    function onTypeaheadInputResetLocalHandler(
      e: React.MouseEvent<HTMLElement>
    ) {
      e.stopPropagation();
      e.preventDefault();
      setInputDirty(false);
      onTypeaheadInputReset();
    }

    function handlePoseComplete(currentPose: string) {
      currentPose === "visible" && inputRef.current
        ? inputRef.current.focus()
        : inputRef.current && onBlurHandler();
    }

    function onBlurHandler() {
      inputRef.current.blur();
      isBlurring.current = false;
      return true;
    }
  }
);
