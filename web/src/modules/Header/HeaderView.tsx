import * as React from "react";
import { Layout } from "antd";
import { RouteComponentProps } from "react-router";
import { DesktopHeaderView } from "./DesktopView/DesktopHeaderView";
import { MobileHeaderView } from "./MobileView/MobileHeaderView";
import "./Header.css";
import { useMedia } from "@shared/Hooks/useMedia";
const { Header } = Layout;

interface Props {
  onLogoutHandler: () => void;
  isAuthenticated: boolean;
  isDrawerOpened: boolean;
  location: RouteComponentProps["location"];
  drawerStateChangedHandler: () => void;
  closeDrawerHandler: () => void;
}

export const HeaderView: React.SFC<Props> = (props: Props) => {
  const mobileVisible = useMedia("(max-width:800px)");
  return (
    // will change is added so that the header is added to its own composite layer and does not cause repaint when scrolling
    <Header className="HeaderFixed" style={{ willChange: "transform", zIndex: 99 }}>
      {mobileVisible ? (
        <MobileHeaderView {...props} />
      ) : (
        <DesktopHeaderView
          location={props.location}
          isAuthenticated={props.isAuthenticated}
          onLogoutHandler={props.onLogoutHandler}
        />
      )}
    </Header>
  );
};
