import * as React from "react";
import "@shared/Css/Tile.css";
import { Order } from "@user/store/user.reducer";
import { Book } from "@book/store/books.entity.reducer";
import * as moment from "moment";
import { ImageZoomer } from "@shared/ImageZoomer/ImageZoomer";
import { defaultBookCover } from "@book/BookCard/BookCardView";
import { useMedia } from "@shared/Hooks/useMedia";

interface Props {
  hasOrder: boolean;
  order: Order;
  orderedBooks: Book[];
}

export const OrderDetailView: React.SFC<Props> = props => {
  const isOnMobile = useMedia("(max-width:720px)");
  return (
    <div className="TileWrapper OrderDetailTileWrapper">
      <div className="TileInner">
        <div
          className="TileUpAndDownHeadingContainer"
          style={{ borderBottom: 0, paddingBottom: 0 }}
        >
          <h1>Your Order</h1>
        </div>
        <div
          className="TileUpAndDownHeadingContainer OrderList"
          style={{
            flexDirection: "column",
            alignItems: "flex-start",
            paddingBottom: 16
          }}
        >
          <p>
            <b>Ordered at: </b> {moment(props.order.createdAt).format("lll")}
          </p>
          <p>
            <b>Total Cost: </b> {props.order.overallPrice} zl
          </p>
          <p>
            <b>Shipping Method: </b> {props.order.shipping}
          </p>
          <p>
            <b>Payment Method: </b> {props.order.payment}
          </p>
        </div>
        <div
          className="TileUpAndDownHeadingContainer"
          style={{ borderBottom: 0, paddingBottom: 0 }}
        >
          <h1>Shipping Details</h1>
        </div>
        <div
          className="TileUpAndDownHeadingContainer OrderList"
          style={{
            flexDirection: "column",
            alignItems: "flex-start",
            paddingBottom: 16
          }}
        >
          {Object.entries(props.order.shippingInfo).map(
            ([key, value], index) => (
              <p key={index}>
                <b>{key}: </b> {value}
              </p>
            )
          )}
        </div>
      </div>
      <div className="TileInner">
        <div className="TileUpAndDownHeadingContainer">
          <h1>Ordered Books ({props.orderedBooks.length})</h1>
        </div>
        <div className="OrderBookListWrapper">
          {props.orderedBooks.map(book => (
            <div
              className="OrderBookListItemWrapper FlexCenteredVertically"
              key={book.id}
            >
              {renderBookInList(book, props.order.books[book.id])}
            </div>
          ))}
        </div>
      </div>
    </div>
  );

  function renderBookInList(book: Book, bookQuantity: number) {
    return (
      <React.Fragment>
        <div
          className="CheckoutListItemImageWrapper OrderImageWrapper"
          style={{ alignSelf: "flex-start" }}
        >
          {book.covers.length === 0 ? (
            <ImageZoomer
              imageSrc={defaultBookCover.normal}
              style={{ width: 64, height: 64 }}
            >
              <img src={defaultBookCover.normal} />
            </ImageZoomer>
          ) : (
            <ImageZoomer
              imageSrc={book.covers[0].normal}
              style={{ width: 64, height: 64 }}
            >
              <img src={book.covers[0].normal} />
            </ImageZoomer>
          )}
        </div>

        <div>
          <div>
            <b>Title: </b> {book.title}
          </div>
          <div className="BookItemDescription">
            <b>Description: </b> {book.description}
          </div>
          <div className="BookItemPrice">
            <b>Price: </b> {book.price} zl
          </div>
          {isOnMobile ? (
            <div>
              <b>Quantity: </b> {bookQuantity}
            </div>
          ) : null}
        </div>
        {!isOnMobile ? (
          <div style={{ padding: 6, marginLeft: "auto" }}>
            <b>({bookQuantity})</b>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
};
