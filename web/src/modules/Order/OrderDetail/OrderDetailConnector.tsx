import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import axios from "axios";
import { toData } from "@store/root/store.utils";
import { Order } from "@user/store/user.reducer";
import { Book, getHydratedBook } from "@book/store/books.entity.reducer";
import { KeyQuantityBooks } from "@shoppingCart/store/shopping.cart.reducer";
import { ApplicationState } from "@store/root/root.reducer";
import { compose, Dispatch } from "redux";
import { connect } from "react-redux";
import { UserActions, AddOrdersToStore } from "@user/store/user.actions";
import {
  BooksEntityActions,
  AddBooksToStore
} from "@book/store/books.entity.action";
import { normalize } from "normalizr";
import { orderListSchema } from "@user/store/user.epics";
import { PosedAppLoader } from "@utils/Loaders/AppLoader/PosedAppLoader";
import { Exception } from "@shared/Exception/Exception";
import { OrderDetailView } from "./OrderDetailView";
import "./OrderDetail.css";
import { routerHistory } from "src";
const ORDER_EMPTY = "ORDER_EMPTY";

interface State {
  isLoading: boolean;
  errorOccurred: boolean;
  hasOrder: boolean;
}

interface StateProps {
  order: Order;
  orderedBooks: Book[];
}

interface DispatchProps {
  addBooksToStore: (books: Book[]) => void;
  addOrdersToStore: (order: Order) => void;
}

class C extends React.Component<
  RouteComponentProps<{ id: string }> & StateProps & DispatchProps,
  State
> {
  public state = {
    isLoading: true,
    errorOccurred: false,
    hasOrder: false
  };

  public async componentDidMount() {
    if (!this.props.order) {
      await this.handleOrderNotInStore();
    } else {
      this.setState({
        isLoading: false,
        hasOrder: true,
        errorOccurred: false
      });
    }
  }

  public render() {
    return (
      <React.Fragment>
        <PosedAppLoader isVisible={this.state.isLoading} />
        {this.state.errorOccurred ? (
          <Exception onRetryClick={this.handleOrderNotInStore} />
        ) : !this.state.isLoading && this.state.hasOrder ? (
          <OrderDetailView
            hasOrder={this.state.hasOrder}
            orderedBooks={this.props.orderedBooks}
            order={this.props.order}
          />
        ) : !this.state.isLoading && !this.state.hasOrder ? (
          <div className="TileWrapper NoOrderTile">
            <div className="TileInner NoOrderInner">
              <h1>Order not found</h1>
              <p>Order was not made by you or it did not exists</p>
              <a onClick={this.onGoBackClick}>Go back</a>
            </div>
          </div>
        ) : null}
      </React.Fragment>
    );
  }

  private onGoBackClick = () => {
    routerHistory.push("/user/profile");
  };

  private handleOrderNotInStore = async () => {
    // i know setState is async, it might get weird here, next time i will use mobx :)
    this.setState({ isLoading: true });
    try {
      const [order, orderedBooks] = await this.getOrderAlongWithBooks();
      this.props.addBooksToStore(orderedBooks);
      this.props.addOrdersToStore(order);
      this.handleOrderSuccessfullyFetched();
    } catch (e) {
      if (e.message === ORDER_EMPTY) {
        this.handleOrderEmpty();
      } else {
        this.handleErrorOccurred();
      }
    }
  };

  private handleOrderEmpty() {
    this.setState({
      isLoading: false,
      hasOrder: false,
      errorOccurred: false
    });
  }

  private handleOrderSuccessfullyFetched() {
    this.setState({
      isLoading: false,
      hasOrder: true,
      errorOccurred: false
    });
  }

  private handleErrorOccurred() {
    this.setState({
      isLoading: false,
      hasOrder: false,
      errorOccurred: true
    });
  }

  private async getOrderAlongWithBooks(): Promise<[Order, Book[]]> {
    const { id } = this.props.match.params;
    const [order]: [Order] = await axios.get(`/order/${id}`).then(toData);
    if (!order) {
      throw new Error(ORDER_EMPTY);
    }
    const orderedBooks: Book[] = await axios
      .get(`/book/multiple?${this.mapKeyQuantityBooksToParams(order.books)}`)
      .then(toData);
    return [order, orderedBooks];
  }

  private mapKeyQuantityBooksToParams(books: KeyQuantityBooks): string {
    return Object.keys(books)
      .map((bookId: string) => `id=${bookId}`)
      .join("&");
  }
}

const mapStateToProps = (
  { user, booksEntities }: ApplicationState,
  {
    match: {
      params: { id }
    }
  }: RouteComponentProps<{ id: string }>
): StateProps => ({
  order: user.entities.orders[id || ""],
  orderedBooks:
    id && user.entities.orders && user.entities.orders[id]
      ? Object.keys(user.entities.orders[id].books).map((bookId: string) =>
          getHydratedBook(booksEntities, bookId)
        )
      : []
});

const mapDispatchToProps = (
  dispatch: Dispatch<UserActions | BooksEntityActions>
): DispatchProps => ({
  addBooksToStore: books => dispatch(AddBooksToStore(books)),
  addOrdersToStore: order =>
    dispatch(AddOrdersToStore(normalize([order], orderListSchema)))
});

const OrderDetailConnector = compose(
  withRouter,
  connect<StateProps, DispatchProps>(
    mapStateToProps,
    mapDispatchToProps
  )
)(C);

export default OrderDetailConnector;
