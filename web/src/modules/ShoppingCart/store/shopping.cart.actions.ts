import { ActionCreator } from "redux";
import { KeyQuantityBooks } from "./shopping.cart.reducer";

export enum ShoppingCartActionsEnum {
  resolveShoppingCartBooks = "@@ShoppingCartEpics RESOLVE_SHOPPING_CART_BOOKS",
  shoppingCartBooksResolved = "@@Shopping Cart Components SHOPPING_CART_BOOKS_RESOLVED",
  fetchShoppingCartBooks = "@@AuthEpics FETCH_SHOPPING_CART_BOOKS",
  shoppingCartBooksFetchedSuccessfully = "@@ShoppingCartEpics SHOPPING_CART_BOOKS_FETCHED_SUCCESSFULLY",
  addShoppingCartBooksToStore = "@@ShoppingCartEpics ADD_SHOPPING_CART_BOOKS_TO_STORE",
  addBookToShoppingCart = "@@BookDetailBuyPanel ADD_BOOK_TO_SHOPPING_CART",
  bookAddedToShoppingCartSuccessfully = "@@ShoppingCartEpics BOOK_ADDED_TO_SHOPPING_CART_SUCCESSFULLY",
  addBookToShoppingCartError = "@@ShoppingCartEpics ADD_BOOK_TO_SHOPPING_CART_ERROR",
  clearCurrentCart = "@@ShoppingCartCheckout DELETE_CURRENT_CART",
  currentCartClearedSuccessfully = "@@ShoppingCartEpics CURRENT_CART_CLEARED_SUCCESSFULLY",
  deleteCartItem = "@@ShoppingCartCheckout DELETE_CART_ITEM",
  cartItemDeletedSuccessfully = "@@ShoppingCartEpics CART_ITEM_DELETED_SUCCESSFULLY",
  shoppingCartProceed = "@@ShoppingCartCheckout SHOPPING_CART_PROCEED",
  updateCartItemQuantity = "@@ShoppingCartCheckout UPDATE_CART_ITEM_QUANTITY",
  cartItemQuantityUpdatedSuccessfully = "@@ShoppingCartEpics CART_ITEM_QUANTITY_UPDATED_SUCCESSFULLY",
  resetCartState = "@@AuthEpics RESET_CART_STATE"
}

interface ShoppingCartBooksResolvedAction {
  type: ShoppingCartActionsEnum.shoppingCartBooksResolved;
}

interface ResetCartStateAction {
  type: ShoppingCartActionsEnum.resetCartState;
}

interface FetchShoppingCartBooksAction {
  type: ShoppingCartActionsEnum.fetchShoppingCartBooks;
}

interface AddBookToShoppingCartErrorAction {
  type: ShoppingCartActionsEnum.addBookToShoppingCartError;
}

interface UpdateCartItemQuantityAction {
  type: ShoppingCartActionsEnum.updateCartItemQuantity;
  payload: { bookId: string; quantity: number };
}

interface CartItemQuantityUpdatedSuccessfullyAction {
  type: ShoppingCartActionsEnum.cartItemQuantityUpdatedSuccessfully;
  payload: { bookId: string; quantity: number };
}

interface ShoppingCartProceedAction {
  type: ShoppingCartActionsEnum.shoppingCartProceed;
}

interface ShoppingCartBooksFetchedSuccessfullyAction {
  type: ShoppingCartActionsEnum.shoppingCartBooksFetchedSuccessfully;
}

interface ClearCurrentCartAction {
  type: ShoppingCartActionsEnum.clearCurrentCart;
}

interface DeleteCartItemAction {
  type: ShoppingCartActionsEnum.deleteCartItem;
  payload: string;
}

interface CartItemDeletedSuccessfullyAction {
  type: ShoppingCartActionsEnum.cartItemDeletedSuccessfully;
  payload: string;
}

interface CurrentCartClearedSuccessfullyAction {
  type: ShoppingCartActionsEnum.currentCartClearedSuccessfully;
}

interface AddBookToShoppingCartAction {
  type: ShoppingCartActionsEnum.addBookToShoppingCart;
  payload: { bookId: string; silent: boolean };
}

interface BookAddedToShoppingCartSuccessfullyAction {
  type: ShoppingCartActionsEnum.bookAddedToShoppingCartSuccessfully;
  payload: string;
}

interface AddShoppingCartBooksToStoreAction {
  type: ShoppingCartActionsEnum.addShoppingCartBooksToStore;
  payload: KeyQuantityBooks;
}

interface ResolveShoppingCartBooksAction {
  type: ShoppingCartActionsEnum.resolveShoppingCartBooks;
}

export const ShoppingCartBooksResolved: ActionCreator<
  ShoppingCartBooksResolvedAction
> = () => ({
  type: ShoppingCartActionsEnum.shoppingCartBooksResolved
});

export const ShoppingCartProceed: ActionCreator<
  ShoppingCartProceedAction
> = () => ({
  type: ShoppingCartActionsEnum.shoppingCartProceed
});

export const AddBookToShoppingCartError: ActionCreator<
  AddBookToShoppingCartErrorAction
> = () => ({
  type: ShoppingCartActionsEnum.addBookToShoppingCartError
});

export const UpdateCartItemQuantity: ActionCreator<
  UpdateCartItemQuantityAction
> = (payload: { bookId: string; quantity: number }) => ({
  type: ShoppingCartActionsEnum.updateCartItemQuantity,
  payload
});

export const CartItemQuantityUpdatedSuccessfully: ActionCreator<
  CartItemQuantityUpdatedSuccessfullyAction
> = (payload: { bookId: string; quantity: number }) => ({
  type: ShoppingCartActionsEnum.cartItemQuantityUpdatedSuccessfully,
  payload
});

export const CurrentCartClearedSuccessfully: ActionCreator<
  CurrentCartClearedSuccessfullyAction
> = () => ({
  type: ShoppingCartActionsEnum.currentCartClearedSuccessfully
});

export const AddBookToShoppingCart: ActionCreator<
  AddBookToShoppingCartAction
> = payload => ({
  type: ShoppingCartActionsEnum.addBookToShoppingCart,
  payload
});

export const BookAddedToShoppingCartSuccessfully: ActionCreator<
  BookAddedToShoppingCartSuccessfullyAction
> = (payload: string) => ({
  type: ShoppingCartActionsEnum.bookAddedToShoppingCartSuccessfully,
  payload
});

export const ClearCurrentCart: ActionCreator<ClearCurrentCartAction> = () => ({
  type: ShoppingCartActionsEnum.clearCurrentCart
});

export const FetchShoppingCartBooks: ActionCreator<
  FetchShoppingCartBooksAction
> = () => ({
  type: ShoppingCartActionsEnum.fetchShoppingCartBooks
});

export const ResetCartState: ActionCreator<ResetCartStateAction> = () => ({
  type: ShoppingCartActionsEnum.resetCartState
});

export const DeleteCartItem: ActionCreator<DeleteCartItemAction> = (
  payload: string
) => ({
  type: ShoppingCartActionsEnum.deleteCartItem,
  payload
});

export const CartItemDeletedSuccessfully: ActionCreator<
  CartItemDeletedSuccessfullyAction
> = (payload: string) => ({
  type: ShoppingCartActionsEnum.cartItemDeletedSuccessfully,
  payload
});

export const ShoppingCartBooksFetchedSuccessfully: ActionCreator<
  ShoppingCartBooksFetchedSuccessfullyAction
> = () => ({
  type: ShoppingCartActionsEnum.shoppingCartBooksFetchedSuccessfully
});

export const AddShoppingCartBooksToStore: ActionCreator<
  AddShoppingCartBooksToStoreAction
> = (payload: KeyQuantityBooks) => ({
  type: ShoppingCartActionsEnum.addShoppingCartBooksToStore,
  payload
});

export const ResolveShoppingCartBooks: ActionCreator<
  ResolveShoppingCartBooksAction
> = () => ({
  type: ShoppingCartActionsEnum.resolveShoppingCartBooks
});

export type ShoppingCartActions =
  | ShoppingCartBooksResolvedAction
  | ResolveShoppingCartBooksAction
  | AddShoppingCartBooksToStoreAction
  | FetchShoppingCartBooksAction
  | AddBookToShoppingCartAction
  | ClearCurrentCartAction
  | CurrentCartClearedSuccessfullyAction
  | BookAddedToShoppingCartSuccessfullyAction
  | DeleteCartItemAction
  | CartItemQuantityUpdatedSuccessfullyAction
  | UpdateCartItemQuantityAction
  | CartItemDeletedSuccessfullyAction
  | ShoppingCartProceedAction
  | ResetCartStateAction
  | AddBookToShoppingCartErrorAction
  | ShoppingCartBooksFetchedSuccessfullyAction;
