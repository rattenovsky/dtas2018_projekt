import {
  ShoppingCartActions,
  ShoppingCartActionsEnum,
  ShoppingCartBooksFetchedSuccessfully,
  AddShoppingCartBooksToStore,
  ResolveShoppingCartBooks,
  ShoppingCartBooksResolved,
  BookAddedToShoppingCartSuccessfully,
  CurrentCartClearedSuccessfully,
  CartItemDeletedSuccessfully,
  CartItemQuantityUpdatedSuccessfully,
  AddBookToShoppingCartError
} from "./shopping.cart.actions";
import {
  BooksEntityActions,
  AddBooksToStore
} from "@book/store/books.entity.action";
import { ActionsObservable, StateObservable, ofType } from "redux-observable";
import { ApplicationState } from "@store/root/root.reducer";
import { Observable, from, of } from "rxjs";
import {
  mergeMap,
  map,
  concatMap,
  withLatestFrom,
  tap,
  switchMap,
  catchError
} from "rxjs/operators";
import axios from "axios";
import { toData, toPayload } from "@store/root/store.utils";
import { Book } from "@book/store/books.entity.reducer";
import { notification } from "antd";
import { KeyQuantityBooks } from "./shopping.cart.reducer";
import { RouterAction, push } from "connected-react-router";
import {
  CheckoutActions,
  ResetCheckoutState
} from "src/modules/Checkout/store/checkout.actions";

type ShoppingCartEpic = (
  action$: ActionsObservable<ShoppingCartActions>,
  state$: StateObservable<ApplicationState>
) => Observable<
  ShoppingCartActions | BooksEntityActions | RouterAction | CheckoutActions
>;

const fetchShoppingCartBooksEpic: ShoppingCartEpic = (action$, state$) =>
  action$.pipe(
    ofType(ShoppingCartActionsEnum.fetchShoppingCartBooks),
    switchMap(() =>
      from(axios.get("/user/getCart")).pipe(
        map(toData),
        concatMap((books: KeyQuantityBooks) => [
          ShoppingCartBooksFetchedSuccessfully(),
          AddShoppingCartBooksToStore(books),
          ResolveShoppingCartBooks()
        ])
      )
    )
  );

const addBookToShoppingCartEpic: ShoppingCartEpic = (action$, state$) =>
  action$.pipe(
    ofType(ShoppingCartActionsEnum.addBookToShoppingCart),
    map(toPayload),
    mergeMap(({ bookId, silent }: { bookId: string; silent: boolean }) =>
      addBookToCartInDb(bookId).pipe(
        tap(() => {
          if (!silent) {
            notification.success({
              message: "Book added to the cart!",
              description:
                "Book was added to your cart, click on the cart icon to go to checkout!",
              placement: "bottomRight"
            });
          }
        }),
        concatMap(() =>
          silent
            ? [
                BookAddedToShoppingCartSuccessfully(bookId),
                push("/user/checkout")
              ]
            : [BookAddedToShoppingCartSuccessfully(bookId)]
        ),
        catchError(e => {
          notification.error({
            message: "Book could not be added to the cart!",
            description:
              "Something went wrong and the operation was not successful",
            placement: "topRight"
          });
          return of(AddBookToShoppingCartError());
        })
      )
    )
  );

const clearCurrentCartEpic: ShoppingCartEpic = (action$, state$) =>
  action$.pipe(
    ofType(ShoppingCartActionsEnum.clearCurrentCart),
    switchMap(() =>
      from(axios.patch("/user/clearCart")).pipe(
        map(toData),
        concatMap(() => [
          CurrentCartClearedSuccessfully(),
          ResetCheckoutState()
        ])
      )
    )
  );

const deleteCartItemEpic: ShoppingCartEpic = (action$, state$) =>
  action$.pipe(
    ofType(ShoppingCartActionsEnum.deleteCartItem),
    map(toPayload),
    switchMap((bookId: string) =>
      from(axios.patch(`/user/deleteCartItem/${bookId}`)).pipe(
        map(toData),
        map(() => CartItemDeletedSuccessfully(bookId))
      )
    )
  );

const updateCartItemQuantityEpic: ShoppingCartEpic = (action$, state$) =>
  action$.pipe(
    ofType(ShoppingCartActionsEnum.updateCartItemQuantity),
    map(toPayload),
    switchMap(({ bookId, quantity }: { bookId: string; quantity: number }) =>
      from(
        axios.post("/user/updateCartItemQuantity", { bookId, quantity })
      ).pipe(
        map(() => CartItemQuantityUpdatedSuccessfully({ bookId, quantity }))
      )
    )
  );

const resolveShoppingCartBooksEpic: ShoppingCartEpic = (action$, state$) =>
  action$.pipe(
    ofType(ShoppingCartActionsEnum.resolveShoppingCartBooks),
    withLatestFrom(state$, (action, state) => [
      state.shoppingCart.shoppingCartBooks,
      state.booksEntities.result
    ]),
    map(([shoppingCartBooks, BooksInsideStore]: [KeyQuantityBooks, string[]]) =>
      Object.keys(shoppingCartBooks).filter(
        shoppingCartBookId => !BooksInsideStore.includes(shoppingCartBookId)
      )
    ),
    mergeMap((missingIds: string[]) =>
      getMissingBooks(missingIds).pipe(
        concatMap((missingBooks: Book[]) => [
          AddBooksToStore(missingBooks),
          ShoppingCartBooksResolved()
        ])
      )
    )
  );

const getMissingBooks = (missingBooksIds: string[]) =>
  missingBooksIds.length > 0
    ? from(
        axios.get(
          "/book/multiple?" + missingBooksIds.map(id => `id=${id}`).join("&")
        )
      ).pipe(map(toData))
    : of([]);

const addBookToCartInDb = (bookId: string) =>
  from(axios.post("/user/addToCart", { book: bookId })).pipe(map(toData));

export const ShoppingCartEpic = [
  fetchShoppingCartBooksEpic,
  resolveShoppingCartBooksEpic,
  addBookToShoppingCartEpic,
  clearCurrentCartEpic,
  deleteCartItemEpic,
  updateCartItemQuantityEpic
];
