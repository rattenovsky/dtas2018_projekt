import {
  ShoppingCartActions,
  ShoppingCartActionsEnum
} from "./shopping.cart.actions";

export interface ShoppingCartState {
  shoppingCartBooks: KeyQuantityBooks;
  hasResolvedBooks: boolean;
  isReachingToAnApi: boolean;
}

export const initialShoppingCartState: ShoppingCartState = {
  shoppingCartBooks: {},
  hasResolvedBooks: false,
  isReachingToAnApi: false
};

export interface KeyQuantityBooks {
  [key: string]: number;
}

export function ShoppingCartReducer(
  state: ShoppingCartState = initialShoppingCartState,
  action: ShoppingCartActions
): ShoppingCartState {
  const setIsReachingToAndApi = (isReachingToAnApi: boolean) => {
    return {
      ...state,
      isReachingToAnApi
    };
  };
  switch (action.type) {
    case ShoppingCartActionsEnum.addBookToShoppingCart:
      return {
        ...state,
        isReachingToAnApi: true
      };
    case ShoppingCartActionsEnum.bookAddedToShoppingCartSuccessfully:
      const quantityOfBookInsideStore = state.shoppingCartBooks[action.payload];
      return {
        ...state,
        isReachingToAnApi: false,
        shoppingCartBooks: {
          ...state.shoppingCartBooks,
          [action.payload]:
            quantityOfBookInsideStore !== undefined
              ? quantityOfBookInsideStore + 1
              : 1
        }
      };
    case ShoppingCartActionsEnum.addShoppingCartBooksToStore:
      return {
        ...state,
        shoppingCartBooks: action.payload
      };
    case ShoppingCartActionsEnum.clearCurrentCart:
    case ShoppingCartActionsEnum.deleteCartItem:
    case ShoppingCartActionsEnum.updateCartItemQuantity:
      return setIsReachingToAndApi(true);
    case ShoppingCartActionsEnum.currentCartClearedSuccessfully:
    case ShoppingCartActionsEnum.resetCartState:
      return {
        ...initialShoppingCartState,
        hasResolvedBooks: true
      };
    case ShoppingCartActionsEnum.cartItemQuantityUpdatedSuccessfully:
      return {
        ...setIsReachingToAndApi(false),
        shoppingCartBooks: {
          ...state.shoppingCartBooks,
          [action.payload.bookId]: action.payload.quantity
        }
      };
    case ShoppingCartActionsEnum.cartItemDeletedSuccessfully: {
      const shoppingCartBooks = state.shoppingCartBooks;
      delete shoppingCartBooks[action.payload];
      return {
        ...setIsReachingToAndApi(false),
        // using concise syntax is not working, it seems that redux does not think that that object really changed which is kinda obvious
        shoppingCartBooks: { ...shoppingCartBooks }
      };
    }
    case ShoppingCartActionsEnum.shoppingCartBooksResolved:
      return {
        ...state,
        hasResolvedBooks: true
      };
    case ShoppingCartActionsEnum.addBookToShoppingCartError:
      return {
        ...state,
        isReachingToAnApi: false
      };
    default:
      return state;
  }
}
