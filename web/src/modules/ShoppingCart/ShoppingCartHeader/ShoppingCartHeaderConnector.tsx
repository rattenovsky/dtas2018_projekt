import * as React from "react";
import { ShoppingCartHeaderView } from "./ShoppingCartHeaderView";
import { ApplicationState } from "@store/root/root.reducer";
import { connect } from "react-redux";
import {
  NormalizedBookEntity,
  NormalizedBook
} from "@book/store/books.entity.reducer";
import { KeyQuantityBooks } from "@shoppingCart/store/shopping.cart.reducer";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { compose } from "redux";

interface State {
  popupVisible: boolean;
}

interface StateProps {
  hasResolvedBooks: boolean;
  bookEntities: NormalizedBookEntity["book"];
  shoppingCartBooks: KeyQuantityBooks;
}

export class C extends React.PureComponent<
  StateProps & RouteComponentProps,
  State
> {
  public state = {
    popupVisible: false
  };

  public render() {
    return (
      <ShoppingCartHeaderView
        onCarItemClick={this.onCarItemClickHandler}
        isLoading={!this.props.hasResolvedBooks}
        books={Object.keys(this.props.shoppingCartBooks).map(
          shoppingCartBookId => this.props.bookEntities[shoppingCartBookId]
        )}
        popupVisible={this.state.popupVisible}
        popupVisibilityToggler={this.togglePopupVisibility}
      />
    );
  }

  private togglePopupVisibility = () => {
    this.setState(({ popupVisible }) => ({ popupVisible: !popupVisible }));
  };

  private onCarItemClickHandler = (book: NormalizedBook) => {
    this.setState({ popupVisible: false }, () =>
      this.props.history.push(`/book/${book.id}`)
    );
  };
}

const mapStateToProps = ({
  shoppingCart,
  booksEntities
}: ApplicationState): StateProps => ({
  hasResolvedBooks: shoppingCart.hasResolvedBooks,
  bookEntities: booksEntities.entities.book,
  shoppingCartBooks: shoppingCart.shoppingCartBooks
});

export const ShoppingCartHeaderConnector = compose(
  withRouter,
  connect<StateProps, {}>(
    mapStateToProps,
    {}
  )
)(C);
