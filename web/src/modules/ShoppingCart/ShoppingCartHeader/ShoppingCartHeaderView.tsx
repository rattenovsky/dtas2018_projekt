import * as React from "react";
import { Badge, Icon, List } from "antd";
import "./ShoppingCartHeader.css";
import posed from "react-pose";
import { useMedia } from "@shared/Hooks/useMedia";
import { NormalizedBook } from "@book/store/books.entity.reducer";
import { PosedAppLoader } from "@utils/Loaders/AppLoader/PosedAppLoader";
import { routerHistory } from "src";

const PosedCartBooksWrapper = posed.div({
  visible: {
    opacity: 1,
    transform: "scale(1)",
    transition: {
      opacity: { ease: "easeOut", duration: 400 },
      transform: { ease: "linear", duration: 100 }
    }
  },
  hidden: {
    opacity: 0,
    transform: "scale(0)",
    transition: {
      opacity: { ease: "easeOut", duration: 100 },
      transform: { ease: "linear", duration: 400 }
    }
  }
});

interface Props {
  isLoading: boolean;
  books: NormalizedBook[];
  popupVisible: boolean;
  popupVisibilityToggler: VoidFunction;
  onCarItemClick: (book: NormalizedBook) => void;
}

export const ShoppingCartHeaderView: React.SFC<Props> = ({
  popupVisible,
  popupVisibilityToggler,
  isLoading,
  books,
  onCarItemClick
}) => {
  function onPoseClickLocalHandler(e: React.MouseEvent<any>) {
    e.stopPropagation();
  }

  let popupState = "";

  const isOnMobile = useMedia("(max-width: 400px)");

  const popupRef = React.createRef<HTMLDivElement>();

  function shouldDisplayNoResults() {
    return !isLoading && books.length === 0;
  }
  function shouldDisplayLoading() {
    return isLoading;
  }

  function shouldDisplayResults() {
    return !isLoading && books.length > 0;
  }

  function onHeaderIconClick(e: React.MouseEvent<any>) {
    e.stopPropagation();
    e.preventDefault();
    popupVisibilityToggler();
  }

  function onPoseCompleteHandler(state: string) {
    const { current } = popupRef;
    if (!current) {
      return;
    }
    state === "visible" ? current.focus() : current && current.blur();
    popupState = state;
  }
  return (
    <div
      className="ShoppingCartHeaderWrapper FlexFullCentered"
      style={{ height: "100%", padding: "0 20px" }}
      onMouseDown={onHeaderIconClick}
    >
      <Badge count={isLoading ? 0 : books.length}>
        <Icon
          type="shopping-cart"
          theme="outlined"
          className={"ShoppingCartIcon " + (isLoading ? "loading" : "")}
          style={{ fontSize: 24, margin: 0 }}
        />
      </Badge>
      <PosedCartBooksWrapper
        onPoseComplete={onPoseCompleteHandler}
        onClick={onPoseClickLocalHandler}
        pose={popupVisible ? "visible" : "hidden"}
        className="CartBooksPopupWrapper"
      >
        {isOnMobile && popupVisible ? (
          <style>
            {`body {
                 overflow: hidden;
            }`}
          </style>
        ) : null}

        <div
          // tslint:disable-next-line:jsx-no-lambda
          onBlur={() => {
            if (popupState === "visible") {
              popupVisibilityToggler();
            }
          }}
          tabIndex={0}
          ref={popupRef}
          style={{ width: "100%", height: "100%", outline: "none" }}
        >
          <PosedAppLoader isVisible={shouldDisplayLoading()} />
          {shouldDisplayNoResults() ? <NoBooksInCartView /> : null}
          {shouldDisplayResults() ? (
            <BooksInCartView books={books} onCarItemClick={onCarItemClick} />
          ) : null}
        </div>
      </PosedCartBooksWrapper>
    </div>
  );
};

const BooksInCartView: React.SFC<{
  books: NormalizedBook[];
  onCarItemClick: (book: NormalizedBook) => void;
}> = ({ books, onCarItemClick }) => {
  function renderBook(normalizedBook: NormalizedBook) {
    return (
      <a
        // tslint:disable-next-line:jsx-no-lambda
        onMouseDown={e => {
          e.stopPropagation();
          e.preventDefault();
          onCarItemClick(normalizedBook);
        }}
      >
        <List.Item style={{ borderBottom: "1px solid #e8e8e8" }}>
          <List.Item.Meta
            title={normalizedBook.title}
            description={normalizedBook.description}
          />
        </List.Item>
      </a>
    );
  }
  function onCheckoutClick(e: any) {
    routerHistory.push("/user/checkout");
  }
  return (
    <div className="BooksInCartWrapper">
      <div className="PopupBookItemsWrapper">
        <List
          itemLayout="horizontal"
          dataSource={books}
          renderItem={renderBook}
        />
      </div>
      <div className="PopupCheckout" onMouseDown={onCheckoutClick}>
        <span>Go to checkout</span>
      </div>
    </div>
  );
};

const NoBooksInCartView: React.SFC = () => (
  <div className="NoBooksInCartWrapper FlexFullCentered">
    <img src="https://gw.alipayobjects.com/zos/rmsportal/RVRUAYdCGeYNBWoKiIwB.svg" />
    <span>Cart is empty</span>
  </div>
);
