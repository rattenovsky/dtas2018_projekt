import * as React from "react";
import { Icon, Divider, Menu, Dropdown, InputNumber } from "antd";
import { useMedia } from "@shared/Hooks/useMedia";

import { defaultBookCover } from "@book/BookCard/BookCardView";
import { ImageZoomer } from "@shared/ImageZoomer/ImageZoomer";
import { CheckoutBook } from "../ShoppingCartCheckoutConnector";

interface Props {
  books: CheckoutBook[];
  clearCurrentCart: () => void;
  onCartItemDeleted: (bookId: string) => void;
  onCartItemUpdated: (
    { bookId, quantity }: { bookId: string; quantity: number }
  ) => void;
  booksCost: number;
}

export const CheckoutItems: React.SFC<Props> = props => {
  const isOnMobile = useMedia("(max-width: 600px)");

  function renderMobileMenu() {
    return (
      <Menu>
        <Menu.Item key="0">
          <a onClick={props.clearCurrentCart}>Clear all</a>
        </Menu.Item>
      </Menu>
    );
  }

  function renderMobileDropdown() {
    return (
      <Dropdown overlay={renderMobileMenu()} trigger={["click"]}>
        <a className="ant-dropdown-link FlexFullCentered">
          Options
          <Icon type="down" style={{ marginLeft: 5 }} />
        </a>
      </Dropdown>
    );
  }

  return (
    <div className="TileInner ShoppingCartCheckoutTile">
      <div className="TileUpAndDownHeadingContainer">
        <h1>Your Basket ({props.books.length})</h1>
        {isOnMobile ? (
          renderMobileDropdown()
        ) : (
          <div className="ShoppingCartCheckoutItemsIcons">
            <a className="FlexFullCentered" onClick={props.clearCurrentCart}>
              Clear all <Icon type="close" />
            </a>
          </div>
        )}
      </div>
      <div className="ShoppingCartCheckoutItemsList">{renderList()}</div>

      <Divider style={{ margin: 0 }} />
      <div
        className="TileUpAndDownHeadingContainer"
        style={{ justifyContent: "flex-end" }}
      >
        <h2>Total: {props.booksCost} zl</h2>
      </div>
    </div>
  );

  function onItemDeletedLocalHandler(itemId: string) {
    props.onCartItemDeleted(itemId);
  }

  function renderList() {
    return props.books.map(book => {
      let currentInputValue = book.quantity;
      let prevInputValue = book.quantity;

      const onChangeHandler = (value: number | string) => {
        const parsedNumber = Number(value);
        isNaN(parsedNumber)
          ? (currentInputValue = book.quantity)
          : (currentInputValue = parsedNumber);
      };

      const decideAndUpdate = () => {
        if (currentInputValue <= 0 || currentInputValue > 30) {
          currentInputValue = book.quantity;
        }

        if (prevInputValue !== currentInputValue) {
          prevInputValue = currentInputValue;
          props.onCartItemUpdated({
            bookId: book.id,
            quantity: currentInputValue
          });
        }
      };

      const onBlur = (event: React.FocusEvent<HTMLInputElement>) => {
        decideAndUpdate();
      };

      const onDelete = () => {
        onItemDeletedLocalHandler(book.id);
      };

      const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
          decideAndUpdate();
        }
      };

      return (
        <div className="CheckoutListItemWrapper" key={book.id}>
          <div className="CheckoutListItemImageWrapper">
            {book.covers.length === 0 ? (
              <ImageZoomer
                imageSrc={defaultBookCover.normal}
                style={{ width: 64, height: 64 }}
              >
                <img src={defaultBookCover.normal} />
              </ImageZoomer>
            ) : (
              <ImageZoomer
                imageSrc={book.covers[0].normal}
                style={{ width: 64, height: 64 }}
              >
                <img src={book.covers[0].normal} />
              </ImageZoomer>
            )}
          </div>
          <div style={{ paddingRight: 12 }}>
            <div>
              <b>{book.title}</b>
            </div>
            <div className="BookItemDescription">{book.description}</div>
            <div className="BookItemPrice">
              <b>Price: {book.price} zl</b>
            </div>
          </div>

          <div className="BookItemControls">
            <InputNumber
              onKeyDown={onKeyDown}
              onBlur={onBlur}
              min={1}
              max={30}
              value={currentInputValue}
              onChange={onChangeHandler}
              defaultValue={book.quantity}
            />
            <Icon
              onClick={onDelete}
              className="BookItemControlsIcon"
              type="delete"
              theme="twoTone"
            />
          </div>
        </div>
      );
    });
  }
};
