import * as React from "react";
import "./ShoppingCartCheckout.css";
import { Affix, Alert } from "antd";
import { CheckoutItems } from "./CheckoutItems/CheckoutItems";
import { CheckoutShipping } from "./CheckoutShipping/CheckoutShipping";
import { CheckoutPayment } from "./CheckoutPayment/CheckoutPayment";
import { CheckoutOrderPanel } from "./CheckoutOrder/CheckoutOrderPanel";
import { useMedia } from "@shared/Hooks/useMedia";
import {
  onCheckoutRadioSelectedHandler,
  CheckoutBook
} from "./ShoppingCartCheckoutConnector";
import "@shared/Css/Tile.css";

interface Props {
  onPaymentSelected: onCheckoutRadioSelectedHandler;
  onShippingSelected: onCheckoutRadioSelectedHandler;
  hasPaymentErrors: boolean;
  hasShippingErrors: boolean;
  selectedShipping: string;
  selectedPayment: string;
  onProceedHandler: (event: any) => void;
  clearCurrentCart: () => void;
  onCartItemDeleted: (bookId: string) => void;
  onCartItemUpdated: (
    { bookId, quantity }: { bookId: string; quantity: number }
  ) => void;
  books: CheckoutBook[];
}
export const ShoppingCartCheckoutView: React.SFC<Props> = props => {
  const isOrderPanelPinned = useMedia("(min-width: 1100px)");
  const booksCost = props.books.reduce(
    (acc, book) => acc + book.price * book.quantity,
    0
  );
  return (
    <div className="ShoppingCartCheckoutGlobalWrapper">
      <div style={{ flex: 1 }}>
        <CheckoutItems
          booksCost={booksCost}
          onCartItemUpdated={props.onCartItemUpdated}
          onCartItemDeleted={props.onCartItemDeleted}
          books={props.books}
          clearCurrentCart={props.clearCurrentCart}
        />
        <CheckoutShipping
          shippingValue={props.selectedShipping}
          hasShippingErrors={props.hasShippingErrors}
          onShippingSelected={props.onShippingSelected}
        />
        {props.hasShippingErrors ? renderErrorAlert("Shipping") : null}
        <CheckoutPayment
          paymentValue={props.selectedPayment}
          hasPaymentErrors={props.hasPaymentErrors}
          onPaymentSelected={props.onPaymentSelected}
        />
        {props.hasPaymentErrors ? renderErrorAlert("Payment") : null}
        {!isOrderPanelPinned ? (
          <CheckoutOrderPanel
            inTotalPrice={booksCost}
            onProceedHandler={props.onProceedHandler}
            selectedPayment={props.selectedPayment}
            selectedShipping={props.selectedShipping}
          />
        ) : null}
      </div>
      {isOrderPanelPinned ? (
        <Affix offsetTop={88}>
          <CheckoutOrderPanel
            inTotalPrice={booksCost}
            onProceedHandler={props.onProceedHandler}
            selectedPayment={props.selectedPayment}
            selectedShipping={props.selectedShipping}
          />
        </Affix>
      ) : null}
    </div>
  );

  function renderErrorAlert(type: string) {
    return (
      <Alert
        type="error"
        showIcon={true}
        message={`Please Pick ${type} Method`}
        style={{ marginTop: 20 }}
      />
    );
  }
};
