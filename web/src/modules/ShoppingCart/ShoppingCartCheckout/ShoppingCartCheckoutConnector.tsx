import * as React from "react";
import { ShoppingCartCheckoutView } from "./ShoppingCartCheckoutView";
import { RadioChangeEvent } from "antd/lib/radio";

import { ApplicationState } from "@store/root/root.reducer";
import { connect } from "react-redux";
import { Book, getHydratedBook } from "@book/store/books.entity.reducer";
import "./ShoppingCartCheckout.css";
import { Dispatch } from "redux";
import {
  ShoppingCartActions,
  ClearCurrentCart,
  DeleteCartItem,
  UpdateCartItemQuantity
} from "@shoppingCart/store/shopping.cart.actions";
import { PosedAppLoader } from "@utils/Loaders/AppLoader/PosedAppLoader";

import { routerHistory } from "src";
import {
  CheckoutActions,
  SetPayment,
  SetShipping,
  IncrementStep
} from "src/modules/Checkout/store/checkout.actions";

export type onCheckoutRadioSelectedHandler = (
  changeEventData: RadioChangeEvent
) => void;
interface State {
  additionalCosts: number;
  hasShippingErrors: boolean;
  hasPaymentErrors: boolean;
}

export interface CheckoutBook extends Book {
  quantity: number;
}

interface StateProps {
  books: CheckoutBook[];
  hasResolvedBooks: boolean;
  isReachingToAnApi: boolean;
  payment: string;
  shipping: string;
}

interface DispatchProps {
  clearCurrentCart: () => void;
  deleteCartItem: (bookId: string) => void;
  updateCartItemQuantity: (
    { bookId, quantity }: { bookId: string; quantity: number }
  ) => void;
  incrementCheckoutStep: () => void;
  setPayment: (payment: string) => void;
  setShipping: (shipping: string) => void;
}

export class C extends React.Component<StateProps & DispatchProps, State> {
  public state: State = {
    additionalCosts: 0,
    hasShippingErrors: false,
    hasPaymentErrors: false
  };

  public render() {
    if (!this.props.hasResolvedBooks) {
      return this.renderLoading();
    } else if (this.props.hasResolvedBooks && this.props.books.length > 0) {
      return this.renderComponentView();
    } else {
      return this.renderFallBack();
    }
  }

  private onPaymentSelected: onCheckoutRadioSelectedHandler = ({
    target: { value }
  }) => {
    this.setState(
      {
        hasPaymentErrors: false
      },
      () => this.props.setPayment(value)
    );
  };

  private onShippingSelected: onCheckoutRadioSelectedHandler = ({
    target: { value }
  }) => {
    this.setState(
      {
        hasShippingErrors: false
      },
      () => this.props.setShipping(value)
    );
  };

  private validateShippingAndPayment(): {
    errors: boolean;
    specificErrors: Partial<State>;
  } {
    return {
      errors: this.props.shipping === null || this.props.payment === null,
      specificErrors: {
        hasPaymentErrors: this.props.payment === null,
        hasShippingErrors: this.props.shipping === null
      }
    };
  }

  private onProceedHandler = () => {
    const validatedRadios = this.validateShippingAndPayment();
    this.setState(
      {
        ...this.state,
        ...validatedRadios.specificErrors
      },
      () => {
        if (!validatedRadios.errors) {
          this.props.incrementCheckoutStep();
        }
      }
    );
  };

  private onCartItemDeletedHandler = (bookId: string) => {
    this.props.deleteCartItem(bookId);
  };

  private onCartItemUpdatedHandler = ({
    bookId,
    quantity
  }: {
    bookId: string;
    quantity: number;
  }) => {
    this.props.updateCartItemQuantity({ bookId, quantity });
  };

  private renderComponentView() {
    return (
      <React.Fragment>
        <PosedAppLoader
          isVisible={this.props.isReachingToAnApi}
          posedDivStyle={{ position: "fixed", height: "calc(100vh - 64px)" }}
        />
        <ShoppingCartCheckoutView
          onCartItemUpdated={this.onCartItemUpdatedHandler}
          onCartItemDeleted={this.onCartItemDeletedHandler}
          clearCurrentCart={this.props.clearCurrentCart}
          books={this.props.books}
          selectedPayment={this.props.payment}
          selectedShipping={this.props.shipping}
          onProceedHandler={this.onProceedHandler}
          onShippingSelected={this.onShippingSelected}
          onPaymentSelected={this.onPaymentSelected}
          hasPaymentErrors={this.state.hasPaymentErrors}
          hasShippingErrors={this.state.hasShippingErrors}
        />
      </React.Fragment>
    );
  }

  private renderLoading() {
    return (
      <PosedAppLoader
        posedDivStyle={{ height: "calc(100vh - 64px)" }}
        isVisible={true}
      />
    );
  }

  private renderFallBack() {
    const onLinkClick = () => {
      routerHistory.push("/home");
    };
    return (
      <div className="ShoppingCartCheckoutGlobalWrapper">
        <div
          className="ShoppingCartCheckoutTileWrapper"
          style={{ padding: 24 }}
        >
          <h1 style={{ margin: 0, color: "rgba(0, 0, 0, 0.7)" }}>
            Looks like the cart is empty
          </h1>
          <span>
            You can browse books on the <a onClick={onLinkClick}>Home page</a>
          </span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({
  shoppingCart,
  booksEntities,
  checkout
}: ApplicationState): StateProps => ({
  books: Object.keys(shoppingCart.shoppingCartBooks).map(bookId => ({
    ...getHydratedBook(booksEntities, bookId),
    quantity: shoppingCart.shoppingCartBooks[bookId]
  })),
  hasResolvedBooks: shoppingCart.hasResolvedBooks,
  isReachingToAnApi: shoppingCart.isReachingToAnApi,
  shipping: checkout.shipping,
  payment: checkout.payment
});

const mapDispatchToProps = (
  dispatch: Dispatch<ShoppingCartActions | CheckoutActions>
): DispatchProps => ({
  clearCurrentCart: () => dispatch(ClearCurrentCart()),
  deleteCartItem: bookId => dispatch(DeleteCartItem(bookId)),
  updateCartItemQuantity: ({ bookId, quantity }) =>
    dispatch(UpdateCartItemQuantity({ bookId, quantity })),
  incrementCheckoutStep: () => dispatch(IncrementStep()),
  setPayment: (payment: string) => dispatch(SetPayment(payment)),
  setShipping: (shipping: string) => dispatch(SetShipping(shipping))
});

export const ShoppingCartCheckoutConnector = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(C);
