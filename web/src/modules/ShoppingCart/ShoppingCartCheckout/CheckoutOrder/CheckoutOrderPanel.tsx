import * as React from "react";

import { Button } from "antd";

interface Props {
  onProceedHandler: (event: any) => void;
  selectedPayment: string;
  selectedShipping: string;
  inTotalPrice: number;
}

export const CheckoutOrderPanel: React.SFC<Props> = props => {
  return (
    <div className="ShoppingCartCheckoutOrderPanelWrapper">
      <div className="ShoppingCartCheckoutOrderPanelInner">
        <div className="ShoppingCartCheckoutOrderPanelShippingInfo">
          <div className="ShoppingCartCheckoutNonListItem">
            <span>Shipping</span>
            {props.selectedShipping === null ? (
              <a>Choose</a>
            ) : (
              <b>{props.selectedShipping}</b>
            )}
          </div>
          <div className="ShoppingCartCheckoutNonListItem">
            <span>Payment</span>
            {props.selectedPayment === null ? (
              <a>Choose</a>
            ) : (
              <b>{props.selectedPayment}</b>
            )}
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <h1>In total</h1>
          <h1>
            {props.inTotalPrice}
            zł
          </h1>
        </div>
        <Button
          className="CheckoutOrderButton"
          tabIndex={0}
          block={true}
          size="large"
          type="primary"
          onClick={props.onProceedHandler}
          style={{ marginBottom: 12 }}
        >
          Proceed
        </Button>
      </div>
    </div>
  );
};
