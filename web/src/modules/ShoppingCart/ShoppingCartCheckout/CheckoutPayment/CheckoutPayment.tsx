import * as React from "react";
import { Radio } from "antd";
import RadioGroup from "antd/lib/radio/group";
import { onCheckoutRadioSelectedHandler } from "../ShoppingCartCheckoutConnector";

interface Props {
  hasPaymentErrors: boolean;
  onPaymentSelected: onCheckoutRadioSelectedHandler;
  paymentValue: string;
}

export const CheckoutPayment: React.SFC<Props> = props => {
  return (
    <div
      className={
        "TileInner ShoppingCartCheckoutTile ShippingWrapper " +
        (props.hasPaymentErrors ? "CheckoutError" : "")
      }
    >
      <div className="TileUpAndDownHeadingContainer">
        <h1>Payment</h1>
      </div>
      <div className="ShoppingCartCheckoutPanelInner">
        <RadioGroup
          value={props.paymentValue}
          style={{ width: "100%" }}
          onChange={props.onPaymentSelected}
        >
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="Visa">
              <b>Visa</b>
            </Radio>
            <span>0.00 zł</span>
          </div>
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="MasterPass">
              <b>MasterPass</b>
            </Radio>
            <span>0.00 zł</span>
          </div>
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="Przelew">
              <b>Przelew</b>
            </Radio>
            <span>0.00 zł</span>
          </div>
        </RadioGroup>
      </div>
    </div>
  );
};
