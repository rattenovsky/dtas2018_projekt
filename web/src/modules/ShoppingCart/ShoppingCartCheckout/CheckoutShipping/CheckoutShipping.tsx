import * as React from "react";
import { Radio } from "antd";
import RadioGroup from "antd/lib/radio/group";
import { onCheckoutRadioSelectedHandler } from "../ShoppingCartCheckoutConnector";

interface Props {
  hasShippingErrors: boolean;
  onShippingSelected: onCheckoutRadioSelectedHandler;
  shippingValue: string;
}

export const CheckoutShipping: React.SFC<Props> = props => {
  return (
    <div
      className={
        "TileInner ShoppingCartCheckoutTile ShippingWrapper " +
        (props.hasShippingErrors ? "CheckoutError" : "")
      }
    >
      <div className="TileUpAndDownHeadingContainer">
        <h1>Shipping</h1>
      </div>
      <div className="ShoppingCartCheckoutPanelInner">
        <RadioGroup
          value={props.shippingValue}
          style={{ width: "100%" }}
          onChange={props.onShippingSelected}
        >
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="DHL">
              <b>DHL</b>
            </Radio>

            <span>0.00 zł</span>
          </div>
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="Poczta Polska">
              <b>Poczta Polska</b>
            </Radio>
            <span>0.00 zł</span>
          </div>
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="UPS">
              <b>UPS</b>
            </Radio>
            <span>0.00 zł</span>
          </div>
          <div className="ShoppingCartCheckoutNonListItem">
            <Radio value="TNT">
              <b>TNT</b>
            </Radio>
            <span>0.00 zł</span>
          </div>
        </RadioGroup>
      </div>
    </div>
  );
};
