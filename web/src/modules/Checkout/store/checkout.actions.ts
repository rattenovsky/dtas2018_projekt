import { ActionCreator } from "redux";
import { CheckoutShippingFormValues } from "@checkout/CheckoutShippingForm/CheckoutShippingFormView";

export enum CheckoutActionsEnum {
  incrementStep = "@@CheckoutComponents INCREMENT_STEP",
  decrementStep = "@@CheckoutComponents DECREMENT_STEP",
  setStep = "@@Checkout SET_STEP",
  setShipping = "@@ShoppingCartCheckout SET_SHIPPING",
  setPayment = "@@ShoppingCartCheckout SET_PAYMENT",
  updateCheckoutShippingFormValues = "@@CheckoutShippingFormView UPDATE_CHECKOUT_SHIPPING_FORM_VALUES",
  resetCheckoutState = "@@AuthEpics RESET_CHECKOUT_STATE",
  setHasOrdered = "@@CheckoutShippingForm SET_HAS_ORDERED"
}

interface SetShippingAction {
  type: CheckoutActionsEnum.setShipping;
  payload: string;
}

interface ResetCheckoutStateAction {
  type: CheckoutActionsEnum.resetCheckoutState;
}

interface UpdateCheckoutShippingFormValuesAction {
  type: CheckoutActionsEnum.updateCheckoutShippingFormValues;
  payload: CheckoutShippingFormValues;
}

interface SetHasOrderedAction {
  type: CheckoutActionsEnum.setHasOrdered;
  payload: boolean;
}

interface SetPaymentAction {
  type: CheckoutActionsEnum.setPayment;
  payload: string;
}

interface IncrementStepAction {
  type: CheckoutActionsEnum.incrementStep;
}

interface DecrementStepAction {
  type: CheckoutActionsEnum.decrementStep;
}

interface SetStepAction {
  type: CheckoutActionsEnum.setStep;
  payload: 0 | 1;
}

export const IncrementStep: ActionCreator<IncrementStepAction> = () => ({
  type: CheckoutActionsEnum.incrementStep
});

export const DecrementStep: ActionCreator<DecrementStepAction> = () => ({
  type: CheckoutActionsEnum.decrementStep
});

export const SetHasOrdered: ActionCreator<SetHasOrderedAction> = (
  payload: boolean
) => ({
  type: CheckoutActionsEnum.setHasOrdered,
  payload
});

export const SetShipping: ActionCreator<SetShippingAction> = (
  payload: string
) => ({
  type: CheckoutActionsEnum.setShipping,
  payload
});

export const UpdateCheckoutShippingFormValues: ActionCreator<
  UpdateCheckoutShippingFormValuesAction
> = (payload: CheckoutShippingFormValues) => ({
  type: CheckoutActionsEnum.updateCheckoutShippingFormValues,
  payload
});

export const ResetCheckoutState: ActionCreator<
  ResetCheckoutStateAction
> = () => ({
  type: CheckoutActionsEnum.resetCheckoutState
});

export const SetPayment: ActionCreator<SetPaymentAction> = (
  payload: string
) => ({
  type: CheckoutActionsEnum.setPayment,
  payload
});

export const SetStep: ActionCreator<SetStepAction> = (payload: 0 | 1) => ({
  type: CheckoutActionsEnum.setStep,
  payload
});

export type CheckoutActions =
  | IncrementStepAction
  | SetPaymentAction
  | SetShippingAction
  | DecrementStepAction
  | UpdateCheckoutShippingFormValuesAction
  | SetStepAction
  | SetHasOrderedAction
  | ResetCheckoutStateAction;
