import { CheckoutActions, CheckoutActionsEnum } from "./checkout.actions";
import {
  initialCheckoutShippingFormValues,
  CheckoutShippingFormValues
} from "@checkout/CheckoutShippingForm/CheckoutShippingFormView";

export interface CheckoutState {
  shipping: string;
  payment: string;
  checkoutShippingForm: CheckoutShippingFormValues;
  currentStep: number;
  hasOrdered: boolean;
}

export const initialCheckoutState: CheckoutState = {
  shipping: null,
  payment: null,
  checkoutShippingForm: initialCheckoutShippingFormValues,
  currentStep: 0,
  hasOrdered: false
};

export function CheckoutReducer(
  state: CheckoutState = initialCheckoutState,
  action: CheckoutActions
): CheckoutState {
  switch (action.type) {
    case CheckoutActionsEnum.setHasOrdered:
      return {
        ...state,
        hasOrdered: action.payload
      };
    case CheckoutActionsEnum.resetCheckoutState:
      return {
        ...initialCheckoutState,
        hasOrdered: state.hasOrdered
      };
    case CheckoutActionsEnum.incrementStep:
      return {
        ...state,
        currentStep: state.currentStep + 1
      };
    case CheckoutActionsEnum.decrementStep:
      return {
        ...state,
        currentStep: state.currentStep - 1
      };
    case CheckoutActionsEnum.setStep:
      return {
        ...state,
        currentStep: action.payload
      };
    case CheckoutActionsEnum.setPayment:
      return {
        ...state,
        payment: action.payload
      };
    case CheckoutActionsEnum.setShipping:
      return {
        ...state,
        shipping: action.payload
      };
    case CheckoutActionsEnum.updateCheckoutShippingFormValues:
      return {
        ...state,
        checkoutShippingForm: action.payload
      };
    default:
      return state;
  }
}
