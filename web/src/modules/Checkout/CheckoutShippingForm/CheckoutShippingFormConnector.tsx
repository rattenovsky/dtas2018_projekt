import * as React from "react";
import {
  CheckoutShippingFormView,
  CheckoutShippingFormValues
} from "./CheckoutShippingFormView";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  CheckoutActions,
  IncrementStep,
  DecrementStep,
  UpdateCheckoutShippingFormValues,
  ResetCheckoutState,
  SetHasOrdered
} from "../store/checkout.actions";
import { ApplicationState } from "@store/root/root.reducer";
import { CheckoutState } from "../store/checkout.reducer";
import {
  ShoppingCartState,
  KeyQuantityBooks
} from "@shoppingCart/store/shopping.cart.reducer";
import axios from "axios";
import {
  ResetCartState,
  ShoppingCartActions
} from "@shoppingCart/store/shopping.cart.actions";
import { NormalizedBookEntity } from "@book/store/books.entity.reducer";
import { Alert } from "antd";
import { Order } from "@user/store/user.reducer";
import { normalize } from "normalizr";
import { orderListSchema } from "@user/store/user.epics";
import {
  AddOrdersPayload,
  AddOrdersToStore,
  UserActions
} from "@user/store/user.actions";
import "./CheckoutShippingForm.css";
interface DispatchProps {
  incrementStep: VoidFunction;
  decrementStep: VoidFunction;
  updateCheckoutShippingFormValues: (
    values: CheckoutShippingFormValues
  ) => void;
  resetCheckoutState: VoidFunction;
  resetCartState: VoidFunction;
  setHasOrdered: (hasOrdered: boolean) => void;
  addOrderToStore: (payload: AddOrdersPayload) => void;
}

interface StateProps {
  checkoutState: CheckoutState;
  cartState: ShoppingCartState;
  overallCartPrice: number;
}

interface StateProps {
  checkoutShippingFormValues: CheckoutShippingFormValues;
}

interface State {
  isLoading: boolean;
  hasErrors: boolean;
}

class C extends React.Component<DispatchProps & StateProps, State> {
  public state = {
    isLoading: false,
    hasErrors: false
  };
  public render() {
    return (
      <div className="CheckoutShippingFormWrapper">
        <CheckoutShippingFormView
          isLoading={this.state.isLoading}
          initialFormValues={this.props.checkoutShippingFormValues}
          updateCheckoutFormValues={this.props.updateCheckoutShippingFormValues}
          submit={this.onSubmitHandler}
          decrementStep={this.props.decrementStep}
        />
        {this.state.hasErrors && !this.state.isLoading ? (
          <Alert
            message="Error"
            description="Could not finish the order, please try again"
            type="error"
            showIcon={true}
          />
        ) : null}
      </div>
    );
  }

  private onSubmitHandler = (values: CheckoutShippingFormValues) => {
    this.setState({ isLoading: true });
    this.handleOrder(values)
      .then(res => {
        this.setState({ isLoading: false, hasErrors: false }, () =>
          this.onOrderSuccessCallback(res.data)
        );
      })
      .catch(() => {
        this.setState({ hasErrors: true, isLoading: false });
      });
  };

  private onOrderSuccessCallback(orderData: Order) {
    const normalizedOrder = normalize([orderData], orderListSchema);
    this.props.addOrderToStore(normalizedOrder);
    this.props.setHasOrdered(true);
    this.props.resetCartState();
    this.props.resetCheckoutState();
  }

  private handleOrder(shippingInfo: CheckoutShippingFormValues) {
    return axios.post("/order", {
      shipping: this.props.checkoutState.shipping,
      payment: this.props.checkoutState.payment,
      shippingInfo,
      books: this.props.cartState.shoppingCartBooks,
      overallPrice: this.props.overallCartPrice
    });
  }
}

const mapDispatchToProps = (
  dispatch: Dispatch<CheckoutActions | ShoppingCartActions | UserActions>
): DispatchProps => ({
  incrementStep: () => dispatch(IncrementStep()),
  decrementStep: () => dispatch(DecrementStep()),
  updateCheckoutShippingFormValues: values =>
    dispatch(UpdateCheckoutShippingFormValues(values)),
  resetCartState: () => dispatch(ResetCartState()),
  resetCheckoutState: () => dispatch(ResetCheckoutState()),
  setHasOrdered: hasOrdered => dispatch(SetHasOrdered(hasOrdered)),
  addOrderToStore: payload => dispatch(AddOrdersToStore(payload))
});

const mapStateToProps = ({
  checkout,
  shoppingCart,
  booksEntities
}: ApplicationState): StateProps => ({
  checkoutShippingFormValues: checkout.checkoutShippingForm,
  checkoutState: checkout,
  cartState: shoppingCart,
  overallCartPrice: mapShoppingCartBooksToPrice(
    shoppingCart.shoppingCartBooks,
    booksEntities.entities
  )
});

function mapShoppingCartBooksToPrice(
  shoppingCartBooks: KeyQuantityBooks,
  NormalizedBooks: NormalizedBookEntity
): number {
  return Object.entries(shoppingCartBooks).reduce(
    (currentPrice: number, [key, quantity]) => {
      currentPrice += NormalizedBooks.book[key].price * quantity;
      return currentPrice;
    },
    0
  );
}

export const CheckoutShippingFormConnector = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(C);
