import * as React from "react";

import {
  Form as FormikForm,
  InjectedFormikProps,
  withFormik,
  FastField
} from "formik";
import { InputField } from "@shared/InputField/InputField";
import { Icon, Button, Divider } from "antd";
import { checkoutShippingFormSchema } from "@yupSchemas/checkout.shipping.form.schema";
import FormItem from "antd/lib/form/FormItem";

export interface CheckoutShippingFormValues {
  firstName: string;
  lastName: string;
  city: string;
  street: string;
  zipCode: number;
}

export const initialCheckoutShippingFormValues: CheckoutShippingFormValues = {
  firstName: "",
  lastName: "",
  city: "",
  street: "",
  zipCode: undefined
};

interface FormProps {
  submit: (values: CheckoutShippingFormValues) => void;
}

interface ViewProps {
  isLoading: boolean;
}

interface Props {
  decrementStep: VoidFunction;
  updateCheckoutFormValues: (values: CheckoutShippingFormValues) => void;
  initialFormValues: CheckoutShippingFormValues;
}

const C: React.SFC<
  InjectedFormikProps<FormProps & Props & ViewProps, CheckoutShippingFormValues>
> = ({ values, decrementStep, updateCheckoutFormValues, isLoading }) => {
  function onGoBackClickLocal() {
    updateCheckoutFormValues(values);
    decrementStep();
  }

  return (
    <FormikForm noValidate={true}>
      <h1 style={{ margin: 0 }}>Fill the shipping info </h1>
      <Divider />
      <FastField
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="firstName"
        placeholder="First Name"
        type="text"
        size="large"
        component={InputField}
      />
      <FastField
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="lastName"
        placeholder="Last Name"
        type="text"
        size="large"
        component={InputField}
      />
      <FastField
        prefix={<Icon type="read" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="city"
        placeholder="City"
        type="text"
        size="large"
        component={InputField}
      />
      <FastField
        prefix={<Icon type="home" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="street"
        placeholder="Street"
        type="text"
        size="large"
        component={InputField}
      />
      <FastField
        prefix={<Icon type="tags" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="zipCode"
        placeholder="Zip Code (without dash)"
        type="number"
        size="large"
        component={InputField}
      />
      <FormItem className="Buttons">
        <Button
          loading={isLoading}
          type="default"
          size={"large"}
          style={{ flex: 1 }}
          onClick={onGoBackClickLocal}
        >
          Back
        </Button>
        <Button
          loading={isLoading}
          type="primary"
          className="SuccessButton"
          htmlType="submit"
          size={"large"}
          style={{ flex: 1, backgroundColor: "#70C040" }}
        >
          Order
        </Button>
      </FormItem>
    </FormikForm>
  );
};

export const CheckoutShippingFormView = withFormik<
  FormProps & Props & ViewProps,
  CheckoutShippingFormValues
>({
  validationSchema: checkoutShippingFormSchema,
  mapPropsToValues: ({ initialFormValues }) => initialFormValues,
  handleSubmit: (values: CheckoutShippingFormValues, { props, resetForm }) => {
    props.submit(values);
  }
})(C);
