import * as React from "react";

import { ShoppingCartCheckoutConnector } from "@shoppingCart/ShoppingCartCheckout/ShoppingCartCheckoutConnector";

import "./Checkout.css";
import "@shared/Css/Tile.css";
import { connect } from "react-redux";
import { ApplicationState } from "@store/root/root.reducer";
import { CheckoutShippingFormConnector } from "./CheckoutShippingForm/CheckoutShippingFormConnector";
import { Dispatch } from "redux";
import {
  CheckoutActions,
  SetStep,
  SetHasOrdered
} from "./store/checkout.actions";
import { routerHistory } from "src";

interface StateProps {
  currentStep: number;
  hasOrdered: boolean;
}

interface DispatchProps {
  setStep: (step: number) => void;
  setHasOrdered: (hasOrdered: boolean) => void;
}

class C extends React.Component<StateProps & DispatchProps> {
  public componentWillUnmount() {
    this.props.setStep(0);
    this.props.setHasOrdered(false);
  }

  public render() {
    return (
      <React.Fragment>
        {this.props.hasOrdered ? (
          <div className="HasOrdered TileWrapper">
            <div className="TileInner" style={{ padding: 24 }}>
              <h1 style={{ margin: 0, color: "rgba(0, 0, 0, 0.7)" }}>
                Thank you for ordering
              </h1>
              <span>
                Your order will be processed within next 24 hours. You can
                browse your order at{" "}
                <a onClick={this.onProfileLinkClick}>Profile Page</a>
              </span>
            </div>
          </div>
        ) : this.props.currentStep === 0 ? (
          <ShoppingCartCheckoutConnector />
        ) : this.props.currentStep === 1 ? (
          <CheckoutShippingFormConnector />
        ) : null}
      </React.Fragment>
    );
  }

  private onProfileLinkClick = () => {
    routerHistory.push("/user/profile");
  };
}

const mapStateToProps = ({ checkout }: ApplicationState): StateProps => ({
  currentStep: checkout.currentStep,
  hasOrdered: checkout.hasOrdered
});

const mapDispatchToProps = (
  dispatch: Dispatch<CheckoutActions>
): DispatchProps => ({
  setStep: step => dispatch(SetStep(step)),
  setHasOrdered: hasOrdered => dispatch(SetHasOrdered(hasOrdered))
});
const CheckoutConnector = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(C);

export default CheckoutConnector;
