import * as React from "react";
import { ApplicationState } from "@store/root/root.reducer";
import { connect } from "react-redux";

interface StateProps {
  isAuthenticated: boolean;
  isFetchingOnInit: boolean;
}

export const AuthContext = React.createContext({
  isAuthenticated: false,
  isFetchingOnInit: false
});

export class C extends React.PureComponent<StateProps, {}> {
  public static Consumer = AuthContext.Consumer;
  public render() {
    return (
      <AuthContext.Provider
        value={{
          isAuthenticated: this.props.isAuthenticated,
          isFetchingOnInit: this.props.isFetchingOnInit
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    );
  }
}

const mapStateToProps = ({ auth }: ApplicationState): StateProps => ({
  isAuthenticated: auth.isAuthenticated,
  isFetchingOnInit: auth.isFetchingOnInit
});

export const AuthProvider = connect<StateProps, {}, {}>(mapStateToProps)(C);
