import * as React from "react";
import { Dispatch } from "redux";
import { LoginView } from "./LoginView";
import { ApplicationState } from "@store/root/root.reducer";

import { connect } from "react-redux";
import {
  LoginCredentials,
  AuthActions,
  Login,
  RemoveAuthErrorFeedback
} from "@auth/store/auth.actions";

interface StateProps {
  errorMessage: string | null;
  isLoading: boolean;
}

interface DispatchProps {
  login: (credentials: LoginCredentials) => void;
  removeAuthErrorFeedback: () => void;
}

class C extends React.PureComponent<StateProps & DispatchProps, {}> {
  public render(): React.ReactNode {
    return (
      <LoginView
        isLoading={this.props.isLoading}
        submit={this.formLoginSubmitHandler}
        errorMessage={this.props.errorMessage}
      />
    );
  }

  public componentWillUnmount() {
    this.props.removeAuthErrorFeedback();
  }

  private formLoginSubmitHandler = (values: LoginCredentials) => {
    this.props.login(values);
  };
}

const mapStateToProps = ({ auth }: ApplicationState): StateProps => ({
  errorMessage: auth.errorMessage,
  isLoading: auth.isLoading
});

const mapDispatchToProps = (
  dispatch: Dispatch<AuthActions>
): DispatchProps => ({
  login: (credentials: LoginCredentials) => dispatch(Login(credentials)),
  removeAuthErrorFeedback: () => dispatch(RemoveAuthErrorFeedback())
});

const LoginConnector = connect<StateProps, DispatchProps, {}>(
  mapStateToProps,
  mapDispatchToProps
)(C);

export default LoginConnector;
