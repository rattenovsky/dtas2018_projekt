import * as React from "react";
import {
  Field,
  Form as FormikForm,
  InjectedFormikProps,
  withFormik
} from "formik";
import { Button, Form as AntForm, Icon, Alert } from "antd";
import { InputField } from "@shared/InputField/InputField";
import "@auth/Auth.css";
import { loginSchema } from "@yupSchemas/login.schema";

const FormItem = AntForm.Item;

interface FormValues {
  email: string;
  password: string;
}

interface FormProps {
  submit: (values: FormValues) => void;
}

interface LoginViewProps {
  errorMessage: string | null;
  isLoading: boolean;
}

const C: React.SFC<
  InjectedFormikProps<FormProps & LoginViewProps, FormValues>
> = ({ errorMessage, isLoading }) => (
  <div className="authFormWrapper">
    <FormikForm noValidate={true} className="authForm">
      <h1>Login</h1>
      <Field
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="email"
        type="email"
        placeholder="Email"
        isDisabled={isLoading}
        size={"large"}
        component={InputField}
      />
      <Field
        prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="password"
        type="password"
        size={"large"}
        isDisabled={isLoading}
        placeholder="Password"
        component={InputField}
      />
      <FormItem>
        <Button
          loading={isLoading}
          type="primary"
          htmlType="submit"
          className="authFormSubmitButton"
          size={"large"}
        >
          Login
        </Button>
      </FormItem>
      {errorMessage && <Alert message={errorMessage} type="error" />}
    </FormikForm>
  </div>
);

export const LoginView = withFormik<FormProps & LoginViewProps, FormValues>({
  validationSchema: loginSchema,
  mapPropsToValues: (): FormValues => ({ email: "", password: "" }),
  handleSubmit: (values: FormValues, { props }) => {
    props.submit(values);
  }
})(C);
