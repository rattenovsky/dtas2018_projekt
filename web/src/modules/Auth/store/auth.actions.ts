import { ActionCreator } from "redux";
import { Action } from "redux";
import { UserAuthData, UserProfileData } from "@auth/store/auth.reducer";

export enum AuthActionsEnum {
  login = "@@LoginConnector LOGIN",
  logout = "@@MenuConnector LOGOUT",
  register = "@@RegisterConnector REGISTER",
  removeAuthErrorFeedback = "@@AuthConnector REMOVE_AUTH_FEEDBACK",
  authFailed = "@@AuthEpic AUTH_FAILED",
  authSuccessful = "@@AuthEpic AUTH_SUCCESSFUL",
  populateAuthState = "@@AuthEpic POPULATE_AUTH_STATE",
  getAuthData = "@@AuthEpicInit GET_AUTH_DATA",
  resetAuthData = "@@GetAuthDataEpic RESET_AUTH_DATA"
}

// interfaces
export interface LoginCredentials {
  email: string;
  password: string;
}

export interface RegisterCredentials extends LoginCredentials {
  firstName: string;
  lastName: string;
  repeatPassword: string;
}

export interface PopulateAuthStatePayload {
  userAuthData: UserAuthData;
  userProfileData: UserProfileData;
}

export interface LoginAction extends Action {
  type: AuthActionsEnum.login;
  payload: LoginCredentials;
}

export interface RegisterAction extends Action {
  type: AuthActionsEnum.register;
  payload: RegisterCredentials;
}

export interface RemoveAuthErrorFeedbackAction extends Action {
  type: AuthActionsEnum.removeAuthErrorFeedback;
}

export interface AuthFailedAction extends Action {
  type: AuthActionsEnum.authFailed;
  payload: string;
}

export interface AuthSuccessfulAction extends Action {
  type: AuthActionsEnum.authSuccessful;
}

export interface PopulateAuthStateAction extends Action {
  type: AuthActionsEnum.populateAuthState;
  payload: PopulateAuthStatePayload;
}

export interface LogoutAction {
  type: AuthActionsEnum.logout;
}

export interface GetAuthDataAction {
  type: AuthActionsEnum.getAuthData;
}

export interface ResetAuthDataAction {
  type: AuthActionsEnum.resetAuthData;
}

// actions
export const Login: ActionCreator<LoginAction> = (
  payload: LoginCredentials
) => ({
  type: AuthActionsEnum.login,
  payload
});

export const Register: ActionCreator<RegisterAction> = (
  payload: RegisterCredentials
) => ({
  type: AuthActionsEnum.register,
  payload
});

export const AuthFailed: ActionCreator<AuthFailedAction> = (
  payload: string
) => ({
  payload,
  type: AuthActionsEnum.authFailed
});

export const AuthSuccessful: ActionCreator<AuthSuccessfulAction> = () => ({
  type: AuthActionsEnum.authSuccessful
});

export const RemoveAuthErrorFeedback: ActionCreator<
  RemoveAuthErrorFeedbackAction
> = () => ({
  type: AuthActionsEnum.removeAuthErrorFeedback
});

export const GetAuthData: ActionCreator<GetAuthDataAction> = () => ({
  type: AuthActionsEnum.getAuthData
});

export const PopulateAuthData: ActionCreator<PopulateAuthStateAction> = (
  payload: PopulateAuthStatePayload
) => ({
  type: AuthActionsEnum.populateAuthState,
  payload
});

export const Logout: ActionCreator<LogoutAction> = () => ({
  type: AuthActionsEnum.logout
});

export const ResetAuthData: ActionCreator<ResetAuthDataAction> = () => ({
  type: AuthActionsEnum.resetAuthData
});

export type AuthActions =
  | LoginAction
  | RegisterAction
  | RemoveAuthErrorFeedbackAction
  | AuthFailedAction
  | PopulateAuthStateAction
  | LogoutAction
  | GetAuthDataAction
  | ResetAuthDataAction
  | AuthSuccessfulAction;
