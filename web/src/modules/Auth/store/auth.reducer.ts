import { AuthActions, AuthActionsEnum } from "@auth/store/auth.actions";

export interface AuthState {
  userAuthData: UserAuthData;
  userProfileData: UserProfileData;
  isAuthenticated: boolean;
  isFetchingOnInit: boolean;
  isLoading: boolean;
  errorMessage: string | null;
}

export interface UserAuthData {
  id: string;
  token: string;
  email: string;
}

export interface UserProfileData {
  firstName: string;
  lastName: string;
  avatar: string | null;
}

export const initialAuthState: AuthState = {
  userAuthData: null,
  userProfileData: null,
  isAuthenticated: false,
  errorMessage: null,
  isFetchingOnInit: false,
  isLoading: false
};

export function authReducer(
  state: AuthState = initialAuthState,
  action: AuthActions
): AuthState {
  switch (action.type) {
    case AuthActionsEnum.login:
    case AuthActionsEnum.register:
      return {
        ...state,
        isLoading: true,
        errorMessage: null
      };
    case AuthActionsEnum.authFailed:
      return {
        ...state,
        errorMessage: action.payload,
        isLoading: false
      };
    case AuthActionsEnum.authSuccessful:
      return {
        ...state,
        isLoading: false
      };
    case AuthActionsEnum.removeAuthErrorFeedback:
      return {
        ...state,
        errorMessage: null,
        // just in case if user decides to switch pages when form is sending data
        isLoading: false
      };
    case AuthActionsEnum.populateAuthState:
      return {
        ...state,
        isAuthenticated: true,
        errorMessage: null,
        userAuthData: action.payload.userAuthData,
        userProfileData: action.payload.userProfileData,
        isFetchingOnInit: false
      };
    case AuthActionsEnum.getAuthData:
      return {
        ...state,
        isFetchingOnInit: true
      };
    case AuthActionsEnum.resetAuthData:
    case AuthActionsEnum.logout:
      return initialAuthState;
    default:
      return state;
  }
}
