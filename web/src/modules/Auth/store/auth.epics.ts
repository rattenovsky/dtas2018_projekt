import {
  CheckoutActions,
  ResetCheckoutState
} from "./../../Checkout/store/checkout.actions";
import { AuthSuccessful, GetAuthData, ResetAuthData } from "./auth.actions";
import { ActionsObservable, ofType } from "redux-observable";
import {
  AuthActions,
  AuthActionsEnum,
  LoginCredentials,
  AuthFailed,
  RegisterCredentials,
  PopulateAuthData,
  PopulateAuthStatePayload
} from "@auth/store/auth.actions";
import { toPayload, toData } from "@store/root/store.utils";
import {
  map,
  mergeMap,
  catchError,
  concatMap,
  tap,
  switchMap,
  withLatestFrom
} from "rxjs/operators";
import axios from "axios";
import { from, Observable, of, defer } from "rxjs";
import { push, RouterAction } from "connected-react-router";
import { composeRight } from "@utils/functions";
import { ApplicationState } from "@store/root/root.reducer";
import {
  FetchShoppingCartBooks,
  ShoppingCartActions,
  ResetCartState
} from "@shoppingCart/store/shopping.cart.actions";

type AuthEpic = (
  action$: ActionsObservable<AuthActions>,
  state$: Observable<ApplicationState>
) => Observable<
  AuthActions | RouterAction | ShoppingCartActions | CheckoutActions
>;

const loginEpic: AuthEpic = (action$, state$) =>
  action$.pipe(
    ofType(AuthActionsEnum.login),
    map(toPayload),
    mergeMap((payload: LoginCredentials) => {
      return from(axios.post("/auth/login", payload)).pipe(
        LoginRegisterHandler(state$)
      );
    })
  );

const registerEpic: AuthEpic = (action$, state$) =>
  action$.pipe(
    ofType(AuthActionsEnum.register),
    map(toPayload),
    mergeMap((payload: RegisterCredentials) => {
      return from(axios.post("/auth/register", payload)).pipe(
        LoginRegisterHandler(state$)
      );
    })
  );

const getAuthDataEpic: AuthEpic = (action$, state$) =>
  action$.pipe(
    ofType(AuthActionsEnum.getAuthData),
    mergeMap(() =>
      getUserIdFromToken().pipe(
        map(toData),
        switchMap((id: string) =>
          getUserById(id).pipe(LoginRegisterHandler(state$))
        ),
        catchError(() => {
          localStorage.removeItem("token");
          return of(ResetAuthData());
        })
      )
    )
  );

const logoutEpic: AuthEpic = action$ =>
  action$.pipe(
    ofType(AuthActionsEnum.logout),
    tap(() => {
      localStorage.removeItem("token");
    }),
    concatMap(() => [ResetCartState(), ResetCheckoutState(), push("/login")])
  );

const initEpic = (): Observable<any> =>
  defer(() => {
    const token = localStorage.getItem("token");
    if (token) {
      return of(GetAuthData());
    }
    return of();
  });

const LoginRegisterHandler = (state$: Observable<ApplicationState>) => <T>(
  source: Observable<T>
) =>
  source.pipe(
    map([toData, toPopulateAuthStatePayload].reduce(composeRight)),
    tap(saveToken),
    withLatestFrom(state$),
    map(([data, state]: [PopulateAuthStatePayload, ApplicationState]) => [
      data,
      toCurrentRouterPath(state)
    ]),
    concatMap(
      ([data, currentRouterPath]: [PopulateAuthStatePayload, string]) => {
        let actions = [];
        if (shouldBeRedirected(currentRouterPath)) {
          actions = [AuthSuccessful(), push("/home")];
        } else {
          actions = [AuthSuccessful()];
        }
        return [PopulateAuthData(data), ...actions, FetchShoppingCartBooks()];
      }
    ),
    catchError((err: any) => {
      if (err.response) {
        return of(AuthFailed(err.response.data));
      }
      return of(AuthFailed("something went wrong!"));
    })
  );

function saveToken({ userAuthData: { token } }: PopulateAuthStatePayload) {
  localStorage.setItem("token", token);
}

function getUserIdFromToken() {
  return from(axios.get("/auth/session"));
}

function getUserById(id: string) {
  return from(axios.get(`/user/${id}`));
}

function shouldBeRedirected(pathname: string) {
  return /login|register/.test(pathname);
}

function toCurrentRouterPath(state$: ApplicationState) {
  return state$.router.location.pathname + state$.router.location.search;
}

function toPopulateAuthStatePayload({
  token,
  firstName,
  lastName,
  avatar,
  email,
  id
}: any): PopulateAuthStatePayload {
  return {
    userAuthData: {
      token: token || localStorage.getItem("token"),
      id,
      email
    },
    userProfileData: {
      firstName,
      lastName,
      avatar
    }
  };
}

export const AuthEpics = [
  loginEpic,
  registerEpic,
  initEpic,
  getAuthDataEpic,
  logoutEpic
];
