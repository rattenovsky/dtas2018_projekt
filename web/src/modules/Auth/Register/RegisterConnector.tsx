import * as React from "react";
import { RegisterView } from "./RegisterView";
import {
  RegisterCredentials,
  Register,
  RemoveAuthErrorFeedback,
  AuthActions
} from "@auth/store/auth.actions";
import { ApplicationState } from "@store/root/root.reducer";
import { Dispatch } from "redux";
import { connect } from "react-redux";

interface DispatchProps {
  register: (payload: RegisterCredentials) => void;
  removeAuthFeedback: () => void;
}

interface StateProps {
  errorMessage: string | null;
  isLoading: boolean;
}

class C extends React.PureComponent<DispatchProps & StateProps, {}> {
  public componentWillUnmount() {
    this.props.removeAuthFeedback();
  }
  public render(): React.ReactNode {
    return (
      <RegisterView
        isLoading={this.props.isLoading}
        errorMessage={this.props.errorMessage}
        submit={this.formRegisterSubmitHandler}
      />
    );
  }

  private formRegisterSubmitHandler = (credentials: RegisterCredentials) => {
    this.props.register(credentials);
  };
}

const mapStateToProps = ({ auth }: ApplicationState): StateProps => ({
  errorMessage: auth.errorMessage,
  isLoading: auth.isLoading
});

const mapDispatchToProps = (
  dispatch: Dispatch<AuthActions>
): DispatchProps => ({
  register: (credentials: RegisterCredentials) =>
    dispatch(Register(credentials)),
  removeAuthFeedback: () => dispatch(RemoveAuthErrorFeedback())
});

const RegisterConnector = connect<StateProps, DispatchProps, {}>(
  mapStateToProps,
  mapDispatchToProps
)(C);

export default RegisterConnector;
