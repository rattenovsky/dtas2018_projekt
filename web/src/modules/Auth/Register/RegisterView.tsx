import * as React from "react";
import { InputField } from "@shared/InputField/InputField";
import {
  Field,
  Form as FormikForm,
  InjectedFormikProps,
  withFormik,
  FastField
} from "formik";
import { Button, Form as AntForm, Icon, Alert } from "antd";
import "@auth/Auth.css";
import { registerSchema } from "@yupSchemas/register.schema";

const FormItem = AntForm.Item;

interface FormValues {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  repeatPassword: string;
}

interface FormProps {
  submit: (values: FormValues) => void;
}

interface RegisterViewProps {
  errorMessage: string;
  isLoading: boolean;
}

const C: React.SFC<
  InjectedFormikProps<FormProps & RegisterViewProps, FormValues>
> = ({ errorMessage, isLoading }) => (
  <div className="authFormWrapper">
    <FormikForm noValidate={true} className="authForm">
      <h1>Register</h1>
      <FastField
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="firstName"
        type="text"
        placeholder="First Name"
        size={"large"}
        isDisabled={isLoading}
        component={InputField}
      />
      <FastField
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="lastName"
        type="text"
        placeholder="Second Name"
        size={"large"}
        isDisabled={isLoading}
        component={InputField}
      />
      <FastField
        prefix={<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="email"
        type="email"
        placeholder="Email"
        size={"large"}
        isDisabled={isLoading}
        component={InputField}
      />
      <Field
        prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="password"
        type="password"
        size={"large"}
        isDisabled={isLoading}
        placeholder="Password"
        component={InputField}
      />
      <Field
        prefix={<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />}
        name="repeatPassword"
        type="password"
        size={"large"}
        isDisabled={isLoading}
        placeholder="Confirm Password"
        component={InputField}
      />
      <FormItem>
        <Button
          type="primary"
          htmlType="submit"
          className="authFormSubmitButton"
          size={"large"}
          loading={isLoading}
        >
          Register
        </Button>
      </FormItem>
      {errorMessage && <Alert message={errorMessage} type="error" />}
    </FormikForm>
  </div>
);

export const RegisterView = withFormik<
  FormProps & RegisterViewProps,
  FormValues
>({
  validationSchema: registerSchema,
  mapPropsToValues: (): FormValues => ({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    repeatPassword: ""
  }),
  handleSubmit: (values: FormValues, { props }) => {
    props.submit(values);
  }
})(C);
