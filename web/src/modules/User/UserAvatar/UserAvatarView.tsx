import * as React from "react";
import { Avatar } from "antd";
import "./UserAvatar.css";
import { routerHistory } from "src";

interface Props {
  firstName: string;
  lastName: string;
  avatar: string;
}

export const UserAvatarView: React.SFC<Props> = ({
  firstName,
  lastName,
  avatar
}) => {
  function onClick() {
    routerHistory.push("/user/profile");
  }
  return avatar ? (
    <div onClick={onClick}>
      <Avatar src={avatar} />
    </div>
  ) : (
    <div onClick={onClick}>
      <Avatar className="UserAvatar">{`${firstName[0]}${lastName[0]}`}</Avatar>
    </div>
  );
};
