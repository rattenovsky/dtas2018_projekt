import * as React from "react";
import { UserAvatarView } from "./UserAvatarView";
import { ApplicationState } from "@store/root/root.reducer";
import { connect } from "react-redux";

interface StateProps {
  firstName: string;
  lastName: string;
  avatar: string;
}

export class C extends React.PureComponent<StateProps, {}> {
  public render() {
    return <UserAvatarView {...this.props} />;
  }
}

const mapStateToProps = ({ auth: { userProfileData } }: ApplicationState) => ({
  ...userProfileData
});

export const UserAvatarConnector = connect<StateProps, {}, {}>(
  mapStateToProps,
  {}
)(C);
