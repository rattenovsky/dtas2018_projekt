import { KeyQuantityBooks } from "@shoppingCart/store/shopping.cart.reducer";
import { UserActions, UserActionsEnum } from "./user.actions";

export interface Order {
  books: KeyQuantityBooks;
  createdAt: number;
  payment: string;
  shipping: string;
  overallPrice: number;
  id: string;
  shippingInfo?: {
    city: string;
    firstName: string;
    lastName: string;
    street: string;
    zipCode: number;
  };
  updatedAt?: number;
}

export interface NormalizedOrderEntities {
  orders: { [key: string]: Order };
}

export interface UserState {
  entities: NormalizedOrderEntities;
  hasFetchedOrders: boolean;
  hasFetchErrors: boolean;
  isFetchingOrders: boolean;
  result: string[];
}

export const initialUserState: UserState = {
  entities: {
    orders: {}
  },
  hasFetchedOrders: false,
  hasFetchErrors: false,
  isFetchingOrders: true,
  result: []
};

export function UserReducer(
  state: UserState = initialUserState,
  action: UserActions
): UserState {
  switch (action.type) {
    case UserActionsEnum.fetchOrders:
      return {
        ...state,
        isFetchingOrders: true,
        hasFetchErrors: false
      };
    case UserActionsEnum.addOrdersToStore:
      return {
        ...state,
        entities: {
          ...state.entities,
          orders: {
            ...state.entities.orders,
            ...action.payload.entities.orders
          }
        },
        result: [...state.result, ...action.payload.result]
      };
    case UserActionsEnum.ordersFetchedSuccessfully:
      return {
        ...state,
        isFetchingOrders: false,
        hasFetchedOrders: true
      };
    case UserActionsEnum.ordersFetchErrorOccurred:
      return {
        ...state,
        hasFetchErrors: true,
        isFetchingOrders: false
      };
    default:
      return state;
  }
}
