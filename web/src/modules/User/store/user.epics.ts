import {
  UserActions,
  UserActionsEnum,
  AddOrdersToStore,
  OrdersFetchedSuccessfully,
  OrdersFetchErrorOccurred,
  AddOrdersPayload
} from "./user.actions";
import { schema, normalize } from "normalizr";
import { ActionsObservable, StateObservable, ofType } from "redux-observable";
import { ApplicationState } from "@store/root/root.reducer";
import { Observable, from, of } from "rxjs";
import { switchMap, map, concatMap, catchError } from "rxjs/operators";
import axios from "axios";
import { toData } from "@store/root/store.utils";
import { Order, NormalizedOrderEntities } from "./user.reducer";
import {
  composeRight,
  removeDuplicateElementsFromArray,
  filterDataBasedOnArray
} from "@utils/functions";
import {
  FetchAndAddMissingBookAction,
  FetchAndAddMissingBooks
} from "@book/store/books.entity.action";

const orderSchema = new schema.Entity("orders");
export const orderListSchema = [orderSchema];

type UserEpic = (
  action$: ActionsObservable<UserActions>,
  state$: StateObservable<ApplicationState>
) => Observable<UserActions | FetchAndAddMissingBookAction>;

const fetchOrdersEpic: UserEpic = (action$, state$) =>
  action$.pipe(
    ofType(UserActionsEnum.fetchOrders),
    switchMap(() =>
      from(axios.get("/orders")).pipe(
        map(
          [
            toData,
            toNormalizedOrders,
            toNormalizedOrdersAlongWithMissingBooks(
              state$.value.booksEntities.result
            )
          ].reduce(composeRight)
        ),
        concatMap(([normalizedOrders, missingBookIds]) => [
          FetchAndAddMissingBooks(missingBookIds),
          AddOrdersToStore(normalizedOrders),
          OrdersFetchedSuccessfully()
        ]),
        catchError(e => of(OrdersFetchErrorOccurred()))
      )
    )
  );

const toNormalizedOrders = (orders: Order[]): NormalizedOrderEntities => {
  const normalizedOrders = normalize(orders, orderListSchema) as any;
  return normalizedOrders.result.length <= 0
    ? {
        ...normalizedOrders,
        entities: {
          orders: {}
        }
      }
    : normalizedOrders;
};

const toNormalizedOrdersAlongWithMissingBooks = (
  booksCurrentlyInStore: string[]
) => (normalizedOrders: AddOrdersPayload): [AddOrdersPayload, string[]] => {
  return [
    normalizedOrders,
    composeRight(
      toBooksFromOrders(normalizedOrders),
      removeDuplicateElementsFromArray,
      toFilteredUniqueBookIds(booksCurrentlyInStore)
    )()
  ];
};

const toBooksFromOrder = (acc: string[], order: Order): string[] => {
  acc.push(...Object.keys(order.books));
  return acc;
};

const toBooksFromOrders = (
  normalizedOrders: AddOrdersPayload
): (() => string[]) => () =>
  Object.values(normalizedOrders.entities.orders).reduce(toBooksFromOrder, []);

const toFilteredUniqueBookIds = (booksCurrentlyInStore: string[]) => (
  uniqueBookIdsFromOrders: string[]
) =>
  booksCurrentlyInStore.length <= 0
    ? uniqueBookIdsFromOrders
    : filterDataBasedOnArray(
        uniqueBookIdsFromOrders,
        booksCurrentlyInStore,
        true
      );

export const UserEpics = [fetchOrdersEpic];
