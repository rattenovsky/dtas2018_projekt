import { ActionCreator } from "redux";
import { NormalizedOrderEntities } from "./user.reducer";

export enum UserActionsEnum {
  fetchOrders = "@@UserProfileConnector FETCH_ORDERS",
  ordersFetchedSuccessfully = "@@UserEpics ORDERS_FETCHED_SUCCESSFULLY",
  addOrdersToStore = "@@UserEpics ADD_ORDERS_TO_STORE",
  ordersFetchErrorOccurred = "@@UserEpics ORDERS_FETCH_ERROR_OCCURRED"
}

interface FetchOrdersAction {
  type: UserActionsEnum.fetchOrders;
}

export interface AddOrdersPayload {
  entities: NormalizedOrderEntities;
  result: string[];
}

interface OrdersFetchedSuccessfullyAction {
  type: UserActionsEnum.ordersFetchedSuccessfully;
}

interface OrdersFetchErrorOccurredAction {
  type: UserActionsEnum.ordersFetchErrorOccurred;
}

interface AddOrdersToStoreAction {
  type: UserActionsEnum.addOrdersToStore;
  payload: AddOrdersPayload;
}

export const FetchOrders: ActionCreator<FetchOrdersAction> = () => ({
  type: UserActionsEnum.fetchOrders
});

export const OrdersFetchedSuccessfully: ActionCreator<
  OrdersFetchedSuccessfullyAction
> = () => ({
  type: UserActionsEnum.ordersFetchedSuccessfully
});

export const OrdersFetchErrorOccurred: ActionCreator<
  OrdersFetchErrorOccurredAction
> = () => ({
  type: UserActionsEnum.ordersFetchErrorOccurred
});

export const AddOrdersToStore: ActionCreator<AddOrdersToStoreAction> = (
  payload: AddOrdersPayload
) => ({
  type: UserActionsEnum.addOrdersToStore,
  payload
});

export type UserActions =
  | FetchOrdersAction
  | AddOrdersToStoreAction
  | OrdersFetchErrorOccurredAction
  | OrdersFetchedSuccessfullyAction;
