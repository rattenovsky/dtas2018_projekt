import * as React from "react";
// import { UserProfileView } from "./UserProfileView";
import { ApplicationState } from "@store/root/root.reducer";
// import { Order } from "@user/store/user.reducer";
import { Dispatch } from "redux";
import { UserActions, FetchOrders } from "@user/store/user.actions";
import { connect } from "react-redux";
import { Order } from "@user/store/user.reducer";
import { Exception } from "@shared/Exception/Exception";
import { UserProfileView } from "./UserProfileView";
//
interface DispatchProps {
  fetchOrders: VoidFunction;
}

interface StateProps {
  hasFetchedOrders: boolean;
  hasFetchErrors: boolean;
  isFetchingOrders: boolean;
  orders: Order[];
}

class UserProfileConnector extends React.Component<DispatchProps & StateProps> {
  public componentDidMount() {
    !this.props.hasFetchedOrders && this.props.fetchOrders();
  }
  public render() {
    return !this.props.isFetchingOrders && this.props.hasFetchErrors ? (
      <Exception onRetryClick={this.props.fetchOrders} />
    ) : (
      <UserProfileView
        isLoading={this.props.isFetchingOrders}
        orders={this.props.orders}
      />
    );
  }
}

const mapStateToProps = ({ user }: ApplicationState): StateProps => ({
  hasFetchErrors: user.hasFetchErrors,
  hasFetchedOrders: user.hasFetchedOrders,
  orders: Object.values(user.entities.orders),
  isFetchingOrders: user.isFetchingOrders
});
const mapDispatchToProps = (
  dispatch: Dispatch<UserActions>
): DispatchProps => ({
  fetchOrders: () => dispatch(FetchOrders())
});

const ConnectedUserProfileConnector = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(UserProfileConnector);

export default ConnectedUserProfileConnector;
