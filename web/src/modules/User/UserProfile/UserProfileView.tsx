import * as React from "react";
import { Table, Tag } from "antd";
import "./UserProfile.css";
import { Order } from "@user/store/user.reducer";
import * as moment from "moment";
import { PosedAppLoader } from "@utils/Loaders/AppLoader/PosedAppLoader";
import { useMedia } from "@shared/Hooks/useMedia";
import { routerHistory } from "src";
import "@shared/Css/Tile.css";
const CURRENT_DATE = Date.now();

interface Props {
  orders: Order[];
  isLoading: boolean;
}

const columns = [
  {
    title: "Order",
    dataIndex: "id",
    key: "id",
    render: (id: string) => (
      // tslint:disable-next-line:jsx-no-lambda
      <Tag onClick={() => routerHistory.push(`/order/${id}`)} color="#2db7f5">
        {id}
      </Tag>
    )
  },
  {
    title: "Date",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date: number) => moment(date).format("lll"),
    // nice MEME ANT DESIGN, STRING NOT ASSIGNABLE TO STRING ROFL!
    defaultSortOrder: "descend" as any,
    align: "center" as any,
    sorter: (a: Partial<Order>, b: Partial<Order>) =>
      a.createdAt - CURRENT_DATE < b.createdAt - CURRENT_DATE ? -1 : 1
  },
  {
    title: "Price",
    dataIndex: "overallPrice",
    key: "overallPrice",
    align: "center" as any,
    render: (num: number) => num + "zł",
    sorter: (a: Partial<Order>, b: Partial<Order>) =>
      a.overallPrice - b.overallPrice
  },
  {
    title: "Number of Books",
    dataIndex: "booksCount",
    align: "center" as any,
    key: "booksCount",
    sorter: (a: any, b: any) => a.booksCount - b.booksCount
  }
];

export const UserProfileView: React.SFC<Props> = props => {
  const isOnMobile = useMedia("(max-width:760px)");

  return (
    <React.Fragment>
      <PosedAppLoader isVisible={props.isLoading} />
      {props.orders.length <= 0 && !props.isLoading
        ? renderNoResults()
        : isOnMobile
        ? renderMobileOrders()
        : !isOnMobile && !props.isLoading
        ? renderTable()
        : null}
    </React.Fragment>
  );

  function renderTable() {
    return (
      <div className="UserProfileWrapper">
        <div className="TileWrapper">
          <h1>Your orders</h1>
          <Table
            bordered={true}
            columns={columns}
            dataSource={props.orders.map(order => ({
              id: order.id,
              createdAt: order.createdAt,
              overallPrice: order.overallPrice,
              booksCount: Object.values(order.books).reduce(
                (acc: number, bookCount) => (acc += bookCount),
                0
              ),
              key: order.id
            }))}
            pagination={false}
          />
        </div>
      </div>
    );
  }

  function renderMobileOrders() {
    const mappedOrders = props.orders
      .sort((a: Order, b: Order) =>
        a.createdAt - CURRENT_DATE < b.createdAt - CURRENT_DATE ? 1 : -1
      )
      .map(order => {
        const onOrderClick = () => routerHistory.push(`/order/${order.id}`);
        return (
          <div
            className="TileInner MobileOrder"
            key={order.id}
            onClick={onOrderClick}
          >
            <h2 style={{ color: "#1890ff" }}>{order.id}</h2>
            <span>Ordered : {moment(order.createdAt).format("lll")}</span>
          </div>
        );
      });

    return (
      <div className="MobileOrdersWrapper">
        <h1>Your orders</h1>
        {mappedOrders}
      </div>
    );
  }

  function renderNoResults() {
    function onLinkClick() {
      routerHistory.push("/home");
    }
    return (
      <div className="TileWrapper MobileFullWidth">
        <div className="TileInner" style={{ padding: 24 }}>
          <h1>You have not ordered anything yet!</h1>
          <span>
            You can browse books at the <a onClick={onLinkClick}>home page</a>
          </span>
        </div>
      </div>
    );
  }
};
