import * as React from "react";
import { Card } from "antd";

export const BooksListingLoadingView: React.SFC = () => {
  const loadingBooks = [...Array.from({ length: 10 }, (_, i) => i)].map(num => (
    <Card
      style={{ display: "block" }}
      key={num}
      className={"GridItem"}
      loading={true}
    />
  ));

  return <div className={"Grid"}>{loadingBooks}</div>;
};
