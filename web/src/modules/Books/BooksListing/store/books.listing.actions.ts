import { Action, ActionCreator } from "redux";
import { Book } from "@book/store/books.entity.reducer";

export enum BooksListingActionEnum {
  fetchInitialListingBooks = "@@BooksFilterEpics FETCH_INITIAL_LISTING_BOOKS",
  initialListingBooksFetchSuccessful = "@@BooksListingEpics INITIAL_BOOKS_FETCH_SUCCESSFUL",
  fetchNextListingBooks = "@@BooksListingEpics FETCH_NEXT_LISTING_BOOKS",
  nextListingBooksFetchSuccessful = "@@BooksListingEpics NEXT_BOOKS_FETCH_SUCCESSFUL",
  fetchNextListingBooksError = "@@BooksListingEpics FETCH_NEXT_LISTING_BOOKS_ERROR",
  parseStartingPagination = "@@BooksFilterEpics PARSE_STARTING_PAGINATION",
  pageChanged = "@@BooksPagination PAGE_CHANGED",
  parseStartingPageSize = "@@BooksListingEpics PARSE_STARTING_PAGE_SIZE",
  parseStartingPageNumber = "@@BooksListingEpics PARSE_STARTING_PAGE_NUMBER",
  pageSizeChanged = "@@BooksPagination PAGE_SIZE_CHANGED",
  fetchingUrlChanged = "@@BooksListingEpics FETCHING_URL_CHANGED",
  setCurrentPageNumber = "@@BooksListingEpics SET_CURRENT_PAGE",
  setCurrentPageSize = "@@BooksListingEpics SET_CURRENT_PAGE_SIZE",
  constructFetchingUrl = "@@BooksListingEpics CONSTRUCT_FETCHING_URL",
  fetchingUrlConstructed = "@@BooksListingEpics FETCHING_URL_CONSTRUCTED"
}

export interface NextListingBooksFetchSuccessfulPayload {
  numberOfBooksForFetchingUrl: number;
  fetchedBooks: Book[];
}

export interface ConstructFetchingUrlAction extends Action {
  type: BooksListingActionEnum.constructFetchingUrl;
  payload: string;
}

export interface FetchingUrlConstructedAction extends Action {
  type: BooksListingActionEnum.fetchingUrlConstructed;
}

export interface FetchNextListingBooksErrorAction extends Action {
  type: BooksListingActionEnum.fetchNextListingBooksError;
}

export interface SetCurrentPageNumberAction extends Action {
  type: BooksListingActionEnum.setCurrentPageNumber;
  payload: number;
}

export interface SetCurrentPageSizeAction extends Action {
  type: BooksListingActionEnum.setCurrentPageSize;
  payload: number;
}

export interface ParseStartingPaginationAction extends Action {
  type: BooksListingActionEnum.parseStartingPagination;
}

export interface FetchInitialListingBooksAction extends Action {
  type: BooksListingActionEnum.fetchInitialListingBooks;
  payload: string;
}

export interface InitialListingBooksFetchSuccessfulAction extends Action {
  type: BooksListingActionEnum.initialListingBooksFetchSuccessful;
}

export interface FetchNextListingBooksAction extends Action {
  type: BooksListingActionEnum.fetchNextListingBooks;
}

export interface NextListingBooksFetchSuccessfulAction extends Action {
  type: BooksListingActionEnum.nextListingBooksFetchSuccessful;
  payload: NextListingBooksFetchSuccessfulPayload;
}

export interface FetchingUrlChangedAction extends Action {
  type: BooksListingActionEnum.fetchingUrlChanged;
  payload: string;
}

export interface PageChangedAction extends Action {
  type: BooksListingActionEnum.pageChanged;
  payload: number;
}

export interface PageSizeChangedAction extends Action {
  type: BooksListingActionEnum.pageSizeChanged;
  payload: number;
}

export const FetchInitialBooks: ActionCreator<
  FetchInitialListingBooksAction
> = (payload: string) => ({
  type: BooksListingActionEnum.fetchInitialListingBooks,
  payload
});

export const ConstructFetchingUrl: ActionCreator<ConstructFetchingUrlAction> = (
  payload: string
) => ({
  type: BooksListingActionEnum.constructFetchingUrl,
  payload
});

export const FetchNextListingBooksError: ActionCreator<
  FetchNextListingBooksErrorAction
> = () => ({
  type: BooksListingActionEnum.fetchNextListingBooksError
});

export const FetchingUrlConstructed: ActionCreator<
  FetchingUrlConstructedAction
> = () => ({
  type: BooksListingActionEnum.fetchingUrlConstructed
});

export const FetchNextBooks: ActionCreator<
  FetchNextListingBooksAction
> = () => ({
  type: BooksListingActionEnum.fetchNextListingBooks
});

export const ParseStartingPagination: ActionCreator<
  ParseStartingPaginationAction
> = () => ({
  type: BooksListingActionEnum.parseStartingPagination
});

export const SetCurrentPageNumber: ActionCreator<SetCurrentPageNumberAction> = (
  payload: number
) => ({
  type: BooksListingActionEnum.setCurrentPageNumber,
  payload
});

export const SetCurrentPageSize: ActionCreator<SetCurrentPageSizeAction> = (
  payload: number
) => ({
  type: BooksListingActionEnum.setCurrentPageSize,
  payload
});

export const NextListingBooksFetchSuccessful: ActionCreator<
  NextListingBooksFetchSuccessfulAction
> = (payload: NextListingBooksFetchSuccessfulPayload) => ({
  type: BooksListingActionEnum.nextListingBooksFetchSuccessful,
  payload
});

export const FetchingUrlChanged: ActionCreator<FetchingUrlChangedAction> = (
  payload: string
) => ({
  type: BooksListingActionEnum.fetchingUrlChanged,
  payload
});

export const PageChanged: ActionCreator<PageChangedAction> = (
  payload: number
) => ({
  type: BooksListingActionEnum.pageChanged,
  payload
});

export const PageSizeChanged: ActionCreator<PageSizeChangedAction> = (
  payload: number
) => ({
  type: BooksListingActionEnum.pageSizeChanged,
  payload
});

export type BooksListingActions =
  | FetchInitialListingBooksAction
  | InitialListingBooksFetchSuccessfulAction
  | FetchNextListingBooksAction
  | NextListingBooksFetchSuccessfulAction
  | FetchingUrlChangedAction
  | SetCurrentPageNumberAction
  | SetCurrentPageSizeAction
  | FetchingUrlConstructedAction
  | ConstructFetchingUrlAction
  | ParseStartingPaginationAction
  | PageSizeChangedAction
  | FetchNextListingBooksErrorAction
  | PageChangedAction;
