import {
  BooksListingActions,
  FetchingUrlChanged,
  BooksListingActionEnum,
  FetchNextBooks,
  NextListingBooksFetchSuccessful,
  SetCurrentPageNumber,
  SetCurrentPageSize,
  FetchingUrlConstructed,
  ConstructFetchingUrl,
  FetchNextListingBooksError
} from "./books.listing.actions";
import { ActionsObservable, StateObservable, ofType } from "redux-observable";
import { ApplicationState } from "@store/root/root.reducer";
import { Observable, from, of } from "rxjs";
import { RouterAction } from "connected-react-router";
import {
  FilterUrlChangedAction,
  BooksFilterActionEnum
} from "@books/BooksFilter/store/books.filter.actions";
import { toPayload, toData } from "@store/root/store.utils";
import {
  map,
  switchMap,
  withLatestFrom,
  concatMap,
  tap,
  catchError
} from "rxjs/operators";

import axios from "axios";
import { RxjsRequestCache } from "@shared/RxjsRequestCache/RxjsRequestCache";
import { Book } from "@book/store/books.entity.reducer";
import {
  AddBooksToStore,
  AddBooksToStoreAction
} from "@book/store/books.entity.action";
import {
  composeRight,
  removeDuplicateElementsFromArray,
  filterDataBasedOnArray
} from "@utils/functions";
import { possiblePageSize } from "./books.listing.reducer";
const rxjsCache = new RxjsRequestCache();

type BooksListingEpic = (
  action$: ActionsObservable<BooksListingActions | FilterUrlChangedAction>,
  state$: StateObservable<ApplicationState>
) => Observable<BooksListingActions | RouterAction | AddBooksToStoreAction>;

interface FetchingUrlResponse {
  books: Book[];
  count: number;
}

const parseStartingPaginationEpic: BooksListingEpic = (action$, state$) =>
  action$.pipe(
    ofType(BooksListingActionEnum.parseStartingPagination),
    map(() => getPaginationParamsFromUrl(state$.value.router.location.search)),
    map(({ pageNumber, pageSize }) => ({
      pageNumber: toValidPageNumber(pageNumber),
      pageSize: toValidPageSize(pageSize)
    })),
    concatMap(({ pageNumber, pageSize }) => [
      SetCurrentPageNumber(pageNumber),
      SetCurrentPageSize(pageSize),
      ConstructFetchingUrl(state$.value.booksFilter.currentFilterUrl)
    ])
  );

const constructFetchingUrlEpic: BooksListingEpic = (action$, state$) =>
  action$.pipe(
    ofType(
      BooksFilterActionEnum.filterUrlChanged,
      BooksListingActionEnum.constructFetchingUrl,
      BooksListingActionEnum.pageChanged,
      BooksListingActionEnum.pageSizeChanged
    ),
    map(
      [toPayload, toConstructFetchingUrlData(state$), toFetchingUrl].reduce(
        composeRight
      )
    ),
    tap((fetchingUrl: string) => history.pushState("null", "", fetchingUrl)),
    concatMap(fetchingUrl => [
      FetchingUrlConstructed(),
      FetchingUrlChanged(fetchingUrl),
      FetchNextBooks()
    ])
  );

const fetchNextBooksEpic: BooksListingEpic = (action$, state$) =>
  action$.pipe(
    ofType(BooksListingActionEnum.fetchNextListingBooks),
    withLatestFrom(state$, (action, state) => state.booksListing.fetchingUrl),
    switchMap(fetchingUrl =>
      cachedFetch(fetchingUrl).pipe(
        map(toData),
        concatMap((fetchingUrlResponse: FetchingUrlResponse) => [
          NextListingBooksFetchSuccessful({
            numberOfBooksForFetchingUrl: fetchingUrlResponse.count,
            fetchedBooks: fetchingUrlResponse.books
          }),
          AddBooksToStore(fetchingUrlResponse.books)
        ]),
        catchError(() => of(FetchNextListingBooksError()))
      )
    )
  );

const toConstructFetchingUrlData = (
  state: StateObservable<ApplicationState>
) => (data: string | number) => {
  return typeof data === "string" ? { filterUrl: data, state } : { state };
};

function getPaginationParamsFromUrl(searchParamsFromUrl: string) {
  const searchParams = new URLSearchParams(searchParamsFromUrl);
  return {
    pageNumber: searchParams.get("page"),
    pageSize: searchParams.getAll("pageSize")
  };
}

function toFetchingUrl({
  state,
  filterUrl = state.value.booksListing.fetchingUrl
}: {
  filterUrl?: string;
  state: StateObservable<ApplicationState>;
}): string {
  const replaceRegexp = new RegExp("(&page=[0-9].*)|(&pageSize=[0-9]{,4})");
  const replaced = filterUrl.replace(replaceRegexp, "");

  return (
    replaced +
    `&page=${state.value.booksListing.currentPage}&pageSize=${
      state.value.booksListing.currentPageSize
    }`
  );
}

function toValidPageSize(pageSizeArr: string[]) {
  if (pageSizeArr.length === 0) {
    return 10;
  }
  const noDuplicates = removeDuplicateElementsFromArray(pageSizeArr);
  const validated = filterDataBasedOnArray(noDuplicates, possiblePageSize);
  if (validated.length === 0) {
    return 10;
  }
  return Number(validated[0]);
}

function toValidPageNumber(pageNumber: string | null): number {
  if (!pageNumber || isNaN(Number(pageNumber)) || Number(pageNumber) <= 0) {
    return 1;
  }
  const rounded = Math.round(Number(pageNumber));
  return rounded <= 0 ? 1 : rounded;
}

const cachedFetch = rxjsCache.cacheFunctionCall(fetchBooks);

function fetchBooks(fetchingUrl: string) {
  return from(axios("/book/paginate" + fetchingUrl));
}

export const BooksListingEpics = [
  constructFetchingUrlEpic,
  fetchNextBooksEpic,
  parseStartingPaginationEpic
];
