import {
  BooksListingActions,
  BooksListingActionEnum
} from "./books.listing.actions";

import { Book } from "@book/store/books.entity.reducer";
import {
  ResetFiltersAction,
  FilterUrlChangedAction,
  BooksFilterActionEnum
} from "@books/BooksFilter/store/books.filter.actions";

export const possiblePageSize = ["10", "20", "30", "40"];

export interface BooksListingState {
  fetchingUrl: string;
  fetchedBooks: Book[];
  numberOfBooksForFetchingUrl: number;
  currentPage: number;
  currentPageSize: number;
  isFetchingInitially: boolean;
  isFetchingNewBooks: boolean;
  fetchingErrorOccurred: boolean;
}

export const initialBooksListingState: BooksListingState = {
  fetchingUrl: "",
  fetchedBooks: [],
  numberOfBooksForFetchingUrl: 0,
  currentPage: 1,
  currentPageSize: 10,
  isFetchingInitially: true,
  isFetchingNewBooks: false,
  fetchingErrorOccurred: false
};

export function BooksListingReducer(
  state: BooksListingState = initialBooksListingState,
  action: BooksListingActions | ResetFiltersAction | FilterUrlChangedAction
): BooksListingState {
  switch (action.type) {
    case BooksFilterActionEnum.filterUrlChanged:
      return {
        ...state,
        currentPage: 1
      };
    case BooksListingActionEnum.fetchNextListingBooksError:
      return {
        ...state,
        isFetchingInitially: false,
        isFetchingNewBooks: false,
        fetchingErrorOccurred: true
      };
    case BooksFilterActionEnum.resetFilters:
      return {
        ...initialBooksListingState,
        fetchedBooks: state.fetchedBooks,
        isFetchingInitially: false
      };
    case BooksListingActionEnum.fetchingUrlChanged:
      return {
        ...state,
        fetchingUrl: action.payload,
        isFetchingNewBooks: true
      };
    case BooksListingActionEnum.setCurrentPageNumber:
      return {
        ...state,
        currentPage: action.payload
      };
    case BooksListingActionEnum.pageChanged:
      return {
        ...state,
        currentPage: action.payload,
        isFetchingNewBooks: true
      };
    case BooksListingActionEnum.setCurrentPageSize:
      return {
        ...state,
        currentPageSize: action.payload
      };
    case BooksListingActionEnum.pageSizeChanged:
      return {
        ...state,
        currentPageSize: action.payload,
        currentPage: 1,
        isFetchingNewBooks: true
      };
    case BooksListingActionEnum.nextListingBooksFetchSuccessful:
      return {
        ...state,
        isFetchingInitially: false,
        isFetchingNewBooks: false,
        fetchingErrorOccurred: false,
        fetchedBooks: action.payload.fetchedBooks,
        numberOfBooksForFetchingUrl: action.payload.numberOfBooksForFetchingUrl
      };
    default:
      return state;
  }
}
