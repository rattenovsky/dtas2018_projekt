import * as React from "react";
import "@books/BooksListing/BooksListing.css";

import "@shared/Css/Utils.css";
import "@books/BooksListing/BooksListing.css";

import { BooksListingLoadingView } from "@books/BooksListing/BooksListingLoadingView";

import { BooksGridView } from "@books/BooksGrid/BooksGridView";

import { Divider, Button } from "antd";
import { BooksPaginationConnector } from "./BooksPagination/BooksPaginationConnector";

import { Book } from "@book/store/books.entity.reducer";
import { PosedAppLoader } from "@utils/Loaders/AppLoader/PosedAppLoader";
import { Exception } from "@shared/Exception/Exception";

interface BooksListingViewProps {
  books: Book[];
  isFetchingInitially: boolean;
  isFetchingNewBooks: boolean;
  resetFilters: VoidFunction;
  hasNotDefaultUrl: boolean;
  fetchErrorOccurred: boolean;
  refetchHandlerOnError: VoidFunction;
}

export class BooksListingView extends React.PureComponent<
  BooksListingViewProps,
  {}
> {
  public render() {
    return (
      <React.Fragment>
        <PosedAppLoader
          isVisible={
            !this.props.isFetchingInitially && this.props.isFetchingNewBooks
          }
        />
        <div className="BooksListingContent ">
          {this.props.isFetchingInitially ? <BooksListingLoadingView /> : null}
          {this.shouldRenderBooksListing() ? this.renderBooksListing() : null}
          {this.shouldRenderResetFilers() ? this.renderResetFilters() : null}
          {this.shouldRenderNoBooks() ? this.renderNoBooksFallback() : null}
          {this.shouldRenderError() ? (
            <Exception
              style={{ padding: 0 }}
              onRetryClick={this.props.refetchHandlerOnError}
            />
          ) : null}
        </div>
      </React.Fragment>
    );
  }

  private renderResetFilters() {
    return (
      <div
        className="FlexFullCentered"
        style={{ width: "100%", flexDirection: "column" }}
      >
        <h1 className="BooksFeedbackHeading">No books fit such criteria</h1>
        <Button onClick={this.props.resetFilters}>Reset Filters</Button>
      </div>
    );
  }

  private renderNoBooksFallback() {
    return (
      <div
        className="FlexFullCentered"
        style={{ width: "100%", flexDirection: "column" }}
      >
        <h1 className="BooksFeedbackHeading">
          There are no books in database :C
        </h1>
      </div>
    );
  }

  private shouldRenderError() {
    return (
      !this.props.isFetchingInitially &&
      !this.props.isFetchingNewBooks &&
      this.props.fetchErrorOccurred
    );
  }

  private shouldRenderBooksListing() {
    return (
      !this.props.isFetchingInitially &&
      this.props.books.length > 0 &&
      !this.props.fetchErrorOccurred
    );
  }
  private shouldRenderNoBooks() {
    return (
      !this.props.isFetchingInitially &&
      !this.props.hasNotDefaultUrl &&
      !this.props.isFetchingNewBooks &&
      this.props.books.length <= 0 &&
      !this.props.fetchErrorOccurred
    );
  }
  private shouldRenderResetFilers() {
    return (
      this.props.hasNotDefaultUrl &&
      !this.props.isFetchingInitially &&
      this.props.books.length <= 0
    );
  }

  private renderBooksListing() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "100%"
        }}
      >
        <BooksGridView books={this.props.books} />
        <Divider />
        <BooksPaginationConnector />
      </div>
    );
  }
}
