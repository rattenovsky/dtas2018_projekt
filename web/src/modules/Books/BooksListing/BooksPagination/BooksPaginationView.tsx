import * as React from "react";
import { Pagination } from "antd";
import { useMedia } from "@shared/Hooks/useMedia";
import { PaginationProps } from "antd/lib/pagination";

export const BooksPaginationView: React.SFC<PaginationProps> = props => {
  const isOnMobile = useMedia("(max-width: 680px)");
  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        justifyContent: isOnMobile ? "center" : "initial"
      }}
    >
      <div style={{ display: "inline-block" }}>
        <Pagination
          showSizeChanger={props.total > 10}
          size={isOnMobile ? "small" : "normal"}
          defaultCurrent={1}
          total={props.total}
          {...props}
          style={{ marginLeft: "auto", marginRight: "auto" }}
        />
      </div>
    </div>
  );
};
