import * as React from "react";
import { BooksPaginationView } from "./BooksPaginationView";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
  BooksListingActions,
  PageChanged,
  PageSizeChanged
} from "../store/books.listing.actions";
import { ApplicationState } from "@store/root/root.reducer";

interface DispatchProps {
  paginationPageChanged: (page: number) => void;
  paginationPageSizeChanged: (pageSize: number) => void;
}

interface StateProps {
  currentPage: number;
  currentPageSize: number;
  numberOfBooksForFetchingUrl: number;
}

export class C extends React.Component<DispatchProps & StateProps, {}> {
  public render() {
    return (
      <BooksPaginationView
        pageSize={this.props.currentPageSize}
        total={this.props.numberOfBooksForFetchingUrl}
        current={this.props.currentPage}
        onChange={this.onChangeHandler}
        onShowSizeChange={this.onShowSizeChangeHandler}
      />
    );
  }

  private onChangeHandler = (page: number, pageSize: number) => {
    this.props.paginationPageChanged(page);
    window.scrollTo(0, 0);
  };

  private onShowSizeChangeHandler = (current: number, size: number) => {
    this.props.paginationPageSizeChanged(size);
  };
}

const mapDispatchToProps = (
  dispatch: Dispatch<BooksListingActions>
): DispatchProps => ({
  paginationPageChanged: page => dispatch(PageChanged(page)),
  paginationPageSizeChanged: pageSize => dispatch(PageSizeChanged(pageSize))
});

const mapStateToProps = ({ booksListing }: ApplicationState): StateProps => ({
  currentPage: booksListing.currentPage,
  currentPageSize: booksListing.currentPageSize,
  numberOfBooksForFetchingUrl: booksListing.numberOfBooksForFetchingUrl
});

export const BooksPaginationConnector = connect<StateProps, DispatchProps, {}>(
  mapStateToProps,
  mapDispatchToProps
)(C);
