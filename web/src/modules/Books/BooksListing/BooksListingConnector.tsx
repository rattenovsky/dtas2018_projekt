import * as React from "react";
import { BooksListingView } from "@books/BooksListing/BooksListingView";
import { ApplicationState } from "@store/root/root.reducer";
import { connect } from "react-redux";
import { Book } from "@book/store/books.entity.reducer";
import { Dispatch } from "redux";
import {
  ResetFiltersAction,
  ResetFilters
} from "@books/BooksFilter/store/books.filter.actions";
import {
  FetchNextBooks,
  FetchNextListingBooksAction
} from "./store/books.listing.actions";

interface StateProps {
  isFetchingInitially: boolean;
  isFetchingNewBooks: boolean;
  fetchingUrl: string;
  fetchedBooks: Book[];
  hasNotDefaultUrl: boolean;
  fetchErrorOccurred: boolean;
}

interface DispatchProps {
  resetFilters: VoidFunction;
  fetchNextListingBooks: VoidFunction;
}

class C extends React.PureComponent<StateProps & DispatchProps, {}> {
  public componentDidMount() {
    !this.props.isFetchingInitially &&
      history.pushState("null", "", this.props.fetchingUrl);
  }
  public render() {
    return (
      <BooksListingView
        hasNotDefaultUrl={this.props.hasNotDefaultUrl}
        resetFilters={this.props.resetFilters}
        isFetchingNewBooks={this.props.isFetchingNewBooks}
        books={this.props.fetchedBooks}
        isFetchingInitially={this.props.isFetchingInitially}
        refetchHandlerOnError={this.props.fetchNextListingBooks}
        fetchErrorOccurred={this.props.fetchErrorOccurred}
      />
    );
  }
}

const mapStateToProps = ({
  booksListing,
  booksFilter
}: ApplicationState): StateProps => ({
  isFetchingInitially: booksListing.isFetchingInitially,
  isFetchingNewBooks: booksListing.isFetchingNewBooks,
  fetchedBooks: booksListing.fetchedBooks,
  fetchingUrl: booksListing.fetchingUrl,
  hasNotDefaultUrl:
    areAnyFiltersSelected(
      [
        booksFilter.selectedAuthors,
        booksFilter.selectedLanguages,
        booksFilter.selectedPublishers
      ]
    ) || booksListing.currentPage !== 1,
  fetchErrorOccurred: booksListing.fetchingErrorOccurred
});

const areAnyFiltersSelected = (filtersArr: string[][]): boolean =>
  filtersArr.reduce((acc: number, filterArr: string[]) => {
    acc += filterArr.length;
    return acc;
  }, 0) > 0;

const mapDispatchToProps = (
  dispatch: Dispatch<ResetFiltersAction | FetchNextListingBooksAction>
): DispatchProps => ({
  resetFilters: () => dispatch(ResetFilters()),
  fetchNextListingBooks: () => dispatch(FetchNextBooks())
});

export const BooksListingConnector = connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps
)(C);
