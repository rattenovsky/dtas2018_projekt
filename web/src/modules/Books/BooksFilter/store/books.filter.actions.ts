import { Action, ActionCreator } from "redux";

import { BookPublisher, BookAuthor } from "@book/store/books.entity.reducer";

export enum BooksFilterActionEnum {
  fetchFilters = "@@BooksFilterConnector FETCH_FILTERS",
  filtersFetchedSuccessfully = "@@BooksFilterEpics FILTERS_FETCHED_SUCCESSFULLY",
  addAuthorsToStore = "@@BooksFilterEpics ADD_AUTHORS_TO_STORE",
  addPublishersToStore = "@@BooksFilterEpics ADD_PUBLISHERS_TO_STORE",
  parseStartingUrl = "@@BooksFilterEpics PARSE_STARTING_URL",
  startingUrlParsed = "@@BooksFilterEpics STARTING_URL_PARSED",
  filterUrlConstructed = "@@BooksFilterEpics FILTER_URL_CONSTRUCTED",
  filterUrlChanged = "@@BooksFilterEpics FILTER_URL_CHANGED",
  setFilterUrl = "@@BooksFilterEpics SET_FILTER_URL",
  filtersFetchError = "@@BooksFilterEpics FILTER_FETCH_ERROR",
  filterSelected = "@@BooksFilters FILTER_SELECTED",
  filterDeselected = "@@BooksFilters FILTER_DESELECTED",
  constructFilterUrl = "@@BooksFilterConnector CONSTRUCT_FILTER_URL",
  resetFilters = "@@BooksListing RESET_FILTERS"
}

export interface FilterPayload {
  filterType: "sort" | "author" | "publisher" | "language";
  filterValue: string[];
}

export interface FetchFiltersAction extends Action {
  type: BooksFilterActionEnum.fetchFilters;
}

export interface ResetFiltersAction extends Action {
  type: BooksFilterActionEnum.resetFilters;
}

export interface FiltersFetchErrorAction extends Action {
  type: BooksFilterActionEnum.filtersFetchError;
}

export interface StartingUrlParsedAction extends Action {
  type: BooksFilterActionEnum.startingUrlParsed;
}

export interface AddAuthorsToStoreAction extends Action {
  type: BooksFilterActionEnum.addAuthorsToStore;
  payload: BookAuthor[];
}

export interface SetFilterUrlAction extends Action {
  type: BooksFilterActionEnum.setFilterUrl;
  payload: string;
}
export interface ParseStartingUrlAction extends Action {
  type: BooksFilterActionEnum.parseStartingUrl;
}

export interface AddPublishersToStoreAction extends Action {
  type: BooksFilterActionEnum.addPublishersToStore;
  payload: BookPublisher[];
}

export interface FiltersFetchedSuccessfullyAction extends Action {
  type: BooksFilterActionEnum.filtersFetchedSuccessfully;
}

export interface FilterSelectedAction extends Action {
  type: BooksFilterActionEnum.filterSelected;
  payload: FilterPayload;
}

export interface FilterDeselectedAction extends Action {
  type: BooksFilterActionEnum.filterDeselected;
  payload: FilterPayload;
}

export interface ConstructFilterUrlAction extends Action {
  type: BooksFilterActionEnum.constructFilterUrl;
  payload?: boolean;
}

export interface FilterUrlConstructedAction extends Action {
  type: BooksFilterActionEnum.filterUrlConstructed;
}

export interface FilterUrlChangedAction extends Action {
  type: BooksFilterActionEnum.filterUrlChanged;
  payload: string;
}

export const StartingUrlParsed: ActionCreator<
  StartingUrlParsedAction
> = () => ({
  type: BooksFilterActionEnum.startingUrlParsed
});

export const FiltersFetchError: ActionCreator<
  FiltersFetchErrorAction
> = () => ({
  type: BooksFilterActionEnum.filtersFetchError
});

export const ResetFilters: ActionCreator<ResetFiltersAction> = () => ({
  type: BooksFilterActionEnum.resetFilters
});

export const ParseStartingUrl: ActionCreator<ParseStartingUrlAction> = () => ({
  type: BooksFilterActionEnum.parseStartingUrl
});

export const FetchFilters: ActionCreator<FetchFiltersAction> = () => ({
  type: BooksFilterActionEnum.fetchFilters
});

export const AddAuthorsToStore: ActionCreator<AddAuthorsToStoreAction> = (
  payload: BookAuthor[]
) => ({
  type: BooksFilterActionEnum.addAuthorsToStore,
  payload
});

export const AddPublishersToStore: ActionCreator<AddPublishersToStoreAction> = (
  payload: BookPublisher[]
) => ({
  type: BooksFilterActionEnum.addPublishersToStore,
  payload
});

export const SetFilterUrl: ActionCreator<SetFilterUrlAction> = (
  payload: string
) => ({
  type: BooksFilterActionEnum.setFilterUrl,
  payload
});

export const FiltersFetchedSuccessfully: ActionCreator<
  FiltersFetchedSuccessfullyAction
> = () => ({
  type: BooksFilterActionEnum.filtersFetchedSuccessfully
});

export const FilterSelected: ActionCreator<FilterSelectedAction> = (
  payload: FilterPayload
) => ({
  type: BooksFilterActionEnum.filterSelected,
  payload
});

export const FilterDeselected: ActionCreator<
  FilterDeselectedAction
> = payload => ({
  type: BooksFilterActionEnum.filterDeselected,
  payload
});

export const ConstructFilterUrl: ActionCreator<ConstructFilterUrlAction> = (
  payload: boolean = false
) => ({
  type: BooksFilterActionEnum.constructFilterUrl,
  payload
});

export const FilterUrlConstructed: ActionCreator<
  FilterUrlConstructedAction
> = () => ({
  type: BooksFilterActionEnum.filterUrlConstructed
});

export const FilterUrlChanged: ActionCreator<
  FilterUrlChangedAction
> = payload => ({
  type: BooksFilterActionEnum.filterUrlChanged,
  payload
});

export type BooksFilterActions =
  | AddPublishersToStoreAction
  | FiltersFetchedSuccessfullyAction
  | FilterSelectedAction
  | FilterDeselectedAction
  | ConstructFilterUrlAction
  | FilterUrlConstructedAction
  | FilterUrlChangedAction
  | ParseStartingUrlAction
  | FetchFiltersAction
  | SetFilterUrlAction
  | FiltersFetchErrorAction
  | StartingUrlParsedAction
  | ResetFiltersAction
  | AddAuthorsToStoreAction;
