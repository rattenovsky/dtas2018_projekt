import {
  BookPublisher,
  BookAuthorNoRelations
} from "@book/store/books.entity.reducer";
import {
  BooksFilterActions,
  BooksFilterActionEnum,
  FilterPayload
} from "./books.filter.actions";
import { schema, normalize } from "normalizr";

export type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

function getFiltersSchema(key: string) {
  return [new schema.Entity(key)];
}

interface NormalizedFiltersPublishers {
  entities: {
    [key: string]: BookPublisher;
  };
  result: string[];
}

interface NormalizedFiltersAuthors {
  entities: {
    [key: string]: BookAuthorNoRelations;
  };
  result: string[];
}

export interface BooksFilterState {
  selectedPublishers: string[];
  selectedAuthors: string[];
  selectedLanguages: string[];
  selectedSort: string[];
  publishers: NormalizedFiltersPublishers;
  authors: NormalizedFiltersAuthors;
  languages: string[];
  sort: string[];
  isFetchingFilters: boolean;
  hasFetchedFilters: boolean;
  currentFilterUrl: string;
}

export const initialBooksFilterState: BooksFilterState = {
  selectedPublishers: [],
  selectedAuthors: [],
  selectedSort: ["price DESC"],
  selectedLanguages: [],
  publishers: {
    entities: null,
    result: []
  },
  languages: ["Polish", "English", "German"],
  sort: ["price DESC", "price ASC"],
  authors: {
    entities: null,
    result: []
  },
  isFetchingFilters: false,
  hasFetchedFilters: false,
  currentFilterUrl: "?sort=price DESC"
};

export function BooksFilterReducer(
  state: BooksFilterState = initialBooksFilterState,
  action: BooksFilterActions
): BooksFilterState {
  switch (action.type) {
    case BooksFilterActionEnum.resetFilters:
      return {
        ...state,
        selectedAuthors: [],
        selectedLanguages: [],
        selectedPublishers: [],
        selectedSort: ["price DESC"],
        currentFilterUrl: ""
      };
    case BooksFilterActionEnum.fetchFilters:
      return {
        ...state,
        isFetchingFilters: true
      };
    case BooksFilterActionEnum.filtersFetchedSuccessfully:
      return {
        ...state,
        isFetchingFilters: false,
        hasFetchedFilters: true
      };
    case BooksFilterActionEnum.addPublishersToStore:
      const normalizedPublishers = normalize(
        action.payload,
        getFiltersSchema("publishers")
      );
      return {
        ...state,
        publishers: {
          entities: {
            ...normalizedPublishers.entities.publishers
          },
          result: normalizedPublishers.result
        }
      };
    case BooksFilterActionEnum.addAuthorsToStore:
      const normalizedAuthors = normalize(
        action.payload,
        getFiltersSchema("authors")
      );
      return {
        ...state,
        authors: {
          entities: {
            ...normalizedAuthors.entities.authors
          },
          result: normalizedAuthors.result
        }
      };
    case BooksFilterActionEnum.filterUrlChanged:
    case BooksFilterActionEnum.setFilterUrl:
      return {
        ...state,
        currentFilterUrl: action.payload
      };
    case BooksFilterActionEnum.filterSelected:
      return handleFilterSelected(action.payload, state);
    case BooksFilterActionEnum.filterDeselected:
      return handleFilterDeselect(action.payload, state);
    default:
      return state;
  }
}

function handleFilterSelected(
  { filterType, filterValue }: FilterPayload,
  state: BooksFilterState
): BooksFilterState {
  switch (filterType) {
    case "sort":
      return {
        ...state,
        selectedSort: filterValue
      };
    case "language":
      return {
        ...state,
        selectedLanguages: [...state.selectedLanguages, ...filterValue]
      };
    case "publisher":
      return {
        ...state,
        selectedPublishers: [...state.selectedPublishers, ...filterValue]
      };
    case "author":
      return {
        ...state,
        selectedAuthors: [...state.selectedAuthors, ...filterValue]
      };
    default:
      return state;
  }
}

function handleFilterDeselect(
  { filterType, filterValue }: FilterPayload,
  state: BooksFilterState
): BooksFilterState {
  let index;
  switch (filterType) {
    case "language":
      index = state.selectedLanguages.indexOf(filterValue[0]);
      return {
        ...state,
        selectedLanguages: removeImmutableFromArray(
          state.selectedLanguages,
          index
        )
      };
    case "author":
      index = state.selectedAuthors.indexOf(filterValue[0]);
      return {
        ...state,
        selectedAuthors: removeImmutableFromArray(state.selectedAuthors, index)
      };
    case "publisher":
      index = state.selectedPublishers.indexOf(filterValue[0]);
      return {
        ...state,
        selectedPublishers: removeImmutableFromArray(
          state.selectedPublishers,
          index
        )
      };
    default:
      return state;
  }
}

const removeImmutableFromArray = <T>(arr: T[], index: number): T[] => [
  ...arr.slice(0, index),
  ...arr.slice(index + 1)
];
