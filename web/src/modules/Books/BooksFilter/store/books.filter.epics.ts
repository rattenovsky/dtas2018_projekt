import { message } from "antd";
import axios from "axios";
import { ActionsObservable, ofType, StateObservable } from "redux-observable";

import { Observable, from, forkJoin, of } from "rxjs";
import { ApplicationState } from "@store/root/root.reducer";
import { RouterAction } from "connected-react-router";
import {
  BooksFilterActions,
  BooksFilterActionEnum,
  AddAuthorsToStore,
  AddPublishersToStore,
  FilterUrlConstructed,
  ParseStartingUrl,
  FiltersFetchedSuccessfully,
  FilterSelected,
  StartingUrlParsed,
  ConstructFilterUrl,
  FilterSelectedAction,
  ConstructFilterUrlAction,
  SetFilterUrl,
  FilterUrlChanged,
  FiltersFetchError
} from "./books.filter.actions";
import {
  map,
  concatMap,
  withLatestFrom,
  catchError,
  tap,
  switchMap
} from "rxjs/operators";
import { toData } from "@store/root/store.utils";
import { BookAuthor, BookPublisher } from "@book/store/books.entity.reducer";
import {
  composeRight,
  removeDuplicateElementsFromArray,
  filterDataBasedOnArray,
  mapReducer,
  filterReducer,
  pushCombiner,
  transduce
} from "@utils/functions";
import { BooksFilterState } from "./books.filter.reducer";
import {
  BooksListingActions,
  ParseStartingPagination
} from "@books/BooksListing/store/books.listing.actions";
import { RetryFetchMessageContent } from "../BooksFilterView";

type BooksFilterEpic = (
  action$: ActionsObservable<BooksFilterActions>,
  state$: StateObservable<ApplicationState>
) => Observable<BooksFilterActions | RouterAction | BooksListingActions>;

const fetchFiltersEpic: BooksFilterEpic = (action$, state) =>
  action$.pipe(
    ofType(BooksFilterActionEnum.fetchFilters),
    tap(() => message.destroy()),
    switchMap(() =>
      forkJoin(
        booksFilterFetcher("/author?noBooks=true"),
        booksFilterFetcher("/publisher")
      ).pipe(
        concatMap(([authors, publishers]: [BookAuthor[], BookPublisher[]]) => [
          AddPublishersToStore(publishers),
          AddAuthorsToStore(authors),
          FiltersFetchedSuccessfully(),
          ParseStartingUrl()
        ]),
        catchError(() => {
          message.error(RetryFetchMessageContent, 0);
          return of(FiltersFetchError());
        })
      )
    )
  );

const parseStartingUrlEpic: BooksFilterEpic = (action$, state) =>
  action$.pipe(
    ofType(BooksFilterActionEnum.parseStartingUrl),
    map(() =>
      getFiltersFromUrl(
        state.value.router.location.search,
        state.value.booksFilter
      )
    ),
    map(filtersFromUrl =>
      filtersFromUrl.reduce(startingUrlParseFunctionsTransducer, [])
    ),
    concatMap(filterSelectedActions => {
      return [
        StartingUrlParsed(),
        ...filterSelectedActions,
        ConstructFilterUrl(true)
      ];
    })
  );

const resetFiltersEpic: BooksFilterEpic = (action$, state$) =>
  action$.pipe(
    ofType(BooksFilterActionEnum.resetFilters),
    map(() => ConstructFilterUrl(false))
  );

const constructUrlEpic: BooksFilterEpic = (action$, state$) =>
  action$.pipe(
    ofType(BooksFilterActionEnum.constructFilterUrl),
    withLatestFrom(
      state$,
      (
        action: ConstructFilterUrlAction,
        state
      ): [BooksFilterState, string, boolean] => {
        return [state.booksFilter, "?", action.payload];
      }
    ),
    map(
      [
        toSortIntoUrl,
        toKeysIntoUrl(
          ["publisher", "selectedPublishers"],
          ["author", "selectedAuthors"],
          ["language", "selectedLanguages"]
        )
      ].reduce(composeRight)
    ),
    map(([newUrl, isFromInitialLoad]) => [
      state$.value.booksFilter.currentFilterUrl,
      newUrl,
      isFromInitialLoad
    ]),
    concatMap(([currentUrl, newUrl, isFromInitialLoad]) => {
      if (isFromInitialLoad) {
        return [
          FilterUrlConstructed(),
          SetFilterUrl(newUrl) as any,
          ParseStartingPagination()
        ];
      }
      return currentUrl !== newUrl
        ? [FilterUrlConstructed(), FilterUrlChanged(newUrl)]
        : [];
    })
  );

function getFiltersFromUrl(
  searchParamsFromUrl: string,
  state: BooksFilterState
): SelectedFilterFromUrl[] {
  const searchParams = new URLSearchParams(searchParamsFromUrl);
  const authorsFromUrl = searchParams.getAll("author");
  const publishersFromUrl = searchParams.getAll("publisher");
  const languagesFromUrl = searchParams.getAll("language");
  const sortFromUrl = searchParams.getAll("sort");

  return [
    {
      urlKey: "author",
      storeKey: "selectedAuthors",
      data: authorsFromUrl,
      filterAgainst: state.authors.result
    },
    {
      urlKey: "publisher",
      storeKey: "selectedPublishers",
      data: publishersFromUrl,
      filterAgainst: state.publishers.result
    },
    {
      urlKey: "language",
      storeKey: "selectedLanguages",
      data: languagesFromUrl,
      filterAgainst: state.languages
    },
    {
      urlKey: "sort",
      storeKey: "selectedSort",
      data: sortFromUrl,
      filterAgainst: state.sort
    }
  ];
}

interface SelectedFilterFromUrl {
  urlKey: string;
  storeKey: string;
  data: string[];
  filterAgainst: string[];
}

const booksFilterFetcher = (path: string) =>
  from(axios.get(path)).pipe(map(toData));

const toKeysIntoUrl = (...nameKeyArray: string[][]): UrlMappingFunction => ([
  booksFilterState,
  url,
  isFromInitialLoad
]) => [
  nameKeyArray.reduce(
    (currentUrl: string, [nextName, nextKey]) =>
      appendToUrl(nextName, currentUrl, booksFilterState[nextKey]),
    url
  ),
  isFromInitialLoad
];

const toNotEmptyFilterFromUrl = (filterMatches: SelectedFilterFromUrl) => {
  return filterMatches.data && filterMatches.data.length > 0;
};

const toRemovedDuplicates = (filterFromUrl: SelectedFilterFromUrl) => {
  return {
    ...filterFromUrl,
    data: removeDuplicateElementsFromArray(filterFromUrl.data)
  };
};

const toValidatedFilter = (filterFromUrl: SelectedFilterFromUrl) => {
  const filteredData = filterDataBasedOnArray(
    filterFromUrl.data,
    filterFromUrl.filterAgainst
  );

  const dataToReturn =
    filteredData.length > 0 && filterFromUrl.urlKey === "price"
      ? filteredData[0]
      : filteredData;

  return {
    ...filterFromUrl,
    data: dataToReturn
  };
};

const toFilterSelectedAction = (
  filterFromUrl: SelectedFilterFromUrl
): FilterSelectedAction => {
  return FilterSelected({
    filterType: filterFromUrl.urlKey,
    filterValue: filterFromUrl.data
  });
};

const toSortIntoUrl = ([state, url, isFromInitialLoad]: [
  BooksFilterState,
  string,
  boolean
]) => {
  const changedUrl = url + `sort=${state.selectedSort}`;
  return [state, changedUrl, isFromInitialLoad] as any;
};

function appendToUrl(
  urlKey: string,
  url: string,
  stateArray: string[]
): string {
  return stateArray.reduce(
    (currentUrl: string, arrayItem: string) =>
      currentUrl + `&${urlKey}=${arrayItem}`,
    url
  );
}

const startingUrlParseFunctionsTransducer = transduce(
  filterReducer(toNotEmptyFilterFromUrl),
  mapReducer(toRemovedDuplicates),
  mapReducer(toValidatedFilter),
  mapReducer<SelectedFilterFromUrl, FilterSelectedAction>(
    toFilterSelectedAction
  )
)(pushCombiner);

type UrlMappingFunction = (
  [state, url, isFromInitialLoad]: [BooksFilterState, string, boolean]
) => [string, boolean];

export const BooksFilterEpics = [
  fetchFiltersEpic,
  parseStartingUrlEpic,
  constructUrlEpic,
  resetFiltersEpic
];
