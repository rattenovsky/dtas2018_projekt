import * as React from "react";

import { Layout, Menu, Icon } from "antd";

const { Sider } = Layout;
import "@books/BooksFilter/BooksFilter.css";

import { FilterPayload, FetchFilters } from "./store/books.filter.actions";
import { SelectParam } from "antd/lib/menu";
import { BooksFilterMask } from "./BooksFilterMask/BooksFilterMask";
import { Omit, BooksFilterState } from "./store/books.filter.reducer";
import { store } from "src";

const { SubMenu } = Menu;

interface MethodProps {
  filterSelected: (payload: FilterPayload) => void;
  filterDeselected: (payload: FilterPayload) => void;
  siderStateChangeHandler: () => void;
}

type StateProps = Omit<
  BooksFilterState,
  "isFetchingFilters" | "hasFetchedFilters" | "currentFilterUrl" | "sort"
>;

interface UiProps {
  isSiderCollapsed: boolean;
}

interface State {
  selectedFilters: string[];
}

// pure component here allows us to skip re-render on some icons
export class BooksFilterView extends React.PureComponent<
  MethodProps & StateProps & UiProps,
  State
> {
  public state: State = {
    selectedFilters: []
  };

  public render() {
    const selectedKeys: string[] = [
      ...this.props.selectedLanguages,
      ...this.props.selectedPublishers,
      ...this.props.selectedAuthors,
      ...this.props.selectedSort
    ];

    return (
      <React.Fragment>
        <BooksFilterMask
          siderStateChangeHandler={this.localSiderStateChangeHandler}
          isSiderCollapsed={this.props.isSiderCollapsed}
        />
        <Sider
          width={256}
          collapsible={true}
          collapsed={this.props.isSiderCollapsed}
          collapsedWidth={64}
          defaultCollapsed={true}
          theme="light"
          className="BooksFilterSider"
          onCollapse={this.localSiderStateChangeHandler}
        >
          <Menu
            theme="light"
            mode="inline"
            multiple={true}
            openKeys={
              !this.props.isSiderCollapsed ? this.state.selectedFilters : []
            }
            inlineCollapsed={true}
            defaultSelectedKeys={selectedKeys}
            selectedKeys={selectedKeys}
            onSelect={this.onKeySelectedLocalHandler}
            onDeselect={this.onKeyDeselectedLocalHandler}
          >
            <SubMenu
              multiple={false}
              disabled={this.props.isSiderCollapsed}
              onTitleClick={this.modifySelectedFilters}
              key="price"
              title={
                <span>
                  <Icon type="dollar" />
                  <span>Price</span>
                </span>
              }
            >
              <Menu.Item key="price ASC" type="sort" value={["price ASC"]}>
                Ascending
              </Menu.Item>
              <Menu.Item key="price DESC" type="sort" value={["price DESC"]}>
                Descending
              </Menu.Item>
            </SubMenu>

            <SubMenu
              multiple={true}
              disabled={this.props.isSiderCollapsed}
              key="language"
              onTitleClick={this.modifySelectedFilters}
              title={
                <span>
                  <Icon type="flag" />
                  <span>Language</span>
                </span>
              }
            >
              {this.props.languages.map((language: string, index: number) => (
                <Menu.Item key={language} type="language" value={[language]}>
                  {language}
                </Menu.Item>
              ))}
            </SubMenu>

            <SubMenu
              key="author"
              onTitleClick={this.modifySelectedFilters}
              disabled={this.props.isSiderCollapsed}
              title={
                <span>
                  <Icon type="user" />
                  <span>Authors</span>
                </span>
              }
            >
              {this.props.authors.result.map(authorId => (
                <Menu.Item key={authorId} value={[authorId]} type="author">
                  {this.props.authors.entities[authorId].firstName}{" "}
                  {this.props.authors.entities[authorId].lastName}
                </Menu.Item>
              ))}
            </SubMenu>

            <SubMenu
              onTitleClick={this.modifySelectedFilters}
              disabled={this.props.isSiderCollapsed}
              key="publisher"
              title={
                <span>
                  <Icon type="book" />
                  <span>Publishers</span>
                </span>
              }
            >
              {this.props.publishers.result.map(publisherId => (
                <Menu.Item
                  key={publisherId}
                  value={[publisherId]}
                  type="publisher"
                >
                  {this.props.publishers.entities[publisherId].name}
                </Menu.Item>
              ))}
            </SubMenu>
          </Menu>
        </Sider>
      </React.Fragment>
    );
  }

  private onKeySelectedLocalHandler = ({ item }: SelectParam) => {
    this.props.filterSelected({
      filterType: item.props.type,
      filterValue: item.props.value
    });
  };

  private readSelectedFiltersFromProps = () => {
    const selectedFilters = [
      this.props.selectedAuthors.length > 0 ? "author" : null,
      this.props.selectedLanguages.length > 0 ? "language" : null,
      this.props.selectedPublishers.length > 0 ? "publisher" : null
    ].filter(item => item != null);
    this.setState({ selectedFilters });
  };

  private modifySelectedFilters = ({ key }: { key: string }) => {
    if (this.state.selectedFilters.includes(key)) {
      this.setState(state => ({
        selectedFilters: state.selectedFilters.filter(filter => filter !== key)
      }));
    } else {
      this.setState(state => ({
        selectedFilters: [...state.selectedFilters, key]
      }));
    }
  };

  private localSiderStateChangeHandler = () => {
    if (this.props.isSiderCollapsed) {
      this.readSelectedFiltersFromProps();
    }
    this.props.siderStateChangeHandler();
  };

  private onKeyDeselectedLocalHandler = ({ item }: SelectParam) => {
    this.props.filterDeselected({
      filterType: item.props.type,
      filterValue: item.props.value
    });
  };
}

export const RetryFetchMessageContent = (
  <span>
    Failed to fetch filters {/* tslint:disable-next-line:jsx-no-lambda */}
    <a onClick={() => store.dispatch(FetchFilters())}>try again</a>
  </span>
);
