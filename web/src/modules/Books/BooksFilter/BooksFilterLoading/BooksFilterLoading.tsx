import * as React from "react";

export const BooksFilterLoading: React.SFC = () => (
  <div className={"BooksFilterLoader"} />
);
