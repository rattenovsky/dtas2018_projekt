import * as React from "react";

interface Props {
  siderStateChangeHandler: () => void;
  isSiderCollapsed: boolean;
}

export const BooksFilterMask: React.SFC<Props> = ({
  siderStateChangeHandler,
  isSiderCollapsed
}) => {
  return (
    <React.Fragment>
      <div
        onClick={siderStateChangeHandler}
        className={
          "BooksFilterMask " + (!isSiderCollapsed ? "MaskVisible" : "")
        }
      />
      {!isSiderCollapsed ? (
        <style>
          {`body {
            overflow: hidden;
          }`}
        </style>
      ) : null}
    </React.Fragment>
  );
};
