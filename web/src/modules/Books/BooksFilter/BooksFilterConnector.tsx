import * as React from "react";
import { BooksFilterView } from "./BooksFilterView";
import { BooksFilterLoading } from "./BooksFilterLoading/BooksFilterLoading";
import { Dispatch } from "redux";
import {
  BooksFilterActions,
  FilterPayload,
  FilterSelected,
  FilterDeselected,
  ConstructFilterUrl,
  FetchFilters
} from "./store/books.filter.actions";
import { connect } from "react-redux";

import { ApplicationState } from "@store/root/root.reducer";
import { BooksFilterState } from "./store/books.filter.reducer";

interface State {
  isSiderCollapsed: boolean;
}

type StateProps = BooksFilterState;

interface DispatchProps {
  fetchFilters: () => void;
  filterSelected: (payload: FilterPayload) => void;
  filterDeselected: (payload: FilterPayload) => void;
  constructFilterUrl: () => void;
}

class C extends React.PureComponent<DispatchProps & StateProps, State> {
  public state = {
    isSiderCollapsed: true
  };

  public componentDidMount() {
    !this.props.hasFetchedFilters && this.props.fetchFilters();
  }

  public render() {
    return (
      <React.Fragment>
        {this.props.isFetchingFilters ? (
          <BooksFilterLoading />
        ) : (
          <BooksFilterView
            selectedLanguages={this.props.selectedLanguages}
            selectedPublishers={this.props.selectedPublishers}
            selectedSort={this.props.selectedSort}
            selectedAuthors={this.props.selectedAuthors}
            filterSelected={this.props.filterSelected}
            filterDeselected={this.props.filterDeselected}
            languages={this.props.languages}
            publishers={this.props.publishers}
            authors={this.props.authors}
            siderStateChangeHandler={this.siderStateChangeHandler}
            isSiderCollapsed={this.state.isSiderCollapsed}
          />
        )}
      </React.Fragment>
    );
  }

  private siderStateChangeHandler = () => {
    !this.state.isSiderCollapsed ? this.props.constructFilterUrl() : null;
    this.setState(({ isSiderCollapsed }) => ({
      isSiderCollapsed: !isSiderCollapsed
    }));
  };
}

const mapDispatchToProps = (
  dispatch: Dispatch<BooksFilterActions>
): DispatchProps => ({
  fetchFilters: () => dispatch(FetchFilters()),
  filterSelected: filterPayload => dispatch(FilterSelected(filterPayload)),
  filterDeselected: filterPayload => dispatch(FilterDeselected(filterPayload)),
  constructFilterUrl: () => dispatch(ConstructFilterUrl())
});

const mapStateToProps = ({ booksFilter }: ApplicationState): StateProps => ({
  ...booksFilter
});

export const BooksFilterConnector = connect<StateProps, DispatchProps, any>(
  mapStateToProps,
  mapDispatchToProps
)(C);
