import * as React from "react";

import { BookCardView } from "@book/BookCard/BookCardView";
import "@shared/Css/Grid.css";

import { Book } from "@book/store/books.entity.reducer";

interface Props {
  books: Book[];
}

export const BooksGridView: React.SFC<Props> = ({ books }) => {
  const Books = books.map((book: Book) => (
    <div key={book.id} className="GridItem">
      <BookCardView {...book} />
    </div>
  ));

  return (
    <div className="Grid" style={{ flex: 1 }}>
      {Books}
    </div>
  );
};
