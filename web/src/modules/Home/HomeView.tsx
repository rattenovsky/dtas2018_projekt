import * as React from "react";
import { Layout } from "antd";
import { BooksListingConnector } from "@books/BooksListing/BooksListingConnector";
import { BooksFilterConnector } from "@books/BooksFilter/BooksFilterConnector";

const { Content } = Layout;

export const HomeView: React.SFC = () => (
  <div style={{ padding: 0 }}>
    <BooksFilterConnector />
    <Content>
      <BooksListingConnector />
    </Content>
  </div>
);
