import * as React from "react";
import "./AddBook.css";
import { CoverUploader } from "./CoverUploader/CoverUploader";
import {
  AddBookForm,
  AddBookFormValues,
  initialAddBookFormValues
} from "./AddBookForm/AddBookForm";
import { Alert } from "antd";
import axios from "axios";
import { toData } from "@store/root/store.utils";
import { Subject } from "rxjs";
import { AddBookSuccess } from "./AddBookFeedback/AddBookSuccess/AddBookSuccess";
import { AddBookWarning } from "./AddBookFeedback/AddBookWarning/AddBookWarning";
import * as moment from "moment";

interface State {
  bookId: string;
  isLoading: boolean;
  hasFormError: boolean;
  wereThereUploadingErrors: boolean;
  isRetryingCoverUpload: boolean;
  isShowingWarningFeedback: boolean;
  addBookSuccessFeedbackTrigger: boolean;
}

const initialState: State = {
  bookId: "",
  isLoading: false,
  hasFormError: false,
  wereThereUploadingErrors: false,
  isRetryingCoverUpload: false,
  isShowingWarningFeedback: false,
  addBookSuccessFeedbackTrigger: false
};

class AddBook extends React.Component<{}, State> {
  public state = initialState;

  private onCoversFinishedUploadingSubject = new Subject<boolean>();
  private onStartUploadingCoverSubject: Subject<string> = new Subject();
  private onResetStateSubject: Subject<void> = new Subject();

  private resetFormFunc: any = null;

  public componentDidMount() {
    this.onCoversFinishedUploadingSubject.subscribe(
      (wereThereUploadingErrors: boolean) => {
        this.setState(
          {
            isLoading: false,
            wereThereUploadingErrors,
            isShowingWarningFeedback: wereThereUploadingErrors
          },
          () =>
            !wereThereUploadingErrors ? this.handleUploadSuccessful() : null
        );
      }
    );
  }

  public componentWillUnmount() {
    this.onCoversFinishedUploadingSubject.unsubscribe();
  }

  public render() {
    return (
      <div className="AddBookWrapper">
        <div className="AddBookWrapperInner">
          <div>
            <CoverUploader
              onSkipRetryUploadClickHandler={this.onSkipRetryUploadClickHandler}
              onRetryUploadClickHandler={this.onRetryUploadClickHandler}
              isRetryingUpload={this.state.isRetryingCoverUpload}
              isSubmittingForm={this.state.isLoading}
              onResetStateSubject={this.onResetStateSubject}
              onFinishedUploadingSubject={this.onCoversFinishedUploadingSubject}
              onStartUploadingCoversSubject={this.onStartUploadingCoverSubject}
            />
          </div>
          <AddBookForm
            isVisible={!this.state.isRetryingCoverUpload}
            isLoading={this.state.isLoading}
            submit={this.onSubmitHandler}
          />
          {this.state.hasFormError && !this.state.isRetryingCoverUpload ? (
            <Alert
              showIcon={true}
              message={"Something went wrong"}
              type="error"
            />
          ) : null}
        </div>
        <AddBookSuccess
          visibilityChange={this.state.addBookSuccessFeedbackTrigger}
        />
        <AddBookWarning
          isShowingWarningFeedback={this.state.isShowingWarningFeedback}
          tryUploadingAgainHandler={this.onTryUploadingAgainClickHandler}
          addNewBookAfterWarningHandler={
            this.onAddNewBookAfterWarningClickHandler
          }
        />
      </div>
    );
  }

  private onTryUploadingAgainClickHandler = () => {
    this.setState({
      isShowingWarningFeedback: false,
      isRetryingCoverUpload: true
    });
  };

  private onRetryUploadClickHandler = () => {
    this.setState(
      {
        isLoading: true
      },
      this.triggerCoverUploading
    );
  };

  private onSkipRetryUploadClickHandler = () => {
    this.resetState();
  };

  private onAddNewBookAfterWarningClickHandler = () => {
    this.resetState();
  };

  private onSubmitHandler = (
    values: AddBookFormValues,
    resetForm: (formValues: any) => void
  ) => {
    this.resetFormFunc = resetForm;
    const transformedValues: AddBookFormValues = {
      ...values,
      authors: values.authors.map(author => ({ ...author, uuid: "1" })),
      publishedDate: +new Date(
        moment(values.publishedDate).format("YYYY-MM-DD")
      )
    };
    this.setState(
      {
        ...this.state,
        isLoading: true,
        hasFormError: false
      },
      () =>
        this.addBookToDb(transformedValues)
          .then(this.triggerCoverUploading)
          .catch(this.handleFormError)
    );
  };

  private handleUploadSuccessful = () => {
    this.resetState();
    this.triggerBookSuccessFeedback();
  };

  private addBookToDb = (values: AddBookFormValues) => {
    return axios.post("book", values).then(toData);
  };

  private triggerCoverUploading = (
    {
      id
    }: {
      id: string;
    } = { id: this.state.bookId }
  ) => {
    this.setState(
      {
        bookId: id,
        isLoading: true
      },
      () => this.onStartUploadingCoverSubject.next(id)
    );
  };

  private resetState() {
    this.onResetStateSubject.next();
    this.setState(initialState);
    !this.state.isRetryingCoverUpload &&
      this.resetFormFunc(initialAddBookFormValues);
  }

  private handleFormError = () => {
    this.setState({
      isLoading: false,
      hasFormError: true
    });
  };

  private triggerBookSuccessFeedback() {
    this.setState(({ addBookSuccessFeedbackTrigger }) => ({
      addBookSuccessFeedbackTrigger: !addBookSuccessFeedbackTrigger
    }));
  }
}

export default AddBook;
