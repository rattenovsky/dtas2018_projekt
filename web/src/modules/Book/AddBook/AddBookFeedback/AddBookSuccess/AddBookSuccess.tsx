import * as React from "react";
import "./AddBookSuccess.css";
import "../AddBookFeedback.css";

import posed, { PoseGroup } from "react-pose";

const PosedContainer = posed.div({
  enter: {
    opacity: 1,
    position: "fixed !important"
  },
  exit: {
    position: "fixed !important",
    opacity: 0
  }
});

interface Props {
  visibilityChange: boolean;
}

export const AddBookSuccess: React.SFC<Props> = ({ visibilityChange }) => {
  const [isVisible, setIsVisible] = React.useState(false);
  const isOnMount = React.useRef(true);
  React.useEffect(
    () => {
      if (isOnMount.current) {
        isOnMount.current = false;
        return null;
      }
      setIsVisible(true);
      const timeout = setTimeout(() => {
        setIsVisible(false);
      }, 1500);
      return () => clearTimeout(timeout);
    },
    [visibilityChange]
  );

  return (
    <PoseGroup flipMove={false}>
      {isVisible ? (
        <PosedContainer
          key={1}
          pose={isVisible ? "visible" : "hidden"}
          className="AddBookFeedbackWrapper FlexFullCentered"
        >
          <svg
            className="checkmark"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 96 96"
          >
            <circle
              className="checkmark__circle"
              cx="48"
              cy="48"
              r="48"
              fill="none"
            />
            <path
              className="checkmark__check"
              fill="none"
              d="M14.1 27.2l7.1 7.2 16.7-16.8"
            />
          </svg>
          <h1 style={{ color: "white", margin: 0, paddingTop: 48 }}>
            Book Added!
          </h1>
        </PosedContainer>
      ) : null}
    </PoseGroup>
  );
};
