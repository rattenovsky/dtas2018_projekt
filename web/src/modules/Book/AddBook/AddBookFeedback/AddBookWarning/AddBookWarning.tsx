import * as React from "react";
import "../AddBookFeedback.css";
import "@shared/Css/Utils.css";
import { Button, Icon } from "antd";
import { Transition, animated } from "react-spring";

interface Props {
  tryUploadingAgainHandler: () => void;
  addNewBookAfterWarningHandler: () => void;
  isShowingWarningFeedback: boolean;
}

export const AddBookWarning: React.SFC<Props> = ({
  tryUploadingAgainHandler,
  addNewBookAfterWarningHandler,
  isShowingWarningFeedback
}) => (
  <Transition
    items={isShowingWarningFeedback}
    from={{ opacity: 0 }}
    enter={{ opacity: 1 }}
    leave={{ opacity: 0 }}
  >
    {(predicate: boolean) =>
      predicate
        ? (styles: React.CSSProperties) => (
            <animated.div
              className="AddBookFeedbackWrapper FlexFullCentered"
              style={styles}
            >
              <Icon
                type="info-circle"
                theme="outlined"
                style={{ fontSize: 120, color: "#fafafa", paddingBottom: 48 }}
              />
              <h1 style={{ color: "white", marginBottom: 24 }}>
                Could not upload all of the images
              </h1>
              <div>
                <Button
                  type="primary"
                  size="large"
                  onClick={tryUploadingAgainHandler}
                  style={{ marginRight: 24 }}
                >
                  Try Uploading Again
                </Button>
                <Button size="large" onClick={addNewBookAfterWarningHandler}>
                  Add New Book
                </Button>
              </div>
            </animated.div>
          )
        : null
    }
  </Transition>
);
