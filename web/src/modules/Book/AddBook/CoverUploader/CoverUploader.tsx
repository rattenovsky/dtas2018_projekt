import * as React from "react";
import "./CoverUploader.css";
import * as uuid from "uuid";
import { Transition, animated } from "react-spring";
import { CoverUploaderThumbCard } from "./CoverUploaderThumbCard/CoverUploaderThumbCard";
import { CoverUploaderForm } from "./CoverUploaderForm/CoverUploaderForm";
import { Subject } from "rxjs";
import { DefaultThumbCard } from "./CoverUploaderThumbCard/DefaultThumbCard";
import { tap, switchMap, bufferCount, filter } from "rxjs/operators";
import { Divider } from "antd";
import { CoverUploaderRetryButtons } from "./CoverUploaderRetryButtons/CoverUploaderRetryButtons";

interface State {
  coverUploaderFileList: CoverUploaderFile[];
  isUploading: boolean;
}

export interface CoverUploaderFile {
  file: File;
  thumb: string;
  uuid: string;
}

interface Props {
  isSubmittingForm: boolean;
  onStartUploadingCoversSubject: Subject<string>;
  onFinishedUploadingSubject: Subject<boolean>;
  onResetStateSubject: Subject<void>;
  isRetryingUpload: boolean;
  onRetryUploadClickHandler: () => void;
  onSkipRetryUploadClickHandler: () => void;
}

export class CoverUploader extends React.PureComponent<Props, State> {
  public state: State = {
    coverUploaderFileList: [],
    isUploading: false
  };

  private onThumbCompletedUploadingSubject: Subject<boolean> = new Subject();

  public componentWillUnmount() {
    this.props.onStartUploadingCoversSubject.unsubscribe();
    this.props.onResetStateSubject.unsubscribe();
  }

  public componentDidMount() {
    this.props.onResetStateSubject.subscribe(() => {
      this.setState({ coverUploaderFileList: [], isUploading: false });
    });

    this.props.onStartUploadingCoversSubject
      .pipe(
        tap(() => {
          if (!this.hasImages()) {
            this.props.onFinishedUploadingSubject.next(false);
          }
        }),
        filter(() => this.hasImages()),
        tap(() => this.setState({ isUploading: true })),
        switchMap(() =>
          this.onThumbCompletedUploadingSubject.pipe(
            bufferCount(this.state.coverUploaderFileList.length)
          )
        )
      )
      .subscribe((wasLastUploadSuccessful: boolean[]) => {
        const wereThereErrors =
          wasLastUploadSuccessful.filter(this.wereThereErrorsFilter).length > 0;
        this.props.onFinishedUploadingSubject.next(wereThereErrors);
      });
  }

  public render() {
    return (
      <div className="CoverUploaderWrapper">
        <h1>Add Covers</h1>
        <Divider style={{ marginTop: 0, marginBottom: 18 }} />
        {this.renderCards()}
        {!this.props.isSubmittingForm ? (
          <CoverUploaderForm onChangeHandler={this.onChangeHandler} />
        ) : null}
        {this.props.isSubmittingForm && !this.hasImages() ? (
          <DefaultThumbCard />
        ) : null}
        {this.props.isRetryingUpload ? (
          <CoverUploaderRetryButtons
            onRetryUploadClickHandler={this.props.onRetryUploadClickHandler}
            onSkipRetryUploadClickHandler={
              this.props.onSkipRetryUploadClickHandler
            }
            isSubmittingCovers={this.props.isSubmittingForm}
          />
        ) : null}
      </div>
    );
  }

  private wereThereErrorsFilter = (predicate: boolean) => {
    return !predicate;
  };

  private hasImages() {
    return this.state.coverUploaderFileList.length > 0;
  }

  private onChangeHandler = (files: File[]) => {
    const coverUploaderFiles: Array<Promise<any>> = files.map(
      this.mapFileToCoverUploaderFilePromise
    );
    Promise.all(coverUploaderFiles).then((res: CoverUploaderFile[]) => {
      this.setState({
        coverUploaderFileList: [...this.state.coverUploaderFileList, ...res]
      });
    });
  };

  private keyMapper = (item: CoverUploaderFile) => item.uuid;

  private renderCards = () => (
    <Transition
      native={true}
      from={{ width: 0, height: 0, opacity: 0 }}
      keys={this.keyMapper}
      trail={30}
      config={{ duration: 200 }}
      enter={{ width: 134, height: 134, opacity: 1 }}
      leave={{ width: 0, height: 0, opacity: 0 }}
      items={this.state.coverUploaderFileList}
    >
      {(coverUploaderFile: CoverUploaderFile) => (
        interpolatedStyles: React.CSSProperties
      ) => (
        <animated.div
          key={coverUploaderFile.uuid}
          style={{
            ...interpolatedStyles,
            display: "inline-flex",
            overflow: "hidden"
          }}
        >
          <CoverUploaderThumbCard
            isSubmittingForm={this.props.isSubmittingForm}
            onDeleteHandler={this.onDeleteHandler}
            coverUploaderFile={coverUploaderFile}
            onCompletedUploadSubject={this.onThumbCompletedUploadingSubject}
            onStartUploadingCoverSubject={
              this.props.onStartUploadingCoversSubject
            }
          />
        </animated.div>
      )}
    </Transition>
  );

  private mapFileToCoverUploaderFilePromise = (
    file: File
  ): Promise<CoverUploaderFile> => {
    const fileReader = new FileReader();
    return new Promise(resolve => {
      fileReader.readAsDataURL(file as File);
      fileReader.onloadend = (res: any) => {
        resolve({
          file,
          thumb: res.srcElement.result,
          uuid: uuid()
        });
      };
    });
  };

  private onDeleteHandler = (file: CoverUploaderFile) => {
    const idx: number = this.state.coverUploaderFileList.indexOf(file);
    this.setState({
      coverUploaderFileList: [
        ...this.state.coverUploaderFileList.slice(0, idx),
        ...this.state.coverUploaderFileList.slice(idx + 1)
      ]
    });
  };
}
