import * as React from "react";
import "@shared/Css/Utils.css";
import "../CoverUploader.css";
import "./CoverUploaderForm.css";
import { Icon, message } from "antd";

interface Props {
  onChangeHandler: (files: File[]) => void;
  style?: React.CSSProperties;
}

// wait for React.memo types to be added, change here!!!!!
export const CoverUploaderForm: React.SFC<Props> = ({
  onChangeHandler,
  style
}) => {
  let inputRef: HTMLInputElement = null;
  const validFileTypes = ["jpg", "jpeg", "png"];
  const setRef: (ref: HTMLInputElement) => void = ref => (inputRef = ref);
  const onChangeLocalHandler = () => {
    const fs: File[] = Array.from(inputRef.files);
    const validatedFiles = validateFiles(fs);
    if (validatedFiles.length < fs.length) {
      message.error("Some of the files have invalid format or are too big!");
    }
    if (validatedFiles.length > 0) {
      onChangeHandler(validatedFiles)
    }
  };
  return (
    <form style={style} className="CoverUploaderForm">
      <label
        htmlFor="uploadInput"
        className="CoverUploaderCard CoverUploaderLabel FlexFullCentered"
      >
        <Icon type="plus" style={{ fontSize: 32, color: "#999" }} />
        <div className="CoverUploaderLabelText">Add Image</div>
      </label>
      <input
        className="CoverUploaderInput"
        type="file"
        id="uploadInput"
        multiple={true}
        ref={setRef}
        onChange={onChangeLocalHandler}
      />
    </form>
  );

  function validateFiles(files: File[]): File[] {
    return files.filter(file => {
      return (
        file.size < 2000000 && validFileTypes.includes(file.type.split("/")[1])
      );
    });
  }
};
