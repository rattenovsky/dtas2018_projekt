import * as React from "react";
import "./CoverUploaderRetryButtons.css";
import { Button } from "antd";

interface Props {
  onRetryUploadClickHandler: () => void;
  onSkipRetryUploadClickHandler: () => void;
  isSubmittingCovers: boolean;
}

export const CoverUploaderRetryButtons: React.SFC<Props> = ({
  onRetryUploadClickHandler,
  onSkipRetryUploadClickHandler,
  isSubmittingCovers
}) => (
  <div className="CoverUploadRetryButtons">
    <Button
      size="large"
      onClick={onRetryUploadClickHandler}
      loading={isSubmittingCovers}
      className="RetryButton"
      type="primary"
    >
      Try Uploading Again
    </Button>
    <Button
      size="large"
      onClick={onSkipRetryUploadClickHandler}
      loading={isSubmittingCovers}
      className="RetryButton"
    >
      Add New Book
    </Button>
  </div>
);
