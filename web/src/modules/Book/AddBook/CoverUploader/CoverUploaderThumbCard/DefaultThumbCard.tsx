import * as React from "react";
import "./CoverUploaderThumbCard";
import "../CoverUploader.css";
import "@shared/Css/Utils.css";
import { Icon } from "antd";

export const DefaultThumbCard: React.SFC = () => (
  <div
    className="CoverUploaderCard FlexFullCentered"
    style={{ flexDirection: "column" }}
  >
    <Icon type="file-unknown" style={{ fontSize: 32, color: "#999" }} />
    <div className="CoverUploaderLabelText">Default Image</div>
  </div>
);
