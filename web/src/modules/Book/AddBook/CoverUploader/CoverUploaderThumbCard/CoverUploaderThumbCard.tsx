import * as React from "react";

import { ImageZoomer } from "@shared/ImageZoomer/ImageZoomer";
import { Icon, Progress, message } from "antd";
import axios from "axios";
import { ProgressProps } from "antd/lib/progress";
import { Transition, animated, config } from "react-spring";
import "./CoverUploaderThumbCard.css";
import "../CoverUploader.css";
import { CoverUploaderFile } from "../CoverUploader";
import { Subject } from "rxjs";
import { takeUntil, filter, tap } from "rxjs/operators";

interface Props {
  onDeleteHandler: (file: CoverUploaderFile) => void;
  coverUploaderFile: CoverUploaderFile;
  onStartUploadingCoverSubject: Subject<string>;
  onCompletedUploadSubject: Subject<boolean>;
  isSubmittingForm: boolean;
}

interface State {
  uploadProgress: number;
  uploadStatus: ProgressProps["status"];
  wasLastUploadSuccessful: boolean;
}

export class CoverUploaderThumbCard extends React.PureComponent<Props, State> {
  public state: State = {
    uploadProgress: 0,
    uploadStatus: "active",
    wasLastUploadSuccessful: false
  };

  private unsubscribeSubject: Subject<void> = new Subject();

  public componentWillUnmount() {
    this.unsubscribeSubject.next();
  }

  public componentDidMount() {
    this.props.onStartUploadingCoverSubject
      .pipe(
        takeUntil(this.unsubscribeSubject),
        tap(() => {
          if (this.state.wasLastUploadSuccessful) {
            this.reportUploadEnded();
          }
        }),
        filter(() => !this.state.wasLastUploadSuccessful),
        tap(() => this.setState({ uploadStatus: "active" }))
      )
      .subscribe(this.handleFileUpload);
  }

  public render() {
    return (
      <div
        className={`CoverUploaderCard CoverUploaderThumbCard  ${
          this.state.uploadStatus !== "active"
            ? "Upload-" + this.state.uploadStatus
            : ""
        }`}
      >
        <Transition
          config={config.gentle}
          native={true}
          items={
            this.props.isSubmittingForm && this.state.uploadStatus === "active"
          }
          from={{ opacity: 0 }}
          enter={{ opacity: 1 }}
          leave={{ opacity: 0 }}
        >
          {(shouldShowUploadingProgress: boolean) =>
            shouldShowUploadingProgress
              ? (styles: React.CSSProperties) => (
                  <animated.div
                    className="CoverUploaderCardMask CoverUploaderThumbCardUploadingMask"
                    style={styles}
                  >
                    <Progress
                      status={this.state.uploadStatus}
                      type="circle"
                      percent={this.state.uploadProgress}
                      width={64}
                    />
                  </animated.div>
                )
              : () => (
                  <NotUploadingMask
                    isSubmittingForm={this.props.isSubmittingForm}
                    uploadStatus={this.state.uploadStatus}
                    file={this.props.coverUploaderFile}
                    onDeleteLocalHandler={this.onDeleteLocalHandler}
                  />
                )
          }
        </Transition>
        <img src={this.props.coverUploaderFile.thumb} />
      </div>
    );
  }

  private onDeleteLocalHandler = () => {
    this.props.onDeleteHandler(this.props.coverUploaderFile);
  };

  private handleFileUpload = (bookId: string) => {
    axios
      .post("/cover", this.makeFormData(bookId), {
        onUploadProgress: this.handleUploadProgress
      })
      .then(() => this.handleUploadEnded("success"))
      .catch(() => {
        message.error("Could not upload the cover");
        this.handleUploadEnded("exception");
      });
  };

  private reportUploadEnded() {
    this.props.onCompletedUploadSubject.next(
      this.state.wasLastUploadSuccessful
    );
  }

  private handleUploadEnded(uploadStatus: State["uploadStatus"]) {
    this.setState(
      {
        uploadStatus,
        uploadProgress: 0,
        wasLastUploadSuccessful: uploadStatus === "success"
      },
      this.reportUploadEnded
    );
  }

  private makeFormData(bookId: string) {
    const formData = new FormData();
    formData.append("book", bookId);
    formData.append("cover", this.props.coverUploaderFile.file);
    return formData;
  }

  private handleUploadProgress = (evt: ProgressEvent) => {
    const uploadProgress = Math.round((evt.loaded * 100) / evt.total);
    this.setState({
      uploadProgress
    });
  };
}

const NotUploadingMask: React.SFC<{
  file: CoverUploaderFile;
  onDeleteLocalHandler: () => void;
  uploadStatus: ProgressProps["status"] | null;
  isSubmittingForm: boolean;
}> = ({ file, onDeleteLocalHandler, uploadStatus, isSubmittingForm }) => (
  <div className="CoverUploaderCardMask CoverUploaderThumbCardHoverMask">
    <ImageZoomer imageSrc={file.thumb}>
      <Icon
        type="eye"
        theme="outlined"
        style={{ fontSize: 24, color: "white", paddingRight: 5 }}
      />
    </ImageZoomer>
    {!isSubmittingForm && uploadStatus !== "success" ? (
      <Icon
        type="delete"
        onClick={onDeleteLocalHandler}
        theme="outlined"
        style={{ fontSize: 24, color: "white", paddingLeft: 5 }}
      />
    ) : null}
  </div>
);
