import * as React from "react";
import { Author } from "./AddBookForm";
import { Field } from "formik";
import { InputField } from "@shared/InputField/InputField";
import { Icon } from "antd";

interface Props {
  author: Author;
  isDisabled: boolean;
  index: number;
}

export const AddBookAuthorFields: React.SFC<Props> = ({
  author,
  isDisabled,
  index
}) => (
  <div className="FieldArrayInputsWrapper">
    <div className="FieldArrayInputWrapper">
      <Field
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name={`authors.${index}.firstName`}
        type="text"
        placeholder="First name"
        size="large"
        component={InputField}
        isInsideFieldArray={true}
      />
    </div>
    <div className="FieldArrayInputWrapper">
      <Field
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        name={`authors.${index}.lastName`}
        type="text"
        placeholder="Last name"
        size="large"
        component={InputField}
        isInsideFieldArray={true}
        isDisabled={isDisabled}
      />
    </div>
  </div>
);
