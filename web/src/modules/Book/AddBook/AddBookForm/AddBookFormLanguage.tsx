import * as React from "react";
import { Select } from "antd";
import { FieldProps, FormikBag, FormikActions } from "formik";
import { AntDesignInputTypes } from "@shared/InputField/InputField";
import FormItem from "antd/lib/form/FormItem";
import { AddBookFormValues } from "./AddBookForm";
const Option = Select.Option;
interface Props {
  isDisabled: boolean;
  showSpinner: boolean;
  setFieldValue: FormikActions<AddBookFormValues>["setFieldValue"];
  setFieldTouched: FormikActions<AddBookFormValues>["setFieldTouched"];
}

type InputFieldFormikTypes = FieldProps<FormikBag<any, any>>;

export const languages = ["Polish", "English", "German"];
const languageOptions = languages.map((language: string, index: number) => (
  <Option key={index} value={language}>
    {language}
  </Option>
));

export const AddBookFormLanguage: React.SFC<
  InputFieldFormikTypes & Partial<AntDesignInputTypes & Props>
> = ({
  field: { value, name },
  form: { touched, errors },
  isDisabled,
  setFieldValue,
  showSpinner,
  setFieldTouched
}) => {
  const errorMsg = touched[name] && errors[name];
  return (
    <FormItem
      hasFeedback={true}
      help={errorMsg}
      validateStatus={
        errorMsg ? "error" : showSpinner ? "validating" : undefined
      }
    >
      <Select
        onDropdownVisibleChange={handleDropdownVisibilityChange}
        disabled={isDisabled}
        size="large"
        onBlur={handleBlur}
        value={value !== "" ? value : undefined}
        filterOption={true}
        showSearch={true}
        placeholder="Select the language"
        onSelect={handleOnSelect}
      >
        {languageOptions}
      </Select>
    </FormItem>
  );
  function handleOnSelect(val: any, option: any) {
    setFieldValue("language", option.props.children);
  }

  function handleDropdownVisibilityChange(isOpen: boolean) {
    setTimeout(
      () =>
        isOpen === false && value === ""
          ? setFieldTouched("language", true)
          : undefined,
      0
    );
  }

  function handleBlur() {
    setFieldTouched("language", true);
  }
};
