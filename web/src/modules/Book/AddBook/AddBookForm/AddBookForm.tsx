import * as React from "react";
import {
  Form as FormikForm,
  InjectedFormikProps,
  withFormik,
  FastField,
  FieldArray
} from "formik";
import { Icon, Button, Divider } from "antd";
import { InputField } from "@shared/InputField/InputField";
import FormItem from "antd/lib/form/FormItem";
import { addBookSchema } from "@yupSchemas/add.book.schema";
import posed from "react-pose";
import { AddBookFormLanguage } from "./AddBookFormLanguage";
import * as uuid from "uuid";
import { AddBookAuthorFields } from "./AddBookAuthorFields";

const PosedFormContainer = posed.div({
  hidden: { height: 0, opacity: 0 },
  visible: { height: "auto", opacity: 1 }
});

export const initialAddBookFormValues: AddBookFormValues = {
  title: "",
  isbn: undefined,
  language: "",
  publisher: "",
  publishedDate: "",
  pageCount: undefined,
  price: undefined,
  description: "",
  authors: [{ firstName: "", lastName: "", uuid: uuid.v4() }]
};

export interface Author {
  firstName: string;
  lastName: string;
  uuid: string;
}

export interface AddBookFormValues {
  title: string;
  isbn?: string;
  language: string;
  publisher: string;
  publishedDate: string | number;
  pageCount: number;
  authors: Author[];
  price: number;
  description: string;
}

interface FormProps {
  submit: (
    values: AddBookFormValues,
    resetFormFunc: (values: AddBookFormValues) => void
  ) => void;
}

interface AddBookFormViewProps {
  isLoading: boolean;
  isVisible: boolean;
}

const C: React.SFC<
  InjectedFormikProps<FormProps & AddBookFormViewProps, AddBookFormValues>
> = ({ isLoading, isVisible, setFieldValue, setFieldTouched, values }) => {
  function handlePublishDateChange(date: any, dateString: string) {
    setFieldValue("publishedDate", dateString);
  }

  function handleDatePickerBlur() {
    setFieldTouched("publishedDate", true);
  }

  return (
    <PosedFormContainer
      pose={isVisible ? "visible" : "hidden"}
      style={{ overflow: "hidden" }}
    >
      <FormikForm noValidate={true}>
        <h1 style={{ marginTop: 18 }}>Fill in the form</h1>
        <Divider style={{ marginTop: 0 }} />
        <FastField
          prefix={<Icon type="tag" style={{ color: "rgba(0,0,0,.25)" }} />}
          name="title"
          type="text"
          placeholder="Book Title"
          size="large"
          isDisabled={isLoading}
          component={InputField}
        />
        <FastField
          prefix={<Icon type="link" style={{ color: "rgba(0,0,0,.25)" }} />}
          name="isbn"
          placeholder="Book isbn number"
          type="text"
          size="large"
          isDisabled={isLoading}
          component={InputField}
        />
        <FastField
          prefix={
            <Icon type="paper-clip" style={{ color: "rgba(0,0,0,.25)" }} />
          }
          name="pageCount"
          placeholder="Page count"
          type="number"
          size="large"
          isDisabled={isLoading}
          component={InputField}
        />
        <FastField
          prefix={<Icon type="dollar" style={{ color: "rgba(0,0,0,.25)" }} />}
          name="price"
          placeholder="Book price"
          type="number"
          size="large"
          isDisabled={isLoading}
          component={InputField}
        />
        <FastField
          name="description"
          placeholder="Book description"
          type="textarea"
          size="large"
          isDisabled={isLoading}
          component={InputField}
        />
        <FastField
          prefix={<Icon type="read" style={{ color: "rgba(0,0,0,.25)" }} />}
          name="publisher"
          placeholder="Book publisher"
          type="text"
          size="large"
          isDisabled={isLoading}
          component={InputField}
        />
        <FastField
          name="publishedDate"
          placeholder="Select publish date"
          type="datePicker"
          size="large"
          onDateChangeHandlerFunc={handlePublishDateChange}
          isDisabled={isLoading}
          component={InputField}
          onBlur={handleDatePickerBlur}
          style={{ width: "100%" }}
        />
        <FastField
          name="language"
          setFieldTouched={setFieldTouched}
          setFieldValue={setFieldValue}
          isDisabled={isLoading}
          component={AddBookFormLanguage}
        />
        <h1 style={{ marginTop: 18 }}>Add Authors</h1>
        <Divider style={{ marginTop: 0 }} />
        <FieldArray
          name="authors"
          // tslint:disable-next-line:jsx-no-lambda
          render={arrayHelpers => {
            function handleAuthorFieldsAdd() {
              arrayHelpers.push({
                firstName: "",
                lastName: "",
                uuid: uuid.v4()
              });
            }
            return (
              <React.Fragment>
                {values.authors.map((author: Author, index: number) => (
                  <div key={author.uuid}>
                    <div className="FieldArrayWrapper">
                      <AddBookAuthorFields
                        author={author}
                        index={index}
                        isDisabled={isLoading}
                      />
                      {values.authors.length > 1 ? (
                        <Icon
                          onClick={() => arrayHelpers.remove(index)}
                          className="dynamic-delete-button"
                          type="minus-circle-o"
                        />
                      ) : null}
                    </div>
                  </div>
                ))}
                {values.authors.length < 4 ? (
                  <FormItem>
                    <Button
                      disabled={isLoading}
                      onClick={handleAuthorFieldsAdd}
                      type="dashed"
                      block={true}
                      size="large"
                    >
                      <Icon type="plus" /> Add an author
                    </Button>
                  </FormItem>
                ) : null}
              </React.Fragment>
            );
          }}
        />
        <FormItem>
          <Button
            loading={isLoading}
            block={true}
            type="primary"
            htmlType="submit"
            size={"large"}
          >
            Add the book
          </Button>
        </FormItem>
      </FormikForm>
    </PosedFormContainer>
  );
};

export const AddBookForm = withFormik<
  FormProps & AddBookFormViewProps,
  AddBookFormValues
>({
  validationSchema: addBookSchema,
  mapPropsToValues: () => initialAddBookFormValues,
  handleSubmit: (values: AddBookFormValues, { props, resetForm }) => {
    props.submit(values, resetForm);
  }
})(C);
