import * as React from "react";
import { Tabs, Divider } from "antd";
const TabPane = Tabs.TabPane;
import "./BookDetailTabs.css";
import { Book } from "@book/store/books.entity.reducer";
import * as moment from "moment";

export const BookDetailTabs: React.SFC<Book> = book => {
  return (
    <div className="BookDetailTabsContainer">
      <Tabs type="card">
        <TabPane tab="Detailed Informations" key="2">
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Title</span>
            <Divider type="vertical" />
            <span className="second">{book.title}</span>
          </div>
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Authors</span>
            <Divider type="vertical" />
            <span className="second">
              {book.authors
                .map(author => `${author.firstName} ${author.lastName}`)
                .join(", ")}
            </span>
          </div>
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Language</span>
            <Divider type="vertical" />
            <span className="second">{book.language}</span>
          </div>
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Number of Pages</span>
            <Divider type="vertical" />
            <span className="second">{book.pageCount}</span>
          </div>
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Publish date</span>
            <Divider type="vertical" />
            <span className="second">
              {moment(book.publishedDate).format("YYYY-MM-DD")}
            </span>
          </div>
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Price</span>
            <Divider type="vertical" />
            <span className="second">{book.price} zl</span>
          </div>
          <div className="AdditionalDescriptionItem">
            <span className="first SmallParagraph">Isbn Number</span>
            <Divider type="vertical" />
            <span className="second">{book.isbn}</span>
          </div>
        </TabPane>
      </Tabs>
    </div>
  );
};
