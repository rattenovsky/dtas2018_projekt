import * as React from "react";
import "./BookDetailCoverCarousel.css";
import Slider from "react-slick";
import { ImageZoomer } from "@shared/ImageZoomer/ImageZoomer";

interface Props {
  covers: string[];
}

const settings = {
  dots: true,
  fade: false,
  infinite: false,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 680,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        speed: 500,
        infinite: false
      }
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,

        speed: 500
      }
    },
    {
      breakpoint: 320,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500
      }
    }
  ]
};

export const BookDetailCoverCarousel: React.SFC<Props> = ({ covers }) => {
  return (
    <div className="BookDetailCoverCarouselWrapper">
      {covers.length > 1 ? (
        <ImageSlider covers={covers} />
      ) : (
        <ImageZoomer imageSrc={covers[0]}>
          <img src={covers[0]} />
        </ImageZoomer>
      )}
    </div>
  );
};

const ImageSlider: React.SFC<{ covers: string[] }> = ({ covers }) => {
  const slides = covers.map((cover, idx) => (
    <div key={idx}>
      <img src={cover} />
    </div>
  ));
  return <Slider {...settings}>{slides}</Slider>;
};
