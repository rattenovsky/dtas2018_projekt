import * as React from "react";
import "./BookDetailBasicInfo.css";
import { Divider } from "antd";

interface Props {
  description: string;
  authors: string;
  title: string;
}

export const BookDetailBasicInfo: React.SFC<Props> = ({
  authors,
  description,
  title
}) => {
  return (
    <div className="BookDetailBasicInfoWrapper">
      <div>
        <h1 style={{ fontWeight: 700, lineHeight: 1 }}>{title}</h1>
        <div className="BookAuthorsWrapper">
          <span className="SmallParagraph">Authors: </span>
          <span
            className="BookAuthors"
            style={{ fontSize: 16, fontWeight: 600 }}
          >
            {authors}
          </span>
        </div>
        <Divider />
      </div>
      <div style={{ wordBreak: "break-all" }}>
        <h2 className="BookDescriptionHeading">Description</h2>
        {description}
      </div>
    </div>
  );
};
