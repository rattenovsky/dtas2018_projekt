import * as React from "react";
import "./BookDetailBuyPanel.css";
import { Button, Divider, Icon } from "antd";

interface Props {
  bookPrice: number;
  addToCartHandler: (silent: boolean) => void;
  isAuthenticated: boolean;
  isReachingToAnApi: boolean;
}

export const BookDetailBuyPanel: React.SFC<Props> = ({
  bookPrice,
  addToCartHandler,
  isAuthenticated,
  isReachingToAnApi
}) => {
  return (
    <div className="BookDetailBuyPanelWrapper">
      <div className="BookDetailPrice">
        <p className="SmallParagraph">On emeison.com</p>
        <p className="BookPrice">{bookPrice} zl</p>
      </div>
      <div
        className="BookDetailBuyPanelInnerBlock BookDetailBookAvailable"
        style={{ marginBottom: 12, marginTop: 12 }}
      >
        Book is <p>Available</p>
      </div>
      <Button
        disabled={!isAuthenticated}
        className="BookDetailBookButton"
        size="large"
        ghost={true}
        loading={isReachingToAnApi}
        type="primary"
        onClick={isAuthenticated ? () => addToCartHandler(true) : null}
      >
        <div className="FlexFullCentered">
          <Icon type="caret-right" style={{ fontSize: 18 }} />
          <Divider
            type="vertical"
            style={{
              height: 18,
              background: isAuthenticated ? "#1890ff" : "#e8e8e8"
            }}
          />
        </div>
        Buy Now
        <Icon type="double-right" theme="outlined" style={{ fontSize: 18 }} />
      </Button>
      <Button
        disabled={!isAuthenticated}
        className="BookDetailBookButton"
        size="large"
        ghost={false}
        loading={isReachingToAnApi}
        type="primary"
        onClick={isAuthenticated ? () => addToCartHandler(false) : null}
      >
        <div className="FlexFullCentered">
          <Icon type="shopping-cart" style={{ fontSize: 18 }} />
          <Divider type="vertical" style={{ height: 18 }} />
        </div>
        Add To Cart
        <Icon type="double-right" theme="outlined" style={{ fontSize: 18 }} />
      </Button>
      <div className="BookDetailBuyPanelInnerBlock" style={{ marginTop: 24 }}>
        Fast and safe delivery
      </div>
    </div>
  );
};
