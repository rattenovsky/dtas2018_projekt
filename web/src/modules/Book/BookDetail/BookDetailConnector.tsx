import * as React from "react";
import { BookDetailView } from "./BookDetailView";
import { RouteComponentProps, withRouter } from "react-router";
import {
  BooksEntityState,
  getHydratedBook,
  Book
} from "@book/store/books.entity.reducer";
import { ApplicationState } from "@store/root/root.reducer";
import { connect } from "react-redux";
import axios from "axios";
import { toData } from "@store/root/store.utils";
import { Dispatch, compose } from "redux";
import {
  BooksEntityActions,
  AddBooksToStore
} from "@book/store/books.entity.action";
import {
  AddBookToShoppingCart,
  ShoppingCartActions
} from "@shoppingCart/store/shopping.cart.actions";

interface BookDetailState {
  isLoading: boolean;
  book: Book;
  hasBook: boolean;
}

interface StateProps {
  booksEntities: BooksEntityState;
  isAuthenticated: boolean;
  isReachingToAnApi: boolean;
}

interface DispatchProps {
  addBooksToStore: (book: Book[]) => void;
  addBookToShoppingCart: (payload: { bookId: string; silent: boolean }) => void;
}

export class C extends React.PureComponent<
  RouteComponentProps & StateProps & DispatchProps,
  BookDetailState
> {
  public state: BookDetailState = {
    isLoading: true,
    book: null,
    hasBook: false
  };

  private bookId = (this.props.match.params as { id: string }).id;

  public componentDidMount() {
    const hydratedBook = getHydratedBook(this.props.booksEntities, this.bookId);
    hydratedBook
      ? this.handleBookFetched(hydratedBook, true)
      : this.handleBookFetching();
  }

  public render() {
    return (
      <BookDetailView
        isReachingToAnApi={this.props.isReachingToAnApi}
        isAuthenticated={this.props.isAuthenticated}
        addToCartHandler={this.addToCartHandler}
        book={this.state.book}
        isLoading={this.state.isLoading}
        isBookNotFound={!this.state.isLoading && !this.state.hasBook}
      />
    );
  }

  private handleBookFetching() {
    axios
      .get(`/book/${this.bookId}`)
      .then(toData)
      .then(
        book =>
          Object.keys(book).length > 0
            ? this.handleBookFetched(book)
            : this.handleBookFetchNoResults()
      );
  }

  private addToCartHandler = (silent: boolean = false) => {
    this.props.addBookToShoppingCart({ bookId: this.state.book.id, silent });
  };

  private handleBookFetchNoResults() {
    this.setState({
      isLoading: false,
      hasBook: false
    });
  }

  private handleBookFetched(book: Book, isHydrated: boolean = false) {
    this.setState(
      {
        isLoading: false,
        hasBook: true,
        book
      },
      () => (!isHydrated ? this.props.addBooksToStore([book]) : null)
    );
  }
}

const mapStateToProps = ({
  booksEntities,
  auth,
  shoppingCart
}: ApplicationState): StateProps => ({
  booksEntities,
  isAuthenticated: auth.isAuthenticated,
  isReachingToAnApi: shoppingCart.isReachingToAnApi
});

const mapDispatchToProps = (
  dispatch: Dispatch<BooksEntityActions | ShoppingCartActions>
): DispatchProps => ({
  addBooksToStore: (book: Book[]) => dispatch(AddBooksToStore(book)),
  addBookToShoppingCart: (payload: { bookId: string; silent: boolean }) =>
    dispatch(AddBookToShoppingCart(payload))
});

export const BookDetailConnector = compose(
  withRouter,
  connect<StateProps, DispatchProps, {}>(
    mapStateToProps,
    mapDispatchToProps
  )
)(C);
