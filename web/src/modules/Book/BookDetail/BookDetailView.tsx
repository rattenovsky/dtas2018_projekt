import * as React from "react";
import "./BookDetail.css";

import { BookDetailBuyPanel } from "./BookDetailBuyPanel/BookDetailBuyPanel";
import { BookDetailBasicInfo } from "./BookDetailBasicInfo/BookDetailBasicInfo";
import { BookDetailTabs } from "./BookDetailTabs/BookDetailTabs";
import { BookDetailCoverCarousel } from "./BookDetailCoverCarousel/BookDetailCoverCarousel";
import { Book } from "@book/store/books.entity.reducer";
import { defaultBookCover } from "@book/BookCard/BookCardView";
import { DefaultLoader } from "@utils/Loaders/DefaultLoader/DefaultLoader";
import { ResourceNotFound } from "@shared/ResourceNotFound/ResourceNotFound";
import { pageNotFoundDefaultActions } from "src/routes/Routes";

interface Props {
  book: Book;
  isLoading: boolean;
  isBookNotFound: boolean;
  addToCartHandler: VoidFunction;
  isAuthenticated: boolean;
  isReachingToAnApi: boolean;
}

export const BookDetailView: React.SFC<Props> = ({
  book,
  isLoading,
  isBookNotFound,
  addToCartHandler,
  isAuthenticated,
  isReachingToAnApi
}) => {
  return (
    <div className="BookDetailWrapper">
      {isLoading
        ? renderLoading()
        : isBookNotFound
          ? renderNotFound()
          : renderContent()}
    </div>
  );

  function renderNotFound() {
    return (
      <ResourceNotFound
        actions={pageNotFoundDefaultActions}
        title="Book not found!"
        style={{ height: "100%", padding: 0, flex: 1 }}
      />
    );
  }

  function renderLoading() {
    return (
      <React.Fragment>
        <div className="BookDetailBasicInfoWrapper">
          <DefaultLoader />
        </div>

        <div className="BookDetailBasicInfoWrapper" style={{ marginTop: 20 }}>
          <DefaultLoader />
        </div>
      </React.Fragment>
    );
  }

  function renderContent() {
    return (
      <React.Fragment>
        <div className="BookDetailRow">
          <BookDetailCoverCarousel
            covers={
              book.covers.length > 0
                ? book.covers.map(cover => cover.normal)
                : [defaultBookCover.normal]
            }
          />
          <BookDetailBasicInfo
            title={book.title}
            description={book.description}
            authors={book.authors
              .map(author => author.firstName + " " + author.lastName)
              .join(",")}
          />
          <BookDetailBuyPanel
            isReachingToAnApi={isReachingToAnApi}
            isAuthenticated={isAuthenticated}
            addToCartHandler={addToCartHandler}
            bookPrice={book.price}
          />
        </div>
        <div className="BookDetailRow" style={{ width: "100%" }}>
          <BookDetailTabs {...book} />
        </div>
      </React.Fragment>
    );
  }
};
