import { ActionsObservable, StateObservable, ofType } from "redux-observable";
import {
  BooksEntityActions,
  BooksEntityActionsEnum,
  AddBooksToStore
} from "./books.entity.action";
import { ApplicationState } from "@store/root/root.reducer";
import { Observable, from } from "rxjs";
import { toPayload, toData } from "@store/root/store.utils";
import { map, switchMap, filter } from "rxjs/operators";

import axios from "axios";
import { Book } from "./books.entity.reducer";

type BooksEntityEpic = (
  action$: ActionsObservable<BooksEntityActions>,
  state$: StateObservable<ApplicationState>
) => Observable<BooksEntityActions>;

const fetchAndAddMissingBooksEpic: BooksEntityEpic = (action$, state$) =>
  action$.pipe(
    ofType(BooksEntityActionsEnum.fetchAndAddMissingBooks),
    // normally you would transduce here but im not sure if filter would behave the same way (i want to filter against the stream not array)
    map(toPayload),
    filter((bookIds: string[]) => bookIds.length > 0),
    map(toMissingBooksFetchingUrl),
    switchMap(
      (fetchingUrl: string) =>
        from(axios.get(`/book/multiple?${fetchingUrl}`)).pipe(
          map(toData),
          map((books: Book[]) => AddBooksToStore(books))
        )
      // TODO: handle error here
    )
  );

const toMissingBooksFetchingUrl = (bookIds: string[]): string =>
  bookIds.map(bookId => `id=${bookId}`).join("&");

export const BooksEntityEpics = [fetchAndAddMissingBooksEpic];
