import { ActionCreator, Action } from "redux";
import { Book } from "./books.entity.reducer";

export enum BooksEntityActionsEnum {
  addBooksToStore = "@@APP ADD_BOOKS_TO_STORE",
  fetchAndAddMissingBooks = "@@UserProfileConnector FETCH_AND_ADD_MISSING_BOOKS"
}

export interface AddBooksToStoreAction extends Action {
  type: BooksEntityActionsEnum.addBooksToStore;
  payload: Book[];
}

export interface FetchAndAddMissingBookAction extends Action {
  type: BooksEntityActionsEnum.fetchAndAddMissingBooks;
  payload: string[];
}

export const AddBooksToStore: ActionCreator<
  AddBooksToStoreAction
> = payload => ({
  type: BooksEntityActionsEnum.addBooksToStore,
  payload
});

export const FetchAndAddMissingBooks: ActionCreator<
  FetchAndAddMissingBookAction
> = (payload: string[]) => ({
  type: BooksEntityActionsEnum.fetchAndAddMissingBooks,
  payload
});

export type BooksEntityActions =
  | AddBooksToStoreAction
  | FetchAndAddMissingBookAction;
