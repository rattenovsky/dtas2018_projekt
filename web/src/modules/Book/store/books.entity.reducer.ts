import { schema, normalize, denormalize } from "normalizr";
import { Omit } from "@books/BooksFilter/store/books.filter.reducer";
import { BooksEntityActionsEnum } from "./books.entity.action";

const bookCoverSchema = new schema.Entity("covers");
const bookPublisherSchema = new schema.Entity("publisher");
const bookAuthorSchema = new schema.Entity("authors");

const bookSchema = new schema.Entity("book", {
  covers: [bookCoverSchema],
  authors: [bookAuthorSchema],
  publisher: bookPublisherSchema
});

const bookListSchema = new schema.Array(bookSchema);

interface TimeStamp {
  createdAt: number;
  updatedAt: number;
}

export interface RawMongoBook extends Omit<Book, "id"> {
  _id: string;
}

export interface BookAuthorNoRelations extends TimeStamp {
  uuid: string;
  id: string;
  firstName: string;
  lastName: string;
  nickname: string;
  fullName?: string;
}

export interface BookAuthor extends TimeStamp {
  uuid: string;
  id: string;
  firstName: string;
  lastName: string;
  nickname: string;
  books: BookNoRelations[];
}

export interface BookPublisher extends TimeStamp {
  id: string;
  name: string;
}

export interface BookCover extends TimeStamp {
  id: string;
  lazy: string;
  normal: string;
  book: string;
}

export interface BookNoRelations extends TimeStamp {
  id: string;
  isbn: string;
  title: string;
  publishedDate: number;
  publisher: string;
  pageCount: number;
  language: string;
  price: number;
  description: string;
}

export interface Book extends TimeStamp, BookNoRelations {
  authors: BookAuthor[];
  covers: BookCover[];
}

export interface NormalizedBook {
  authors: string[];
  covers: string[];
  id: string;
  isbn: string;
  title: string;
  description: string;
  publishedDate: number;
  pageCount: number;
  language: string;
  price: number;
}

export interface NormalizedBookEntity {
  book: {
    [key: string]: NormalizedBook;
  };
  covers: {
    [key: string]: BookCover;
  };
  publishers: {
    [key: string]: BookPublisher;
  };
  authors: {
    [key: string]: BookAuthor;
  };
}

export interface BooksEntityState {
  entities: NormalizedBookEntity;
  result: string[];
}

export const initialBooksEntityState: BooksEntityState = {
  entities: {
    book: {},
    authors: {},
    publishers: {},
    covers: {}
  },
  result: []
};

export function BooksEntityReducer(
  state: BooksEntityState = initialBooksEntityState,
  action: any
): BooksEntityState {
  switch (action.type) {
    case BooksEntityActionsEnum.addBooksToStore:
      const filteredBooks = action.payload.filter(
        (book: Book) => !state.result.includes(book.id)
      );

      const normalizedBooks = normalize(filteredBooks, bookListSchema);

      if (state.result.length === 0 && normalizedBooks.result.length > 0) {
        return {
          ...state,
          ...normalizedBooks
        };
      }
      if (filteredBooks.length === 0) {
        return state;
      } else {
        return {
          ...state,
          entities: {
            ...addToEntities(normalizedBooks.entities, state.entities)
          },
          result: [...state.result, ...normalizedBooks.result]
        };
      }

    default:
      return state;
  }
}

function addToEntities(
  normalizedData: NormalizedBookEntity,
  currentNormalizedState: NormalizedBookEntity
): NormalizedBookEntity {
  Object.keys(normalizedData).forEach(
    stateKey =>
      (currentNormalizedState[stateKey] = {
        ...currentNormalizedState[stateKey],
        ...normalizedData[stateKey]
      })
  );

  return currentNormalizedState;
}

export const getHydratedBook = (state: BooksEntityState, id: string): Book =>
  state.result.includes(id)
    ? denormalize(id, bookSchema, state.entities)
    : null;
