import * as React from "react";
import { Card } from "antd";
import "@book/BookCard/BookCard.css";
import { Link } from "react-router-dom";
import { LazyLoadedImage } from "@shared/LazyLoadedImage/LazyLoadedImage";
import { Book } from "@book/store/books.entity.reducer";
// import { store } from "../../../index";
const { Meta } = Card;

export const defaultBookCover = {
  normal: "https://via.placeholder.com/300x200",
  lazy: "https://via.placeholder.com/300x200",
  id: "1",
  book: " 1",
  createdAt: 1,
  updatedAt: 1
};

// Using Pure Component here for performance reasons, why-did-you-update was showing unnecessary re-renders
export class BookCardView extends React.PureComponent<Book, {}> {
  public render() {
    return (
      <Link to={`/book/${this.props.id}`}>
        <Card
          extra={this.props.title}
          hoverable={true}
          bordered={true}
          className="BookCard"
          cover={
            <LazyLoadedImage
              {...(this.props.covers.length > 0
                ? { ...this.props.covers[0] }
                : { ...defaultBookCover })}
            />
          }
        >
          <Meta
            title={
              <div>
                <div className="BookDetailPrice" style={{ marginBottom: 0 }}>
                  <p className="SmallParagraph">On emeison.com</p>
                  <p className="BookPrice">{this.props.price} zł</p>
                </div>
              </div>
            }
            description={this.renderCardDescription()}
          />
        </Card>
      </Link>
    );
  }

  private renderCardDescription = () => {
    const names = this.props.authors.map(
      author => `${author.firstName} ${author.lastName}`
    );
    return names.join(", ");
  };
}
