# React Book Library App

This project is the result of my efforts to learn the basics of React

# [Live version](https://emeison-web2.herokuapp.com/)

Please note that it might take some time for the app to load since heroku puts app to sleep.

1. Refreshed my knowledge about RxJs
2. Learned how to transduce
3. Implemented basic stuff like redux ( with normalizr ) etc ...
4. ServiceWorker with Workbox

#### Additional notes

1. I will not post backend here since it was **not** made fully by me ( i fiddled a lot with it )
