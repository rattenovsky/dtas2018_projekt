const WorkboxWebpackPlugin = require("workbox-webpack-plugin");

module.exports = {
  webpack: function(config, env) {
    config.plugins = config.plugins.map(plugin => {
      if (plugin.constructor.name === "SWPrecacheWebpackPlugin") {
        return new WorkboxWebpackPlugin.InjectManifest({
          swSrc: "./src/sw.js",
          swDest: "sw.js"
        });
      }
      return plugin;
    });
    return config;
  }
};
