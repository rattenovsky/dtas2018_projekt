const sails = require('sails')
module.exports["swagger-generator"] = {
    swaggerJsonPath: "./assets/swagger.json",
    swagger: {
        swagger: '2.0',
        info: {
            title: 'Emeison API Docs',
            description: 'Emeison API documenations',
            termsOfService: 'http://example.com/terms',
            contact: {name: 'Wiktor Morawski', url: 'http://github.com/pistoolero', email: 'ratten@aceleague.pl'},
            license: {name: 'Apache 2.0', url: 'http://www.apache.org/licenses/LICENSE-2.0.html'},
            version: '1.0.0'
        },
        blueprint_parameters: {list: []},
        params: {
          BearerHeaderParam: {
            in: 'header',
            name: 'authorization',
            required: false,
            type: 'string',
            description: 'Bearer Token for some requests'
          }
        },
        host: process.env.NODE_ENV === 'production' ? 'emeison.herokuapp.com' : 'localhost:1337',
        basePath: '/',
        externalDocs: {url: 'http://theophilus.ziippii.com'}
    }
};
