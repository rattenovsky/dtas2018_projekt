/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */
const uuidAPIKey = require("uuid-apikey");
module.exports.policies = {
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/
  "*": "isAuth",
  ApikeyController: {
    renew: true,
    get: true,
    add: true
  },
  UserController: {
    get: "checkToken",
    token: "checkToken",
    update: "checkToken",
    destroy: "checkToken",
    session: "checkToken",
    addToCart: "checkToken",
    getCart: "checkToken",
    clearCart: "checkToken",
    deleteCartItem: "checkToken",
    updateCartItemQuantity: "checkToken",
    add: true
  },
  OrderController: {
    add: "checkToken",
    get: "checkToken",
    getOrderById: "checkToken"
  }
};
