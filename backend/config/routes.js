/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  "/": {
    view: "pages/swagger",
    locals: {
      layout: "layouts/swagger"
    },
    swagger: {}
  },

  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  "get /book/": {
    controller: "BookController",
    action: "list",
    swagger: {
      summary: "List books",
      description: "List all books in DataBase",
      responses: {
        200: {
          description: "An array of Book objects"
        }
      }
    }
  },
  "get /book/search": {
    controller: "BookController",
    action: "findBook",
    swagger: {
      summary: "Filter books by query",
      description: "Find books whose fields match specified query (?q= ...)"
    }
  },
  "get /book/paginate": {
    controller: "BookController",
    action: "paginate",
    swagger: {
      summary: "Used to paginate books"
    }
  },
  "get /book/multiple": {
    controller: "BookController",
    action: "findMultipleById",
    swagger: {
      summary: "Find multiple books using query params"
    }
  },
  "get /book/:id": {
    controller: "BookController",
    action: "findId",
    swagger: {
      summary: "Find ID",
      description: "Find book by ID in DataBase"
    }
  },
  "get /book/isbn/:isbn": {
    controller: "BookController",
    action: "findIsbn",
    swagger: {
      summary: "Find ISBN",
      description: "Find book by ISBN"
    }
  },
  "delete /book/:id": {
    controller: "BookController",
    action: "delete",
    swagger: {
      summary: "Delete given book",
      params: {
        id: { type: "number", required: true }
      }
    }
  },
  "patch /book/isbn/:isbn": {
    controller: "BookController",
    action: "update",
    swagger: {
      summary: "Update book by ISBN",
      body: {
        title: { type: "string" }
      }
    }
  },
  "patch /book/:id": {
    controller: "BookController",
    action: "update",
    swagger: {
      summary: "Update book by ID"
    }
  },
  "post /book/": {
    controller: "BookController",
    action: "addBook",
    swagger: {
      summary: "Add book",
      description: "Body can be either an object and an array of objects",
      body: {
        isbn: { type: "string", required: true },
        title: { type: "string", required: true },
        language: { type: "string", required: true }
      }
    }
  },
  "put /book/authors/:id": {
    controller: "BookController",
    action: "updateAuthors",
    swagger: {
      summary: "Update book authors",
      description: "Body can be either an object and an array of objects",
      body: {
        authors: { type: "string", required: true }
      }
    }
  },
  "post /author/": {
    controller: "AuthorController",
    action: "add",
    swagger: {
      summary: "Add author",
      description: "Body can be either an object and an array of objects",
      body: {
        firstName: { type: "string", required: true },
        lastName: { type: "string", required: true }
      }
    }
  },
  "get /author/": "AuthorController.list",
  "get /author/:id": "AuthorController.find",
  "get /author/book/:book": "AuthorController.findByBook",
  "post /apikey/": {
    controller: "ApikeyController",
    action: "add",
    swagger: {
      summary: "Add apikey to database",
      body: {},
      parameters: [
        {
          $ref: "#/parameters/TokenHeaderParam",
          in: "header",
          name: "x-api-key",
          required: false,
          type: "string",
          description: "x-api-key is not required for this request"
        }
      ]
    }
  },
  "get /apikey/": "ApikeyController.list",
  "patch /apikey/renew/:apikey/:uuid": "ApikeyController.renew",
  "get /apikey/:apikey": "ApikeyController.get",
  "post /cover/": {
    controller: "CoverController",
    action: "uploadFile",
    swagger: {
      summary: "Add cover to book",
      body: {
        book: { type: "string", required: true },
        cover: { type: "file", required: true }
      }
    }
  },
  "get /cover/": "CoverController.list",
  "post /auth/register": {
    controller: "UserController",
    action: "add",
    swagger: {
      summary: "Add user to database",
      body: {
        firstName: { type: "string", required: true },
        lastName: { type: "string", required: true },
        email: { type: "string", required: true },
        password: { type: "string", required: true },
        repeatPassword: { type: "string", required: true }
      }
    }
  },
  "get /user/": "UserController.list",
  "post /auth/login": {
    controller: "UserController",
    action: "login",
    swagger: {
      summary: "Login to user account, returns JWT",
      body: {
        email: { type: "string", required: true },
        password: { type: "string", required: true }
      }
    }
  },
  "post /user/addToCart": "UserController.addToCart",
  "get /user/getCart": "UserController.getCart",
  "patch /user/clearCart": "UserController.clearCart",
  "patch /user/deleteCartItem/:bookId": "UserController.deleteCartItem",
  "post /user/updateCartItemQuantity": "UserController.updateCartItemQuantity",
  "get /user/:id": {
    controller: "UserController",
    action: "get",
    swagger: {
      parameters: [
        {
          in: "header",
          name: "authorization",
          required: false,
          type: "string",
          description: "Bearer Token for some requests"
        }
      ]
    }
  },
  "get /auth/session": {
    controller: "UserController",
    action: "session",
    swagger: {
      parameters: [
        {
          in: "header",
          name: "authorization",
          required: false,
          type: "string",
          description: "Bearer Token for some requests"
        }
      ]
    }
  },
  "get /publisher/": "PublisherController.list",
  "post /publisher/": {
    controller: "PublisherController",
    action: "add",
    swagger: {
      summary: "Add publisher",
      description: "Body can be either an object and an array of objects",
      body: {
        name: { type: "string", required: true }
      }
    }
  },
  "patch /publisher/:id": {
    controller: "PublisherController",
    action: "update",
    swagger: {
      summary: "Update publisher by ID"
    }
  },
  "delete /publisher/:id": {
    controller: "PublisherController",
    action: "delete",
    swagger: {
      summary: "Delete given publisher",
      params: {
        id: { type: "number", required: true }
      }
    }
  },
  "get /publisher/:id": {
    controller: "PublisherController",
    action: "find",
    swagger: {
      summary: "Get publisher by ID"
    }
  },
  "get /publisher/name/:name": {
    controller: "PublisherController",
    action: "findName",
    swagger: {
      summary: "Get publisher by name"
    }
  },
  "post /order": {
    controller: "OrderController",
    action: "add"
  },
  "get /orders": {
    controller: "OrderController",
    action: "get"
  },
  "get /order/:id": {
    controller: "OrderController",
    action: "getOrderById"
  }
  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝

  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝
};
