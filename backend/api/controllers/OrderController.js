// const UserController = require("./UserController");

module.exports = {
  async add(req, res) {
    const promises = [
      Order.create({ ...req.body, user: req.user }).fetch(),
      User.update({ id: req.user }).set({ cart: {} })
    ];
    await Promise.all(promises).then(([order]) => res.json(order));
  },
  async get(req, res) {
    Order.find({ user: req.user })
      .then(orders => res.json(orders))
      .catch(e => res.badRequest("Could not fetch orders!"));
  },
  getOrderById(req, res) {
    Order.find({ user: req.user, id: req.params.id })
      .then(order => res.json(order))
      .catch(() => res.badRequest("Could not fetch the order"));
  }
};
