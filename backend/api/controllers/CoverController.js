/**
 * CoverController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const sharp = require("sharp");
const AWS = require("aws-sdk");
const fs = require("fs");
const uuidAPIKey = require("uuid-apikey");
module.exports = {
  async uploadFile(req, res, json = true) {
    const isWin = process.platform === "win32";
    if (req.body.book) {
      sails.log(req.file("cover"));
      let uploaded = await new Promise((resolve, reject) => {
        req.file("cover").upload({}, (err, uploadedFiles) => {
          if (err) reject(err);
          resolve(uploadedFiles);
        });
      });
      sails.log(uploaded);
      let original = uploaded[0].fd;
      let path;
      if (isWin) {
        path = original.split("\\");
      } else {
        path = original.split("/");
      }
      let name = path[path.length - 1];
      let smallPath = name.split(".");
      smallPath[0] += "_lazy";
      smallPath = smallPath.join(".");
      let s3 = new AWS.S3({
        accessKeyId: "AKIAI2VA6XF5TH2OKJ4Q",
        secretAccessKey: "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",
        params: {
          Bucket: "emeison-uploads"
        }
      });
      let data = await sharp(original)
        .resize(300, 200, { fit: "contain" })
        .blur(35)
        .jpeg({ quality: 40 })
        .toBuffer();
      lazy = await s3
        .upload({ ACL: "public-read", Body: data, Key: smallPath })
        .promise();
      let dataNormal = await sharp(original)
        .jpeg({ quality: 80 })
        .resize(300, 200, { fit: "contain" })
        .toBuffer();
      normal = await s3
        .upload({ ACL: "public-read", Body: dataNormal, Key: name })
        .promise();
      await Cover.create({
        normal: normal.Location,
        lazy: lazy.Location,
        book: req.body.book
      })
        .fetch()
        .then(cover => {
          return json ? res.json(cover) : cover;
        });
    } else {
      return res.badRequest(
        "You have to upload a file and provide id of book to update"
      );
    }
  },
  async list(req, res) {
    await Cover.find().then(covers => {
      return res.json(covers);
    });
  }
};
