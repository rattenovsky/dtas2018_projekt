/**
 * AuthorController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  async add(req, res) {
    if (_.isArray(req.body)) {
      await Author.createEach(req.body)
        .fetch()
        .then(authors => {
          return res.json(authors);
        });
    } else {
      await Author.create(req.body)
        .fetch()
        .then(author => {
          return res.json(author);
        });
    }
  },
  async findByBook(req, res, json = true) {
    await Author.find()
      .populate("books", {
        id: res.params.book
      })
      .then(authors => {
        return json ? res.json(authors) : authors;
      });
  },
  async find(req, res) {
    await Author.findOne({ id: req.params.id })
      .populate("books")
      .then(author => {
        return res.json(author);
      });
  },

  async list(req, res) {
    req.query.noBooks
      ? await Author.find().then(authors => {
          return res.json(authors);
        })
      : await Author.find()
          .populate("books")
          .then(authors => {
            return res.json(authors);
          });
  }
};
