/**
 * ApikeyController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const uuidAPIKey = require('uuid-apikey')
module.exports = {
  async add (req, res) {
    let key = uuidAPIKey.create()
    await Apikey.create({ uuid: key.uuid, key: key.apiKey }).fetch().decrypt().then(apikey => {
      return res.json(apikey)
    })
  },
  async list (req, res) {
    await Apikey.find().decrypt().then(apikeys => {
      return res.json(apikeys)
    })
  },
  async renew (req, res) {
    await Apikey.update({uuid: req.params.uuid, key: req.params.apikey})
                .set({validUntil: Date.now() + 2592000000})
                .fetch()
                .decrypt()
                .then(updated => {
                  return res.json(updated)
                })
  },
  async get (req, res) {
    await Apikey.findOne({key: req.params.apikey}).then(key => {
      return res.json(key)
    })
  }
};
