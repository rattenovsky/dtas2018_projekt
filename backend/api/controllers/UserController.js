/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const jwt = require("jsonwebtoken");
const actionUtil = require("../../node_modules/sails/lib/hooks/blueprints/actionUtil");
var ObjectId = require("mongodb").ObjectID;

const rawUserCollection = User.getDatastore().manager.collection(
  User.tableName
);

module.exports = {
  async add(req, res) {
    if (
      ["email", "password", "repeatPassword", "firstName", "lastName"].some(r =>
        Object.keys(req.body).includes(r)
      )
    ) {
      if (req.body.password !== req.body.repeatPassword)
        return res.badRequest("Passwords not equal");
      try {
        const createdUser = await User.create(
          _.omit(req.body, ["repeatPassword"])
        ).fetch();
        const token = jwt.sign(
          { id: createdUser.id },
          sails.config.session.secret,
          {
            expiresIn: sails.config.session.duration
          }
        );
        return res.json({
          id: createdUser.id,
          token,
          email: createdUser.email,
          firstName: createdUser.firstName,
          lastName: createdUser.lastName,
          avatar: null
        });
      } catch (e) {
        if (e.code === "E_UNIQUE")
          return res.badRequest("This email is already taken");
        return res.badRequest(e);
      }
    } else {
      return res.badRequest("No empty fields allowed");
    }
  },

  async list(req, res) {
    return await User.find().then(users => res.json(users));
  },

  async addToCart(req, res) {
    const incrementKey = `cart.${req.body.book}`;
    const incrementObj = {};
    incrementObj[incrementKey] = 1;

    try {
      await rawUserCollection.findOneAndUpdate(
        {
          _id: new ObjectId(req.user)
        },
        {
          $inc: incrementObj
        },
        { upsert: true }
      );
      return res.json(req.body.book);
    } catch (e) {
      return res.badRequest("Could not add book to the cart");
    }
  },
  async getCart(req, res) {
    return await User.find({ id: req.user }).then(user =>
      res.json(user[0].cart)
    );
  },
  async clearCart(req, res) {
    return await User.update({ id: req.user })
      .set({ cart: {} })
      .then(() => {
        return res.ok();
      })
      .catch(e => {
        return res.badRequest("Could not clear the cart!");
      });
  },
  async deleteCartItem(req, res) {
    const bookId = req.params.bookId;
    const path = `cart.${bookId}`;
    const obj = {};
    obj[path] = 1;
    try {
      await rawUserCollection.findOneAndUpdate(
        {
          _id: ObjectId(req.user)
        },
        {
          $unset: obj
        }
      );
      return res.ok();
    } catch (e) {
      return res.badRequest("Could not delete item from cart");
    }
  },
  async updateCartItemQuantity(req, res) {
    const obj = {};
    const path = `cart.${req.body.bookId}`;
    obj[path] = req.body.quantity;
    try {
      await rawUserCollection.findOneAndUpdate(
        {
          _id: ObjectId(req.user)
        },
        {
          $set: obj
        }
      );
      return res.ok();
    } catch (e) {
      return res.badRequest("Could not update item from cart");
    }
  },
  async login(req, res) {
    if (!["email", "password"].some(r => Object.keys(req.body).includes(r))) {
      return res.badRequest("No field should be empty");
    }
    let user = await User.findOne({
      email: req.body.email
    });
    if (!user) return res.badRequest("Either password or email is incorrect");
    try {
      await sails.helpers.passwords.checkPassword(
        req.body.password,
        user.password
      );
    } catch (e) {
      return res.badRequest("Either password or email is incorrect");
    }
    const token = jwt.sign({ id: user.id }, sails.config.session.secret, {
      expiresIn: sails.config.session.duration
    });
    return res.json({
      id: user.id,
      token,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      avatar: user.avatar || null
    });
  },
  async token(req, res) {
    let user = await User.findOne(req.user.id);
    if (!user) return res.badRequest("User not found");
    const token = jwt.sign({ id: user.id }, sails.config.session.secret, {
      expiresIn: sails.config.session.duration
    });
    return res.ok(token);
  },
  async get(req, res) {
    return await User.findOne(req.params.id).then(user => res.json(user));
  },
  session(req, res) {
    return res.json(req.user);
  }
};
