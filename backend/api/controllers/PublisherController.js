/**
 * PublisherController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  async list(req, res) {
    await Publisher.find().then(publishers => {
      return res.json(publishers);
    });
  },
  async find(req, res) {
    await Publisher.findOne({ id: req.params.id }).then(publisher => {
      return res.json(publisher);
    });
  },
  async findName(req, res) {
    await Publisher.findOne({ name: req.params.name }).then(publisher => {
      return res.json(publisher);
    });
  },
  async add(req, res) {
    await Publisher.create(req.body)
      .fetch()
      .then(publisher => res.json(publisher));
  },
  async delete(req, res) {
    await Publisher.destroy({ id: req.body.id })
      .fetch()
      .then(publisher => {
        return res.json(publisher);
      });
  },
  async update(req, res) {
    await Publisher.update({ id: req.params.id })
      .set(req.body)
      .fetch()
      .then(upd => {
        return res.json(upd);
      });
  }
};
