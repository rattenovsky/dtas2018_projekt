/**
 * BookController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const CoverController = require("./CoverController");
const AuthorController = require("./AuthorController");
const actionUtil = require("../../node_modules/sails/lib/hooks/blueprints/actionUtil");

const rawBooksCollection = Book.getDatastore().manager.collection(
  Book.tableName
);

const rawAuthorsCollection = Author.getDatastore().manager.collection(
  Author.tableName
);

const rawPublisherCollection = Publisher.getDatastore().manager.collection(
  Publisher.tableName
);

module.exports = {
  async findIsbn(req, res) {
    await Book.findOne({ isbn: req.params.isbn })
      .populate("authors")
      .populate("covers")
      .populate("publisher")
      .then(books => {
        return res.json(books);
      });
  },
  async findId(req, res) {
    await Book.findOne({ id: req.params.id })
      .populate("authors")
      .populate("covers")
      .populate("publisher")
      .then(books => {
        return res.json(books);
      });
  },
  async findMultipleById(req, res) {
    const ids = req.param("id");
    await Book.find({ id: ids })
      .populateAll()
      .then(books => res.json(books));
  },
  async findBook(req, res) {
    const urlQuery = (req.query.q || "").replace(/[^a-zA-Ząćęłńóśźż ]/g, "");
    if (urlQuery.length <= 0) return res.json([]);
    console.log(urlQuery);
    const regexpExpression = new RegExp(".*" + urlQuery + ".*", "gi");

    // i rly tried to find a way to merge those operations together but nothing of significance was
    // coming up inside mongoose docs
    const foundBooks = await rawBooksCollection
      .find({ title: { $regex: regexpExpression } })
      .map(book => ({ typeaheadField: book.title, id: book._id }))
      .toArray();

    const foundAuthors = await rawAuthorsCollection
      .find({
        $or: [
          { firstName: { $regex: regexpExpression } },
          { lastName: { $regex: regexpExpression } }
        ]
      })
      .map(author => ({
        typeaheadField: [author.firstName, author.lastName].join(" "),
        id: author._id
      }))
      .toArray();

    const foundPublishers = await rawPublisherCollection
      .find({
        name: { $regex: regexpExpression }
      })
      .map(publisher => ({ typeaheadField: publisher.name, id: publisher._id }))
      .toArray();

    const results = [
      { type: "book", displayName: "Books", results: foundBooks },
      { type: "author", displayName: "Authors", results: foundAuthors },
      { type: "publisher", displayName: "Publishers", results: foundPublishers }
    ].filter(res => res.results.length > 0);

    return res.json(results);
  },
  async list(req, res) {
    await Book.find()
      .populate("authors")
      .populate("covers")
      .populate("publisher")
      .then(books => {
        return res.json(books);
      });
  },
  async updateIsbn(req, res) {
    await Book.update({ isbn: req.params.isbn })
      .set(req.body)
      .fetch()
      .then(upd => {
        return res.json(upd);
      });
  },
  async update(req, res) {
    await Book.update({ id: req.params.id })
      .set(req.body)
      .fetch()
      .then(upd => {
        return res.json(upd);
      });
  },
  async addBook(req, res) {
    let authorIds = [];
    async function getOrCreateAuthor(author) {
      return Author.findOrCreate(author, author);
    }
    async function getOrCreatePublisher(publisher) {
      return await Publisher.findOrCreate(
        { name: publisher },
        { name: publisher }
      );
    }

    function sequentiallyCreateAuthors() {
      return req.body.authors.reduce(
        (previous, currentAuthor) =>
          previous.then(accumulator =>
            getOrCreateAuthor(currentAuthor).then(author =>
              accumulator.concat(author.id)
            )
          ),
        Promise.resolve([])
      );
    }

    const publisher = await getOrCreatePublisher(req.body.publisher);

    sequentiallyCreateAuthors()
      .then(authors => {
        authorIds = authors;
        return Book.create({
          ...req.body,
          authors,
          publisher: publisher.id
        });
      })
      .then(async book => {
        await Book.addToCollection(book.id, "authors", authorIds);
        return res.json(book);
      })
      .catch(e => res.badRequest("Could not add the book"));
  },
  async paginate(req, res) {
    req.options = {
      ...req.options,
      criteria: {
        blacklist: [
          "limit",
          "skip",
          "sort",
          "populate",
          "page",
          "pageSize",
          "author"
        ]
      }
    };

    let ids = [];
    if (req.query.author) {
      const booksByAuthor = await Author.find({
        id: req.query.author
      }).populate("books");
      ids = booksByAuthor.reduce((acc, author) => {
        // push returns length of array, have to use manual return in this case :C
        acc.push(...author.books.map(book => book.id));
        return acc;
      }, []);
    }

    const query = Book.find()
      .where({
        ...actionUtil.parseCriteria(req),
        id: ids.length > 0 ? ids : undefined
      })
      .sort(actionUtil.parseSort(req))
      .paginate(
        Number(req.query.page) - 1 || 0,
        Number(req.query.pageSize) || 20
      );
    const count = await Book.count({
      ...actionUtil.parseCriteria(req),
      id: ids.length > 0 ? ids : undefined
    });
    query
      .populate("covers")
      .populate("authors")
      .populate("publisher")
      .then(books => {
        return res.json({
          count,
          books
        });
      });
  },
  async delete(req, res) {
    await Book.destroy({ id: req.params.id })
      .fetch()
      .then(book => {
        return res.json(book);
      });
  },
  async updateAuthors(req, res) {
    sails.log(req.body);
    await Book.addToCollection(req.params.id, "authors", req.body.authors);
    return res.ok();
  }
};
