const jwt = require("jsonwebtoken");
module.exports = async function(req, res, proceed) {
  let bearerToken;
  let bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    let bearer = bearerHeader.split(" ");
    bearerToken = bearer[1];
    if (bearer[0] !== "Bearer") {
      return res.forbidden("Bearer not understood");
    }
    try {
      let decoded = jwt.verify(bearerToken, sails.config.session.secret);
      let user = await User.findOne(decoded.id);
      if (!user) return res.serverError("User not found");
      req.user = user.id;
      proceed();
    } catch (e) {
      sails.log("verification error", e);
      return res.forbidden(e.message);
    }
  } else {
    return res.forbidden("No token provided");
  }
};
