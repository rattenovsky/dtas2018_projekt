module.exports = async function (req, res, proceed) {
  if (!req.headers['x-api-key']) {
    return res.forbidden()
  } else {
    return proceed()
  }
}
