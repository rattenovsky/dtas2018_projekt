/**
 * Book.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    isbn: {
      type: "string",
      required: true,
      unique: true
    },
    title: {
      type: "string",
      required: true
    },
    publishedDate: {
      type: "number"
    },
    pageCount: {
      type: "number"
    },
    language: {
      type: "string",
      required: true
    },
    description: {
      type: "string"
    },
    price: {
      type: "number",
      required: true
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    authors: {
      collection: "author",
      via: "books"
    },
    categories: {
      collection: "category",
      via: "books"
    },
    covers: {
      collection: "cover",
      via: "book"
    },
    publisher: {
      model: "publisher"
    }
  }
};
